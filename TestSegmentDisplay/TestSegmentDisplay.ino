
#include <Arduino.h>
#include "LedControl.h"
#include "SD.h"
#include <SPI.h>



void turnOn(int pin) {
    digitalWrite(pin, LOW);
}

void turnOff(int pin) {
    digitalWrite(pin, HIGH);
}

void setup() {
    pinMode(segDisplayPin, OUTPUT);
}

void loop() {
    /* 
     * Display a character on a 7-Segment display.
     * There are only a few characters that make sense here :
     *	'0','1','2','3','4','5','6','7','8','9','0',
     *  'A','b','c','d','E','F','H','L','P',
     *  '.','-','_',' ' 
     * Params:
     * addr	address of the display
     * digit	the position of the character on the display (0..7)
     * value	the character to be displayed. 
     * dp	sets the decimal point.
     */
    segdisplay.setChar(0, 7, 'H', false);
    segdisplay.setChar(0, 6, 'E', false);
    segdisplay.setChar(0, 5, 'L', false);
    segdisplay.setChar(0, 4, 'P', false);

    segdisplay.setChar(0, 2, 'L', false);
    segdisplay.setChar(0, 1, '2', false);
    segdisplay.setChar(0, 0, '6', false);

    //    for (int i = 0; i < 8; i++)
    //        segdisplay.setChar(0, i, i + 1, false);
    delay(1000);
}