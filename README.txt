/* AutomatedEvaluation */


Changes:


	V1_01:
		1.Can measure more than one device at a time (currently can only READ RFM190 and AR20)
		2.New Button (mostly for debugging since final product will only have one button for use)
		3.LCD is no using software serial on digital pin 10
		4.added flags for using multiple devices at once



	V1:
		1. Can communicate with the refractometer and accumet Research AR 20 pH meter
		2. Has scrollable menu using the C - down and D - UP on the keypad
		3. Has function to set lcd brightness to 100% and on
		4. General Measurer and Master Receiver are test modes, they are only made
		to communicate from arduino to arduino without measuring devices connected
		to them.
		5. Button functionality in pH reads the data - the printer spits out the data
		at a set interval and set baud rate/
		6.Button functionality in refractometer sends a signal for the refractometer 
		to take a measurement and send the results back. This connection needs DSR
		asserted ( LOW) in order to work.

	V1_02:
		1.MasterReceiver() is working
		2.Multiple Devices ca now have the option of a test batch (press '#')
		or a real data batch (press button).
		3.Multiple Arduinos can send measurements to the master, the master can determine which
		devices are being sent and how many using flags that all arduinos must pass in on every 
		send.


	V1_03:
