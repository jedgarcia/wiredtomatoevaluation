/* 
 * File:   debugFunctions.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 10:51 AM
 */

#ifndef DEBUGFUNCTIONS_H
#define	DEBUGFUNCTIONS_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "SDManipulation.h"
#include "SimpleVector.h"
#include "SDFunctions.h"

void sdManipulationDebug() {
    String choice = "";
    do {
        Serial.print("Want to: 1. edit    or    2. print a file   or  3.write to File?:  ");
        choice = getIncomingString(Serial);
        Serial.println(choice);

        if (choice.toInt() == 1) {//wants to edit

            lcd_clear();
            lcd_print(lines[0], F("which file?"));
            Serial.print("Which file?: ");
            pathToRegradeFile = getIncomingString(Serial);
            pathToRegradeFile.toCharArray(test_c, 25);
            Serial.println(pathToRegradeFile);

            lcd_print(lines[1], F("What message: "));
            Serial.print(F("What Message?: "));
            test_s = getIncomingString(Serial);
            Serial.println(test_s);

            lcd_print(lines[2], F("Comma pos?: "));
            Serial.print(F("Comma pos?: "));
            test_s2 = getIncomingString(Serial);
            Serial.println(test_s2);

            bool isRegrade = false;
            String regrade = "";
            Serial.print("Regrade(y/n)?: ");
            regrade = getIncomingString(Serial);
            Serial.println(regrade);
            if (regrade.equals("Y") || regrade.equals("y")) isRegrade = true;

            addToSD(test_c, test_s, test_s2.toInt(), isRegrade);

            printFile(test_c, Serial);

        } else if (choice.toInt() == 2) {//wants to print a file
            lcd_clear();
            lcd_print(lines[0], "Which file?:");

            printNewline(Serial, 10);

            Serial.print("Which file?: ");
            pathToRegradeFile = getIncomingString(Serial);
            pathToRegradeFile.toCharArray(test_c, 25);
            Serial.println(pathToRegradeFile);


            printNewline(Serial, 4);
            printFile(test_c, Serial);
        } else if (choice.toInt() == 3) {//want to test writing to a file
            Serial.print("Which file?: ");
            pathToRegradeFile = getIncomingString(Serial);
            pathToRegradeFile.toCharArray(test_c, 25);
            Serial.println(pathToRegradeFile);

            bool isRegrade = false;
            String regrade = "";
            Serial.print("Regrade(y/n)?: ");
            regrade = getIncomingString(Serial);
            Serial.println(regrade);
            if (regrade.equals("Y") || regrade.equals("y")) isRegrade = true;

            Serial.println(F("1. Color"));
            Serial.println(F("2. Refrac"));
            Serial.println(F("3. pH"));
            Serial.println(F("4. Weights"));
            Serial.print(F("Your choice: "));
            String type = getIncomingString(Serial);
            Serial.println(type);

            int type_i = type.toInt();
            switch (type_i) {
                case 1:
                    colorBuffer = "314";
                    writeCommToFile(test_c, isRegrade);
                    break;
                case 2:
                    refracBuffer = "3.14";
                    writeRefracToFile(test_c, isRegrade);
                    break;
                case 3:
                    phBuffer = "3.14";
                    writePhToFile(test_c, isRegrade);
                    break;
                case 4:
                    for (int i = 0; i < 10; i++) {
                        weightBuffers[i] = "MOT EXT: ";
                        weightBuffers[i] += i;
                    }
                    writeWeightsToFile(test_c, isRegrade);
                    break;
                case 5:
                    break;
                default:
                    return;
                    break;
            }




        }
    } while (choice.toInt() != 1 && choice.toInt() != 2 && choice.toInt() != 3);
}

void printNewline(HardwareSerial& output, int amount) {
    for (int i = 0; i < amount; i++)
        output.println();
}

String getIncomingString(HardwareSerial& input) {
    String incomingString = "";
    while (1) {
        if (input.available()) {
            while (input.available()) {
                incomingString += (char) input.read();
                delay(40);
            }
            break;
        }
    }
    return incomingString;
}


//==============================================================================
//Debug Functions 

void setToFalse(bool array[], int size) {
    for (int i = 0; i < size; i++) {
        array[i] = false;
    }
}

//***************************************

void getPhSettings() { //this function allows the user to input the ph settings they want then it stores it into eeprom
    //dont worry too much about this

    while (1) { //getting valid rate for reading the ph meter
        lcd_clear();
        lcd_println(L1Ta, F("Rate(sec): "));


        numberPad(L1Ta + 12, 1, rate); //numberpad allows for a user to enter in numbers at a certain part of the screen
        //all you have to do is specify where on the screen, how many digits to accept,
        //and give a char buffer to store the response in


        PH_READING_RATE = atoi(rate)*1000; //converting rate from seconds to milliseconds

        if (PH_READING_RATE < 3000) {
            lcd_println(L1Ta, F("Rate Too Low"));
            delay(750);
        } else {
            //EEPROM_writeAnything(PH_READING_RATE_A, PH_READING_RATE);
            break;
        }
    }


    lcd_print(L2Ta, F("Epsilon(.xxx): "));
    lcd_print(L2Ta + 15, "0.");
    numberPad(L2Ta + 17, 3, &(epsilon[2]));
    PH_EPSILON = atof(epsilon);
    //EEPROM_writeAnything(PH_EPSILON_A, PH_EPSILON);


    while (1) {//getting a valid ph timeout from the user
        lcd_println(L3Ta, F("Timeout(sec): "));
        numberPad(L3Ta + 14, 2, timeout);
        PH_STABLE_TIMEOUT = atoi(timeout) * 1000L;
        if (PH_STABLE_TIMEOUT > 60000) {
            lcd_println(L3Ta, F("Timeout Too High"));
            delay(750);
        } else {
            //EEPROM_writeAnything(PH_STABLE_TIMEOUT_A, PH_STABLE_TIMEOUT);
            break;
        }
    }//while(1)


    lcd_print(L4Ta, F("Configuration Set"));
    saveCurrentConfiguration(MAIN_MENU_MODE);

    delay(1000);
}

void check_ph_settings() { //This function displays the saved ph settings neatly
    //dont worry too much about this
    lcd_clear();

    //buffers for the settings so we can print them out
    char rate_[15];
    char epsilon_[15];
    char timeout_[15];
    char dummy[15];
    int space = 0;

    itoa(PH_READING_RATE / 1000, rate_, 10); //divided by a thousand to get the rate in seconds (currently in milli seconds)
    itoa(PH_EPSILON * 1000, epsilon_, 10);
    itoa(PH_STABLE_TIMEOUT / 1000, timeout_, 10); // 10 as the last argument to denote that we want to convert from integer to alphanumeric decimal format

    if (PH_EPSILON * 1000 > 100) {
        strcpy(dummy, "");
        space = 0;
    } else if (PH_EPSILON * 1000 > 10) {
        strcpy(dummy, "0");
        space = 1;
    } else if (PH_EPSILON * 1000 < 10) {
        strcpy(dummy, "00");
        space = 2;
    }


    lcd_print(L1Ta, F("Rate(secs): "));
    lcd_print(L1Ta + 15, rate_);

    lcd_print(L2Ta, F("Epsilon: "));
    lcd_print(L2Ta + 15, F("0."));
    lcd_print(L2Ta + 17, dummy);
    lcd_print(L2Ta + 17 + space, epsilon_);


    lcd_print(L3Ta, F("Timeout(secs): "));
    lcd_print(L3Ta + 15, timeout_);

    lcd_print(L4Ta, F("Press Clear to exit"));

    while (getKey() != CLEAR);

}

void Sniffer(HardwareSerial& receiver, String message, String filename) {
    lcd_clear();
    lcd_print(L1Ta, message);

    char filePath[15];
    filename.toCharArray(filePath, 15);

    //These must be confired like this in order for the SD library to work
    pinMode(53, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(10, OUTPUT);

    Serial1.begin(9600);

    File file = SD.open(filePath, FILE_WRITE);
    file.close();
    pinMode(53, INPUT);



}

void LedRefracSniffer() { //This is a sniffer to see the signals going between the led - refrac - gradestar
    // You can follow along real time through the serial monitor, or leave it running and look in the sd card

    //Prints "Sniffing..." to the first line on the lcd
    lcd_clear();
    lcd_print(L1Ta, F("Sniffing..."));


    //Serial 1 is for the refractometer to LED
    //Serial 2 is for the LED to refractometer
    //Serial 3 is for the LED to gradestar
    Serial1.begin(9600); //refrac
    Serial2.begin(9600); //led
    Serial3.begin(19200); //gradestar

    //These must be confired like this in order for the SD library to work
    pinMode(53, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(10, OUTPUT);

    //file paths
    char RefracToLEDPath[] = "rtoled.txt";
    char LedToRefracPath[] = "ledtor.txt";
    char LedToGradestarPath[] = "ledtog.txt";
    char LogFilePath[] = "log.txt";

    //opening SD card files - this just creates the files
    //make sure your file names aren't too long
    //The reason the file names use 'rToLed' instead of
    //"refracToLed", is because the library wouldn't create that
    //long of a file name
    //FILE_WRITE - you intend to write to a file
    //FILE_READ  -  you intend to read from a file
    File RefracToLED = SD.open(RefracToLEDPath, FILE_WRITE);
    File LedToRefrac = SD.open(LedToRefracPath, FILE_WRITE);
    File LedToGradestar = SD.open(LedToGradestarPath, FILE_WRITE);
    File LogFile = SD.open(LogFilePath, FILE_WRITE);


    //Every time you want to save something you have to close it
    //So in this case, im closing all the files so that the files are 
    //written to the SD card, otherwise the following SD card check will fail
    //and say that the files did not open successfully
    RefracToLED.close();
    LedToRefrac.close();
    LedToGradestar.close();
    LogFile.close();


    //Checking to see that the files wrote successfully
    //SD card should preferably be formatted to FAT16 (also called 'FAT')
    //Arduino SD library recommends FAT 16 over FAT 32
    //The following error check makes sure that the files were created on
    //the SD card
    if (SD.exists(RefracToLEDPath) && SD.exists(LedToRefracPath) &&
            SD.exists(LedToGradestarPath) && SD.exists(LogFilePath)) {

        //In order to write to a file in the SD card you must open it
        //and then close it when you're done writing to it, I know it is really
        //annoying, however, if you don't do this, and power is lost,
        //then nothing will have been saved to the files and they will
        //contain nothing when the SD card is examined 
        LogFile = SD.open(LogFilePath, FILE_WRITE); //open file

        Serial.println(F("ALL FILES OPENED SUCCESSFULLY"));
        LogFile.println(F("ALL FILES OPENED SUCCESSFULLY"));

        LogFile.close(); //close file
    } else {
        LogFile = SD.open(LogFilePath, FILE_WRITE); //open file

        Serial.println(F("FILES DID NOT OPEN SUCCESSFULLY"));
        LogFile.println(F("FILES DID NOT OPEN SUCCESSFULLY"));

        LogFile.close(); //close file
    }


    //Starting the real time clock
    RTC_DS1307 RTC; //this is the type of RTC we got, so you must declare it
    Wire.begin(); //this must be done in order to use SCL & SDA
    RTC.begin(); //You must begin the RTC

    if (!RTC.isrunning()) { //checks to see that the clock is running well

        LogFile = SD.open(LogFilePath, FILE_WRITE); //open file
        LogFile.println(F("RTC IS NOT WORKING"));
        LogFile.close(); //close file

        Serial.println(F("RTC IS NOT WORKING"));


    } else {
        LogFile = SD.open(LogFilePath, FILE_WRITE); //open file
        LogFile.println(F("RTC IS WORKING"));
        LogFile.close(); //close file

        Serial.println(F("RTC IS WORKING"));
    }


    while (1) {//main loop that keeps sniffing forever, until you power down the box or press CLEAR

        if (getKey() == CLEAR) return;

        while (Serial1.available()) {//Serial 1 is for the refractometer to LED communication
            //this while loop has a while loop within it so that we can time stamp
            //only once and not every single time that we receive an incoming byte

            //must timestamp that some communication is happening
            RefracToLED = SD.open(RefracToLEDPath, FILE_WRITE); //opening file
            //writing timestamp to the file
            DateTime now = RTC.now();
            RefracToLED.println();
            RefracToLED.print(now.year(), DEC);
            RefracToLED.print(F("/"));
            RefracToLED.print(now.month(), DEC);
            RefracToLED.print(F("/"));
            RefracToLED.print(now.day(), DEC);
            RefracToLED.print(F(" "));
            RefracToLED.print(now.hour(), DEC);
            RefracToLED.print(F(":"));
            RefracToLED.print(now.minute(), DEC);
            RefracToLED.print(F(":"));
            RefracToLED.print(now.second(), DEC);
            RefracToLED.println();

            //realtime Feed
            Serial.println(F("REFRAC TO LED: "));

            while (Serial1.available()) {//this input is the data the refrac want to tell the LED

                //Store what the refrac is trying to tell the LED
                incoming = Serial1.read();
                RefracToLED.print((char) incoming); //writing the incoming message to file


                //Real time feed
                Serial.print((char) incoming);


                //Send the LED its message            
                Serial2.print((char) incoming);

                //The Serial buffer for the UARTS does not fill up that fast so 
                //we put a delay so that if we exit out of this inner while loop
                //we are sure that there is no more information to be received
                delay(20);
            }

            RefracToLED.println();
            RefracToLED.close(); //closing file

            Serial.println();
            Serial.println();
            break;

        }

        while (Serial2.available()) {//Serial 2 is LED - refrac
            //this is input is what the led wants to tell the refrac

            //must timestamp that some communication is happening
            LedToRefrac = SD.open(LedToRefracPath, FILE_WRITE); //open file

            DateTime now = RTC.now();
            LedToRefrac.println();
            LedToRefrac.print(now.year(), DEC);
            LedToRefrac.print(F("/"));
            LedToRefrac.print(now.month(), DEC);
            LedToRefrac.print(F("/"));
            LedToRefrac.print(now.day(), DEC);
            LedToRefrac.print(F(" "));
            LedToRefrac.print(now.hour(), DEC);
            LedToRefrac.print(F(":"));
            LedToRefrac.print(now.minute(), DEC);
            LedToRefrac.print(F(":"));
            LedToRefrac.print(now.second(), DEC);
            LedToRefrac.println();

            Serial.println(F("LED TO REFRAC: "));

            while (Serial2.available()) {

                //Store what LED is trying to tell the refrac
                incoming = Serial2.read();
                LedToRefrac.print((char) incoming); //storing the incoming message in file


                //Realtime feed
                Serial.print((char) incoming);

                //Send the Refrac its message            
                Serial1.print((char) incoming);

                delay(20); //delay to ensure the serial buffer had enough time to fill
            }
            Serial.println();
            Serial.println();
            LedToRefrac.println();
            LedToRefrac.close(); //closing file
            break;
        }

        //This only needs one 
        while (Serial3.available()) {//Serial 3 is for the LED  - gradestar
            //must timestamp that some communication is happening
            LedToGradestar = SD.open(LedToGradestarPath, FILE_WRITE); //opening file
            DateTime now = RTC.now();
            LedToGradestar.println();
            LedToGradestar.print(now.year(), DEC);
            LedToGradestar.print(F("/"));
            LedToGradestar.print(now.month(), DEC);
            LedToGradestar.print(F("/"));
            LedToGradestar.print(now.day(), DEC);
            LedToGradestar.print(F(" "));
            LedToGradestar.print(now.hour(), DEC);
            LedToGradestar.print(F(":"));
            LedToGradestar.print(now.minute(), DEC);
            LedToGradestar.print(F(":"));
            LedToGradestar.print(now.second(), DEC);
            LedToGradestar.println();

            Serial.println(F("LED TO GRADESTAR"));

            while (Serial3.available()) {

                //this is input that the led wants to tell gradestar
                incoming = Serial3.read();
                LedToGradestar.print((char) incoming); //writing to file


                //debug
                Serial.print((char) incoming);
                delay(25);
            }
            Serial.println();
            Serial.println();
            LedToGradestar.println();
            LedToGradestar.close();

        }
    }//while(1)
}// ledSniffer())

void getRidOfSpaces(String& input) { // gets rid of all spaces in a string
    int length = input.length();
    String tempString = "";

    for (int i = 0; i < length; i++) {
        if (input.charAt(i) != ' ') {
            tempString += input.charAt(i);
        }
    }

    input = tempString;
}

void LogFromXbee(char* filename) {
    //Serial is going to be used for the Xbee
    //These must be confired like this in order for the SD library to work
    pinMode(53, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(10, OUTPUT);

    File log = SD.open(filename, FILE_WRITE);
    log.close();

    lcd_clear();
    if (SD.exists(filename)) {
        lcd_print(L1Ta, F("File Found!"));
    } else {
        lcd_print(L1Ta, F("File not found"));
    }

    while (1) {
        if (Serial.available()) {
            log = SD.open(filename, FILE_WRITE);
            log.print(incoming = (char) Serial.read());
            Serial.print((char) incoming);
        }
        log.close();
        if (getKey() == CLEAR) {
            log.close();
            return;
        }
    }

}

void SerialFlush(HardwareSerial& chosenSerial) {
    while (chosenSerial.available()) {
        chosenSerial.read();
        delay(7);
    }
}

void FlushAllSerial() {
    Serial.flushBuffer();
    Serial1.flushBuffer();
    Serial2.flushBuffer();
    Serial3.flushBuffer();

}

void turnIndicator(String choice) {

    if (choice == "green" || choice == "GREEN") {
        digitalWrite(indicatorGreen, HIGH);
        digitalWrite(indicatorRed, LOW);
        Serial.println(F("Turning the indicator green"));
    } else if (choice == "red" || choice == "RED") {
        digitalWrite(indicatorGreen, LOW);
        digitalWrite(indicatorRed, HIGH);
        Serial.println(F("turningg the indicator red"));
    }
}

void multipleDevicesScanForMaster() {
    Serial.println(F("Doing the master scan"));
    masterAndGradestarSerial.print(PING);
    waitForConfirmationFromPing();
}

bool AtLeastOneListenFlagIsEnabled() {
    if (!listenPh && !listenColor && !listenRefrac && !listenWeight) {//user did not select any devices
        return false;
    } else
        return true;
}

void makeSampleQueueFile() {
    char Qfilepath[] = "queue.txt";
    SD.remove(Qfilepath);
    File queue = SD.open(Qfilepath, FILE_WRITE);
    queue.seek(0); //FILE_WRITE starts us off at the end of the file

    //List of sample queue
    queue.println(F("0903L1.txt"));
    queue.println(F("0903L2.txt"));
    queue.println(F("0903L3.txt"));
    queue.println(F("0903L4.txt"));
    queue.println(F("0903L5.txt"));


    queue.close();
}

String readNextQueueFile(File & file) {
    String loadBuffer = "";
    while ((incoming = file.read()) != '\n' && file.available()) {
        loadBuffer += (char) incoming;
        Serial.print((char) incoming);
    }
    Serial.println();
    return loadBuffer.substring(0, loadBuffer.length() - 1);
}

int sizeOfQueue(File& file) {
    file.seek(0);
    String dummy = "";
    int size = 0;
    while (true) {
        dummy = readNextQueueFile(file);
        if (dummy.length() < 10)
            break;
        else
            ++size;
    }
    file.seek(0);
    return size;
}

void makeSureDateIsInTheFile(char* filepath) {

    File file = SD.open(filepath, FILE_WRITE);
    file.seek(0);

    int forSdCounter = 0;
    StringForSD sdInput[2];

    bool needToAddDate = false;
    bool needToAddTime = false;

    goToCommaPosition(2, file);

    //========================================================
    String date = readNextLoadValue(file);
    Serial.print(F("The date is: "));
    Serial.println(date);
    if (date.length() < 10) {
        needToAddDate = true;
    }

    String time = readNextLoadValue(file);
    Serial.print(F("The time is: "));
    Serial.println(time);
    if (time.length() < 8) {
        needToAddTime = true;
    }
    //================================================
    if (needToAddDate) {
        date = "";
        do {
            date = getDateFromDateTime();
        } while (date.length() < 10);

        sdInput[forSdCounter++].add(date, 2);
    }
    if (needToAddTime) {
        time = "";
        do {
            Serial.println("trying to get date");
            time = getTimeFromDateTime();
            Serial.println(time);
            delay(3000);
        } while (time.length() < 8);

        sdInput[forSdCounter++].add(time, 3);
    }

    if (needToAddDate || needToAddTime)
        addToSD(filepath, sdInput, false, forSdCounter);

    file.close();
}

void queueMenu() {
    lcd_clear();
    sendFinishedQueuedLoads(false);

    int number_of_files = loadQueue.getSize() + 1;

    String queueMenu[number_of_files];
    queueMenu[0] = "   Queue Menu";
    if (number_of_files == 1) {
        lcd_print(L3Ta, F("No Loads in Queue"));
        lcd_print(L4Ta, F("ENTER or Clear: exit"));

    }
    for (int i = 1; i < number_of_files; i++) {
        queueMenu[i] = "";
        if (i < 10)
            queueMenu[i] += "0";
        queueMenu[i] += i;
        queueMenu[i] += ". Load: ";
        queueMenu[i] += loadQueue.get(i - 1);
    }

    Menu2Digits(queueMenu, number_of_files);

    if (key == CLEAR || key == ENTER) {
        lcd_clear();
        lcd_print(L1Ta, F("Saving Changes..."));
        saveCurrentConfiguration(MAIN_MENU_MODE);
        return;
    }
    if (key < 1) {
        lcd_clear();
        lcd_print(L1Ta, "Incorrect Value");
        delay(1000);
    } else if (key > number_of_files) {
        lcd_clear();
        lcd_print(L1Ta, "Incorrect Value");
        delay(1000);
    } else {
        String filepath = "TEMP/";
        int loadNumber = loadQueue.get(key - 1);
        filepath += loadQueue.get(key - 1);
        filepath += ".txt";
        readFromLoadDataFromSD(&filepath[0]);
        if (key == BUTTONNUMBER) {//They want to send this load to gradestar

            makeSureDateIsInTheFile(&filepath[0]);

            File tempFile = SD.open(&filepath[0], FILE_WRITE);
            sendToGradestar(masterAndGradestarSerial, tempFile);

            String newFileName = getAppropriateFilename(&filepath[0]);
            String folders = newFileName.substring(0, 12);
            SD.mkdir(&folders[0]);

            Serial.print("The correct filepath is: ");

            Serial.println(&newFileName[0]);


            copyWholeFileFromTo(tempFile, &newFileName[0]);
            loadQueue.removeData(loadNumber);
            tempFile.close();

            SD.remove(&filepath[0]);
            saveCurrentConfiguration(MAIN_MENU_MODE);
            return;


        }
    }





}

void clockDisplay() {
    lcd_clear();
    //Starting the real time clock
    RTC_DS1307 RTC; //this is the type of RTC we got, so you must declare it
    Wire.begin(); //this must be done in order to use SCL & SDA
    RTC.begin(); //You must begin the RTC

    if (!RTC.isrunning()) { //checks to see that the clock is running well
        lcd_clear();
        lcd_print(L1Ta, F("Clock not working"));
        lcd_print(L2Ta, F("load not recorded"));
        digitalWrite(indicatorGreen, LOW);
        digitalWrite(indicatorRed, HIGH);
        delay(5000);
        return;
    }

    while (true) {
        now = RTC.now();
        lcd_print(lines[0], F("Time: "));
        lcd_print(lines[0] + 6, now.hour());
        lcd_print(lines[0] + 8, ":");
        lcd_print(lines[0] + 9, now.minute());
        lcd_print(lines[0] + 11, ":");
        lcd_print(lines[0] + 12, now.second());

        lcd_print(lines[1], F("Date: "));
        lcd_print(lines[1] + 6, now.month());
        lcd_print(lines[1] + 8, "/");
        lcd_print(lines[1] + 10, now.day());
        lcd_print(lines[1] + 11, "/");
        lcd_print(lines[1] + 12, (int) now.year());


        lcd_print(lines[3], F("Last sync: 9/3/13"));

        if (getKey() == CLEAR)
            return;

    }


}

void listenTo(HardwareSerial& input) {
    while (true) {
        getKey();
        if (key == CLEAR)return;

        if (input.available()) {
            incoming = input.read();
            //Serial.print((int) incoming );
            Serial.print((char) incoming);
        }
    }
}

void debugSDCard(char* filePath, int commaPosition, String message) {
    File file = SD.open(filePath, FILE_WRITE);
    if (!file) {
        Serial.println("filepath could not be opened");
        return;
    }
    file.seek(0);

    goToCommaPosition(commaPosition, file);
    file.print(message);
    file.close();
}


#endif	/* DEBUGFUNCTIONS_H */

