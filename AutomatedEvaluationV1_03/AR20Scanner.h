/* 
 * File:   AR20Scanner.h
 * Author: Jedan Garcia
 *
 * Created on September 26, 2013, 5:46 PM
 */

#ifndef AR20SCANNER_H
#define	AR20SCANNER_H
#include <Arduino.h>
#include <Timer.h>

class AR20Scanner {
public:

    AR20Scanner(HardwareSerial* inputSerial) : AR20Serial(inputSerial), unresponsive(false) {
        AR20Serial->begin(19200);
    }


    String getPh();
    bool unresponsive;
private:

    //Serial buffer
    HardwareSerial* AR20Serial;
    String getNextLine();

    void enableUnresposive() {
        unresponsive = true;
    }


};

#endif	/* AR20SCANNER_H */

