//#include <Arduino.h>
//#include <avr/pgmspace.h>       //has a function to see check how much sram is left
#include <SoftwareSerial.h>     //used for lcd and Serial port 4
#include <Keypad.h>             //used for the keypad - duh...
#include <Timer.h>              //used for anything from blinking light to ph timeouts
#include <EEPROM.h>             //main arduino eeprom library
#include "EEPROMAnything.h"     //used to make writing different variable types to eeprom easier
#include <SD.h>                 //used to store data in sd cards
#include <Wire.h>               //used for communicating to the SDA and SCL
#include <RTClib.h>             //used for the real time clock
#include <string.h>         
#include "SimpleVector.h"
#include "StringForSD.h"
#include "AR20Scanner.h"
//this exists so that we don't have any scope errors, every time you add a new
//function somewhere in the project, you should add its forward declaration here
#include "ForwardDeclarations.h"
#include "HopperScanner.h"

//This is where I declare all my global variables
#include "GlobalVariables.h"
#include "SDFunctions.h"        //allows us to save to SD card
#include "Lcd.h"                //contains all lcd related functions
#include "Button.h"             //contains button related functions
#include "GetSerial.h"          //contains getSerial() functions
#include "AR20.h"               //contains AR20 functions
#include "debugFunctions.h"     //contains a mix of functions, not just debugging
#include "LoadFunctions.h"      //contains load number functions
#include "MenuFunctions.h"      //contains menu functions
#include "RFM190.h"             //contains rfm190 functions
#include "MultipleDevices.h"    //contains functions that deal with measuring multiple devices
#include "MasterReceiverFunctions.h"    //contains the master functions
#include "Hopper.h"             //contains hopper functions
#include "LedColorMeter.h"      //cotains the LED color meter functions
#include "SettingsFunctions.h"  
#include "RegradeFunctions.h"
#include "sampleFormats.h"
#include "SDManipulation.h"
#include "QueueFunctions.h"
#include "Algorithms.h"
#include "StringForSD.h"

void setup() {

    //Setting variables to their correct values
    for (int i = 0; i < 13; i++) //expectedDevices[]]
        expectedDevices[i] = true;


    for (int i = 0; i < 4; i++) //ReceivingDevicesFlags
        ReceivingDevicesFlags[i] = false;

    for (int i = 0; i < NUMBEROFMEASUREMENTS; i++)
        measurementsReceived[i] = false;

    Serial.begin(9600);

    main_menu[0] = F("01. Master Mode"); //1st Arduino Function
    main_menu[1] = F("02. Measuring Mode"); //2nd Arduino Function
    main_menu[2] = F("03. Bright - 100%");
    main_menu[3] = F("04. Set Load Number");
    main_menu[4] = F("05. Change pH config");
    main_menu[5] = F("06. Check pH config");
    main_menu[6] = F("07. Regrade");
    main_menu[7] = F("08. Set station code");
    main_menu[8] = F("09. Delete SD info");
    main_menu[9] = F("10. Load def. Debug");
    main_menu[10] = F("11. Load Data-SD");
    main_menu[11] = F("12. Choose Devices");
    main_menu[12] = F("13. Set weight names");
    main_menu[13] = F("14. Queue Menu");

    device_menu[0] = F("01. Nothing else");
    device_menu[1] = F("02. Go to main menu");
    device_menu[2] = F("03. RFM190 Refrac");
    device_menu[3] = F("04. AR20 pH meter");
    device_menu[4] = F("05. Colorimeter");
    device_menu[5] = F("06. VC505 WT Hopper");
    device_menu[6] = F("07. LED Color Meter");
    //end of menu - 10 items

    for (int i = 0; i < 12; i++)
        measurements_menu[i] = "";


    //Hardware Handshaking for RFM190
    pinMode(dsrPin1, OUTPUT);
    digitalWrite(dsrPin1, LOW);

    pinMode(dsrPin2, OUTPUT);
    digitalWrite(dsrPin2, LOW);

    pinMode(dsrPin3, OUTPUT);
    digitalWrite(dsrPin3, LOW);


    //setting up the button to take measurements - button on pin 51
    //pull up button
    pinMode(button1Pin, INPUT);
    digitalWrite(button1Pin, HIGH);

    //Setting up Indicator Light - Red and Green 
    pinMode(indicatorRed, OUTPUT);
    pinMode(indicatorGreen, OUTPUT);

    //By default when the device is turned on the indicator light will shine green
    digitalWrite(indicatorRed, LOW);
    digitalWrite(indicatorGreen, LOW);

    //Setting up Lcd - Software Serial
    pinMode(lcdPin, OUTPUT);
    lcdSerial.begin(9600);

    //Seting up the master software serial
    pinMode(rxMasterPin, INPUT);
    pinMode(txMasterPin, OUTPUT);

    // This sets up the light in the measurement button
    pinMode(buttonLED, OUTPUT);
    digitalWrite(buttonLED, LOW);

    // Hardware Serial UARTs
    Serial1.begin(9600);
    Serial2.begin(9600);
    Serial3.begin(9600); // digital serial connection
    masterAndGradestarSerial.begin(9600); //The communication to the master will be at baud rate 19200 because the communication to gradestar is at 19200
    delay(50);

    //These must be configured like this in order for the SD library to work
    //pinMode(53, OUTPUT);
    if (!SD.begin(4))
        Serial.println(F("SD DID NOTTT BEGIN!"));
    else
        Serial.println(F("SD DID BEGIN"));
    //pinMode(53, INPUT);



    //clear screen
    lcd_clear();



    //Don't remove this, if you do, then sometimes the LCD will
    //not show the menu when it is booted up, it will just show a blank screen
    //until you press something.
    lcd_print(L1Ta, F("STARTING UP..."));


    Wire.begin(); //this must be done in order to use SCL & SDA
    RTC.begin(); //You must begin the RTC 

    Serial.print(F("freeMemory()="));
    Serial.println(freeMemory());

}//setup

void loadDefaults() {
    //makes a file called "X.txt" - x is the load number  
    char filePath[] = "config.txt";
    if (SD.exists(filePath)) {
        //read configuration
        Serial.println(F("config.txt exists!"));
        File file = SD.open(filePath, FILE_READ);
        readFromConfigFile(file);
    } else {
        Serial.println(F("config.txt does not exist!"));
        //create a configuration file
        createConfigFile();
        File file = SD.open(filePath, FILE_READ);
        readFromConfigFile(file);
    }
}


//main function
bool first_run = true;

void loop() {
    AR20Scanner phScanner(&Serial2);
    char* filePaths_array[4] = {"H1.TXT", "H2.TXT", "H3.TXT", "H4.TXT"};
    int i = 0;

    char filePath_c[25];
    String filePath_s;

    turnIndicator("green");

    Serial.print(F("freeMemory()="));
    Serial.println(freeMemory());

    //this delay is needed the first boot so that it boots into the menu
    //otherwise when you unplug and plug back in, the arduino boots
    //to an empty screen, however it is undesirable to wait for the main menu
    //to load every single time you access it
    if (first_run) {
        loadDefaults();
        delay(1000);

    }


    byte one = 1; //this is used later dont delete it

    lcd_clear();

    //Menu2Digits is a function which displays a scrollable menu and allows
    //the user to press two numbers to get to the menu item they want
    //arduinoFunction is a variable which denotes what the menu item they chose
    //is, because it is used in this main menu, it also kind of denotes 
    //the arduinos MAIN function
    if (!first_run) {
        arduinoFunction = mainMenu(main_menu, MAINMENUSIZE); //loads the user menu
    } else {
        first_run = false;
    }


    switch (arduinoFunction) { //based on the users input in the menu

        case 0:// this is for when the SD card's function is 0 and it means
            //to start from this main menu
            break;

        case 1:
            masterReceiver(); //master mode
            break;
        case 2:
            MeasuringMultiple(); //mode to measure multiple
            break;
        case 3:
            restoreBrightness(); //restores lcd to full brightness
            break;
        case 4:
            SetLoadNumber(); //menu to set the load number
            break;
        case 5:
            getPhSettings(); //displays the settings for the ph
            break;

            //this shows you the ph settings
        case 6:
            check_ph_settings();
            break;
            //this changes the first_boot_flag in eeprom to 1 so
            //that next time the device boots it will read default
            //values and not the user defined ones
        case 7:
            regrade();
            break;
        case 8:
            setStationCode();
            break;
        case 9:
            DeleteSDCard();
            createConfigFile();
            break;
        case 10:
            loadDefaults();
            break;
        case 11:
            readFromLoadDataFromSD();
            break;
        case 12:
            setDevicesMasterIsExpecting();
            break;
        case 13:
            HopperSettings();
            break;
        case 14:
            queueMenu();
            break;
        case 15:
            break;
        case 90:
            break;
        case 93:
            sdManipulationDebug();
            break;
        case 92:
            do {
                filePath_s = getIncomingString(Serial);
                Serial.println(filePath_s);

                if (loadIsDone(&filePath_s[0], false)) {
                    Serial.println("Load is done!");
                } else {
                    Serial.println("Load is NOT done");
                }


                if (filePath_s.equals("exit")) {
                    break;
                }


            } while (1);

            break;

        case 94:
            gradestarSampleMenu();
            break;
        case 95:
            clockDisplay();
            break;
        case 96:
            break;
        case 97:
            makeSampleQueueFile();
            break;
        case 98:
            Serial.println();
            Serial.print("This is the ph: ");
            Serial.println(phScanner.getPh());
            break;
        case 99:
            Serial1.begin(9600, SERIAL_8N2);

            do {//will only keep sending files when you press the button and exit when CLEAR

                if (button1IsPressed()) {
                    Serial.println("Doing it");
                    //Sets the i to a value between 0 and 4
                    i %= 4;

                    //opens up the correct filepath
                    File file = SD.open(filePaths_array[i++], FILE_READ);
                    delay(500);

                    //prints out the whole file to monitor and S1
                    while (file.available()) {
                        incoming = file.read();
                        delay(5);
                        //Serial.print((char) incoming);
                        Serial1.print((char) incoming);
                    }
                    file.close();
                }//if the button is pressed send a file to monitor and S1   
            } while (getKey() != CLEAR);

            break;
        default:
            IncorrectValueEntered();
            break;
    }
}//loop
