/* 
 * File:   ForwardDeclarations.h
 * Author: Jedan Garcia
 *
 * Created on July 1, 2013, 12:43 PM
 */

#ifndef FORWARDDECLARATIONS_H
#define	FORWARDDECLARATIONS_H
#include "MemoryFree.h"

//int freeMemory();

//Seperate Chaining
//class SeperateChaining;

//AR20
void AR20TimerFunction();
bool getPhBuffer();
void MeasuringAR20();
bool checkDifference(float first_measure, float second_measure,
        float epsilon_F);
void readPh();
void enableExitFlag();
float absolute_value(float number);

//RFM190
void RFM190TimerFunction();
String readFromBSBuffer(char first, String& buffer);
void refracRequestRead();
void refracRequestLast();
void refracRequestTemp();
void MeasuringRFM190();

//Button 
bool button1IsPressed();
void Blink(int pinNumber);
void redIndicatorBlink();
void greenIndicatorBlink();


//Get Serial
void getIncoming0();
void getIncoming1();
void getIncoming2();
void getIncoming3();
void getRFM190();
void getAR20();
String getIncomingString(HardwareSerial& Serial);
void printNewline(HardwareSerial& output, int amount);

//LCD
void lcd_print(byte location, String string);
void lcd_printChar(byte location, String character);
void lcd_clear();
void restoreBrightness();
void lcd_clearln(byte location);
void lcd_println(byte location, String string);
void lcd_printCharln(byte location, String string);
void setSplashScreenTo(byte location1,
        char* L1, byte location2, char* L2);
void resetLCDToDefault();
void lcd_print(byte location, const __FlashStringHelper* string);

//Load
void SetLoadNumber();
void decrementLoadNumber();
void incrementLoadNumber();
String getDateFromDateTime();
String getTimeFromDateTime();
void sendFileAndRemoveFromQueue(char* filepath, bool isRegrade);


//Queue functions
void sendFinishedQueuedLoads(bool isRegrade);


//Master Receiver
void getIncomingMeasures(HardwareSerial &SerialChosen);
void getRefracFromSerial(HardwareSerial &SerialChosen);
String readFromBSBufferSerial(char first, HardwareSerial &SerialChosen);
void getPhBufferFromSerial(HardwareSerial &SerialChosen);
void getIncomingFlag(HardwareSerial &SerialChosen, int flagIndex);
byte countRecievedFlags();
void getData(HardwareSerial &SerialChosen);
void masterReceiver();
void loadNumberExchange(HardwareSerial& SerialChosen, int SlaveLoadIndex);
void getLoadNumberFromSerial(HardwareSerial& SerialChosen, char* dummyLoadNumber);
String getMeasurement(HardwareSerial& serialChosen);
void sendToGradestar(SoftwareSerial& gradestar);
void sendToGradestar(SoftwareSerial& gradestar, File& file);
void getLoadNumberFromSerial(SoftwareSerial& SerialChosen, char* dummyLoad);



//Menu
void IncorrectValueEntered();
void printFourFromReference(byte frame, const String incoming_menu[]);
void Menu(String incoming_menu[], int incoming_size);
char* numberPad(byte location, byte max_size, char* destination_array);
byte Menu2Digits(const String incoming_menu[], int incoming_size);
void displayMenu(bool scrollable, String incoming_menu[], int incoming_size,
        int& frame);
void displayMenuMaster(bool scrollable, String incoming_menu[],
        int incoming_size, int& frame);
void printFourFromReference(byte frame, const __FlashStringHelper* incoming_menu[]);
byte mainMenu(const __FlashStringHelper* incoming_menu[], int incoming_size);
void MenuButton(String incoming_menu[], int incoming_size);

//Multiple Devices
void chooseSerialFromInput(HardwareSerial& ref);
void setFlagsFromInput();
void MeasuringMultiple();
void sendMultipleToMaster();
void sendMeasurements();


void sendAR20Flag();
void sendLedFlag();
void sendRFM190Flag();
void sendColorimeterFlag();
void sendVC505Flag();


void sendAR20();
void sendColorimeter();
void sendLed();
void sendRFM190();
void sendVC505();

void waitForConfirmation();
void waitForConfirmationFromPing();
bool refracInBounds();
bool phInBounds();
void NoMaster_func();

/******************************/



//debug
void SerialListen(HardwareSerial & SerialChosen);
void SerialWrite(HardwareSerial& SerialChosen, char character);
void debugSerialRead();
void debugSerialWrite();
void setRFMSerialTo();
void getPhSettings();
void EepromDefaultSetup();
void readValuesFromEEPROM();
void check_ph_settings();
void getRidOfSpaces(String& input);
bool weveReceivedAllMeasurements();
void clearMeasurementsReceived();
void getNextIncoming(SoftwareSerial& SerialChosen);
void LogFromXbee(char* filename);
void SerialFlush(HardwareSerial& chosenSerial);
void FlushAllSerial();
void turnIndicator(String choice);
bool lcd_print_long(String message);
void makeSampleQueueFile();
void setToFalse(bool array[], int size);
void listenTo(HardwareSerial& input);
void makeSureDateIsInTheFile(char* filepath);



//Hopper
void readFromVC505();
bool hopperIsDonePrinting();
bool startsWith;
int firstIndexOfLine(String array, int pos);
int lastIndexOfFloat(String array, int starting_pos);
void getMotType(String& category, HardwareSerial& serialChosen,
        String& hopperBuffer);
void getCategory(HardwareSerial& serialChosen, String& category,
        String& hopperBuffer);
void getNumberFromHopperBuffer(String& buffer, String& destination,
        int pos_of_b);
void sendVC505Flag();
String RemoveCategoryFromWeight(String weight);
bool MotIsDirt();
bool MotISBoth();
bool MotISExtra();
String getCategoryFromString(String input);
bool weightSyntaxRecognition(String input);
void customHopperName();
int askUserForDefectNumber();
String customizeStringByScrolling(byte location, String customString);
void HopperSettings();
char displayWeightFieldInfo(const __FlashStringHelper * Title, int weightFieldIndex);
int countWeightsExpected();
int countmasterWeightsExpected();
void readFromHopper();

//LedColorMeter

//this returns the color measurement that the LED thinks it send to gradestar
int readFromLedColorMeter();
void sendVC505Flag();


//xBee
//void masterXbeeSetup();
//void slaveXbeeSetup();
//void xbeeSpeaker();
//void xbeeListener();
//char* command_XBee(char* command);
//void command_XBee(char* command, char* value);
//char* getNextXToBuffer();
//void serialFlush();
//void startCommand();
//void stopCommand();
//char* stringCopy(char* destination, char* source);
//void individual_setup(); //setting up one slave with the master
//void checkXbeeSetting();
//void setXbeeToBroadcast();


//sd card functions
void recordLoadDataToSD();
String SyntaxParser(String syntax, File& file, bool isMot);
String readAfter(int x, int clear_amount, File& file);
String getLine(File file);
void readFromConfigFile(File file);
String getNextConfigValue(File file);
void createConfigFile();
void saveSerialToSD(HardwareSerial& inputSerial, char* filepath);
void getSerialXFrom(HardwareSerial& destinationSerial, char* filepath);
void saveCurrentConfiguration(int input_function);
void createMakeMe();
String readNextLoadValue(File& file);
void recursiveSDDelete(File dir, String ParentName);
void DeleteSDCard();
void createDefaultLoad(File& file);



//sampleFormats
void gradestarSampleMenu();
void SampleFormat(bool pH, bool comm, bool refrac, bool weights);

//SD Manipulation
bool loadFileIsDone(char* filePath, bool isRegrade);
bool checkWhichDataIsCompleted(char* filePath, bool loadIsRegrade);
int getRegradePosition(File& file);
int skipToRegradePosition(File& file);
void addToSD(char* filepath, String message, int commaPosition, bool isRegrade);
void addToSD(char* filepath, StringForSD messages[], bool isRegrade, int sizeOfArray);
void copyUntilCommaPosition(File& source, int endingCommaPosition, File& destination, bool isRegrade);
void copyFromCurrentUntilCommaPosition(File& source, int endingCommaPosition, File& destination, bool isRegrade);
void goToCommaPosition(int commaPosition, File& file);
void goToCommaPosition(int commaPosition, File& file, bool isRegrade);
String getValueFromCommaPosition(int commaPosition, File & file, bool isRegrade);
bool dataIsPresent(String value);
void copyRestOfFileTo(File& input, File& destination);
void copyWholeFileFromTo(File& source, char* filepath);
bool printFile(char* filePath, HardwareSerial& monitor);
void writeCommToFile(char* filepath, bool isRegrade);
void writeRefracToFile(char* filepath, bool isRegrade);
void writePhToFile(char* filepath, bool isRegrade);
void writeWeightsToFile(char* filepath, bool isRegrade);
String getMonthAndDayFromDate(String date);
String stringWithoutChar(String input, char target);
String getAppropriateFilename(char* filePath);

#endif	/* FORWARDDECLARATIONS_H */

