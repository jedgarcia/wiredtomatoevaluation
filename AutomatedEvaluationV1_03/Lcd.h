/* 
 * File:   Lcd.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 10:21 AM
 */

#ifndef LCD_H
#define	LCD_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"

//==============================================================================
//LCD FUNCTIONS

//This will clear the whole screen and use it all to write
//your message

bool lcd_print_long(String message) {
    lcd_clear();
    if (message.length() > 80)
        return false;
    else {
        if (message.length() > 20) {
            lcd_print(L1Ta, message.substring(0, 20));
        } else {
            lcd_print(L1Ta, message);
            return true;
        }

        if (message.length() > 40) {
            lcd_print(L2Ta, message.substring(20, 40));
        } else {
            lcd_print(L2Ta, message.substring(20, message.length()));
        }

        if (message.length() > 60) {
            lcd_print(L3Ta, message.substring(40, 60));
        } else {
            lcd_print(L3Ta, message.substring(40, message.length()));
        }

        lcd_print(L4Ta, message.substring(60, message.length()));

        return true;
    }
}


//prints to a specific part of the screen

void lcd_print(byte location, const __FlashStringHelper* string) {
    lcdSerial.write(TX_com);
    lcdSerial.write(location);
    lcdSerial.print(string);
}

void lcd_print(byte location, String string) {
    lcdSerial.write(TX_com);
    lcdSerial.write(location);
    lcdSerial.print(string);
}

void lcd_print(byte location, int string) {
    lcdSerial.write(TX_com);
    lcdSerial.write(location);
    lcdSerial.print(string);
}

void lcd_print(byte location, char string) {
    lcdSerial.write(TX_com);
    lcdSerial.write(location);
    lcdSerial.print(string);
}

void lcd_print(byte location, float string) {
    lcdSerial.write(TX_com);
    lcdSerial.write(location);
    lcdSerial.print(string);
}

//***************************************

void lcd_printChar(byte location, char character) {
    lcdSerial.write(TX_com);
    lcdSerial.write(location);
    lcdSerial.print(character);
}
//***************************************


//clears the whole screen

void lcd_clear() {
    lcdSerial.write(TX_com);
    lcdSerial.write(ClearLCD);
}
//***************************************

//clears a specific line 

void lcd_clearln(byte location) {
    lcdSerial.write(TX_com);
    lcdSerial.write(location);
    lcdSerial.print(F("                    "));
}
//***************************************

//Restores brightness and contrast to 100%

void restoreBrightness() {
    lcdSerial.write(B_com);
    lcdSerial.write(full_brightness);
    lcdSerial.write(display_on);


    lcd_clear();
    lcd_print(L2Ta, F("Brightness Restored!"));
    delay(750);
}

//***************************************

void lcd_println(byte location, String string) {
    lcd_clearln(location);
    lcd_print(location, string);
}

void lcd_println(byte location, const __FlashStringHelper* string) {
    lcd_clearln(location);
    lcd_print(location, string);
}

void lcd_printCharln(byte location, char character) {
    lcd_clearln(location);
    lcd_printChar(location, character);
}

void setSplashScreenTo(byte location1, char* l1, byte location2, char* l2) {

    lcd_clear();

    lcd_println(L1Ta + location1, l1);
    lcd_println(L2Ta + location2, l2);

    delay(3000);

    lcdSerial.write(124);
    lcdSerial.write(10);

}

void setSplashScreenTo(byte location1, const __FlashStringHelper* l1,
        byte location2, const __FlashStringHelper* l2) {

    lcd_clear();

    lcd_println(L1Ta + location1, l1);
    lcd_println(L2Ta + location2, l2);

    delay(3000);

    lcdSerial.write(124);
    lcdSerial.write(10);

}

void resetLCDToDefault() {
    setSplashScreenTo(0, F("        PTAB        "), 0, F("Instrument Interface"));
}

#endif	/* LCD_H */

