/* 
 * File:   RFM190.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 10:48 AM
 */

#ifndef RFM190_H
#define	RFM190_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"


//==============================================================================
//BELLINGHAM AND STANLEY REFRACTOMETER FUNCTIONS RFM190

//***************************************

void RFM190TimerFunction() { //this function is called when the timeout timer finishes counting down to 0 
    //if this function is called, it means that the rfm190 is taking too long to respond
    //and is being unresponsive
    doneWaiting = true; //this flag is global and will be checked in the calling function to see if it is unresponsive
}

//***************************************

String readFromBSBuffer(char first, String& buffer) { //this reads from the rfm190 Serial port to the given buffer - BS - bellingham and stanley
    //only the first 5 chars are important but there are 7 sent
    buffer = "";
    
    buffer += first;
    for (int i = 1; i < 7; i++) {
        while (1) {
            getRFM190();
            if (i >= 5 && incoming != NODATA) {
                break;
            }
            
            if (incoming != NODATA && i < 5) {
                buffer += (char) incoming;
                break;
            }//if getting buffer data
        }//while getting buffer
    }//for getting buffer	

    for (int i = 0; i < 10; i++)//flushing the rest of the info
        refracSerial.read();
    Serial.print("The buffer is: ");
    Serial.println(buffer);
    return buffer;
}

//***************************************

void refracRequestRead() { //This function sends D to rfm190 which means "send your last measured value"
    //The value returned by the refrac is then stored in refracBuffer

    Timer RFM190Timer; //This timer is used to check for an unresponsive rfm190
    RFM190Timer.after(7000, RFM190TimerFunction); //timer is set for 7 seconds

    //Sends DIGIREAD - 'R' - to rfm190
    refracSerial.write(DIGIREAD); //sends request to Read

    while (1) { //waits until a valid number is sent from the refrac to us

        RFM190Timer.update(); //this update function is how the timer keeps track of the 7 second countdown

        getRFM190(); //This simply stores whatever the rfm190 is spitting out into our global variable 'byte incoming;'

        if (doneWaiting) { //this flag is set automatically when RFM190Timer counted down, it signals an unresponsive rfm190
            Serial.println("Refrac is done waiting");
            //since the timer being able to finish means 7 seconds elapsed before the rfm190 sent a valid response
            refracUnresponsive = true; //this flag lets the calling function know that they should not look at the 
            doneWaiting = false; //this resets the flag so we can use it again later
            return;
        }


        if (incoming != NODATA) { //When serial devices are not communicating, the default value is 255, which is NODATA
            Serial.println(F("we are starting to get the refrac meas"));
            //if there is a valid response from rfm190, then read the rest of the message and store it all in refracBuffer
            readFromBSBuffer((char) incoming, refracBuffer);
            break; //break out of this while loop
        }//if valid data being received
    }//while(1)
}//refracRequestRead()

//***************************************

void refracRequestLast() { //this function asks the rfm190 for the last measurement it took - currently not used in the device
    if (!listenRefrac) getKey();
    if (key == 'B') {
        lcd_clearln(L4Ta); //gets rid of any other lingering value
        Serial3.write(DIGIRESEND); //sends 'R' for Read
        lcd_print(L3Ta, F("Previous value.."));

        while (1) { //waits until a valid number is sent
            //Serial.println("WAITING FOR INCOMING NUMBER");
            getIncoming3();

            if (incoming != NODATA) {
                readFromBSBuffer((char) incoming, refracBuffer);
                break;
            }//if incoming is valid data
        }//while looping to find valid data


        //print refracBuffer onto LCD
        lcd_clearln(L3Ta); // cleans up the "last value..."
        lcd_print(L4Ta, F("index_R = "));
        lcd_print(L4Ta + 11, refracBuffer);
    }//if user wants to read

}//refracRequestLast()

//***************************************

void refracRequestTemp() { //this function asks the rfm190 for the temperature it records, - currently not used in the device
    if (!listenRefrac) getKey();
    if (key == 'C') {
        lcd_clearln(L4Ta); //gets rid of any other lingering value
        // Serial.println("Temperature...");
        Serial3.write(DIGITEMPREAD); //sends 'R' for Read
        lcd_print(L3Ta, F("Temperature..."));

        while (1) { //waits until a valid number is sent
            //Serial.println("WAITING FOR INCOMING NUMBER");
            getIncoming3();

            if (incoming != NODATA) {
                readFromBSBuffer((char) incoming, refracBuffer);
                break;
            }//while waiting for valid number
        }//while waiting for valid number

        //print refracBuffer onto LCD
        lcd_clearln(L3Ta); // cleans up the "temperature..."
        lcd_print(L4Ta, F("Temp = "));
        lcd_print(L4Ta + 8, refracBuffer);
        lcd_print(L4Ta + 14, "'C");
    }//if user wants to read

}//refracRequestTemp()

//***************************************

void MeasuringRFM190() { //this is the function used to actually get a measurement from the rfm190
    //this function then uses sub-functions to fulfill the tast
    //the measurement is stored in refracBuffer

    //The refracSerial is set up to the correct Serial UART 
    //in another function that can be found in MultipleDevices.h
    refracSerial.begin(9600); //rfm190 uses 9600 baud rate
    refracSerial.flushBuffer();
    

    refracRequestRead(); // tells rfm190 to read the measurement and stores it in refracBuffer
    return;
}//measuringRFM190()

//***************************************

#endif	/* RFM190_H */

