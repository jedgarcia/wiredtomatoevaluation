/* 
 * File:   GlobalVariables.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 9:33 AM
 */

#ifndef GLOBALVARIABLES_H
#define	GLOBALVARIABLES_H


//EEPROM MEMORY ADDRESSES - the A is for Address
#define FIRST_BOOT_FLAG_A 0         //byte
#define PH_STABLE_TIMEOUT_A 1       //unsigned long int
#define PH_EPSILON_A 5              //float
#define PH_READING_RATE_A 9         //int
#define LOAD_NUMBER_A 11            //int


#define COLOR_SERIAL_A 0
#define PH_SERIAL_A 1
#define REFRAC_SERIAL_A  2
#define WEIGHT_SERIAL_A 3

SimpleVector<int> loadQueue(10);

int config_arduino_function = 0;
int stationCode = 0;

bool loadIsRegrade = false;

String pathToRegradeFile;


#define COLOR 1 
#define PH 3
#define REFRAC 2
#define WEIGHT 0

//1 - color 
//3 - pH
//2 - refrac
//0 - weight
bool SDContains[4] = {false, false, false, false};

//1 - color found after 12 commas
//3 - pH found after 14 commas
//2 - refrac found after 13 commas
//0 - weight found after 4 commas
byte SDLocation[4] = {4, 12, 13, 14};

//==============================================================================
#define TRUE '1'
#define FALSE '0'

//'G' when sent to another Arduino specifies that a message was received
//'G' stands for Good, as in "That last transmission was Good".
#define CONFIRMATION 'G'

// 'R' when sent to another Arduino queries for a measurement of the
// instrument the Arduino is connected to
#define READ 'R'

//'S' can be sent from slave to master as a request to listen to the measures
//it is about to send
#define SENDING 'S'

//NODATA is NODATA because that is the default value that comes in through
//The serial connection when a signal isn't being sent
#define NODATA 255


//when the user presses this key on the keypad, they mean to do one measurement
//without sending the information to the master Arduino or the PTAB database
#define TESTING '?'

//Ping - checks to see if the master is connected
#define PING 'P'

bool LEDHasBeenMeasured = false;
bool VC505HasBeenMeasured = false;
bool noMaster = false;

//Keypad non-numericals
#define CLEAR '*'
#define ENTER '$'
#define UP '<'
#define DOWN '>'
#define HELP '?'
#define SECOND 'T'


//Device Listening Flags
bool listenPh = false;
bool phUnresponsive = false;
bool listenAR20 = false;

bool listenColor = false;
bool colorUnresponsive = false;
bool listenLED = false;

bool listenRefrac = false;
bool refracUnresponsive = false;
bool listenRFM190 = false;

bool listenWeight = false;
bool weightUnresponsive = false;
bool listenVC505 = false;

#define MEASURING_MODE 2
#define MASTER_MODE 1
#define MAIN_MENU_MODE 0



//Serial flags for the master to know when to respond while in the menu
//These are so that when another device goes into measuring mode and pings
bool SerialTryingToSend[4] = {false, false, false, false};

#define DATE 0
#define TIME 1
#define HOPPERWT 2
#define WORM 3
#define MOLD 4
#define GREEN 5
#define MOT 6
#define LU 7
#define SPARE_1 8
#define SPARE_2 9

#define BUTTONNUMBER 80

int maximumHopperEntries = 10;
String weightFields[] = {"DATE",
    "TIME",
    "SAMPLE WT.",
    "WORM",
    "MOLD",
    "GREEN",
    "MOT",
    "LU",
    "PEELER",
    "ALL"};

bool weightFieldIsOn[10] = {true, true, true, true, true, true, true, true, true, true};
bool masterWeightIsOn[10] = {true, true, true, true, true, true, true, true, true, true};

int spareCustomDefectNumber[2] = {0, 0};
String spareCustomName[2] = {"SE 1", "SPARE 2"};


//Always in alphabetical order
//ReceivingDevicesFlags[0] = 0; //Color Flag
//ReceivingDevicesFlags[1] = 0; // pH Flag
//ReceivingDevicesFlags[2] = 0; //Refrac Flag
//ReceivingDevicesFlags[3] = 0; //Weight Flag
#define  ReceivingDevicesSize  4    //how many devices there are
bool ReceivingDevicesFlags[4]; //all initialized to false in setup
bool expectedDevices[13]; //all are initialized to true - done in setup


//flag that when set signals the user wants to go back to exit to the main menu
bool main_menu_flag = false;

//flag denotes user does not want measurement data sent to master Arduino or DB
bool testing_dont_send = false;


//Timer Variables

Timer blinkTimer;
int blinkDuringOngoing;
bool CurrentlyBlinking = false;


bool doneWaiting = false;

Timer masterPing;
Timer scanTimer;
int noMaster_t;
int indi_blink;
int masterScan;

//This gets the date and time from the RTC
//it is beneficial to have a global one so that we can keep using one 
//time throughout the code for consistent times
DateTime now;

//Variable Serial ports corresponding to different devices
//The following lines require a modified version of the
//Arduino's HardwareSerial library located in:
// arduino\hardware\arduino\cores\arduino\HardwareSerial.cpp and
// HardwareSerial.h
HardwareSerial phSerial(true); // = new HardwareSerial(true);
HardwareSerial refracSerial(true); // = new HardwareSerial(true);
HardwareSerial weightSerial(true); // = new HardwareSerial(true);
HardwareSerial colorSerial(true); //= new HardwareSerial(true);

RTC_DS1307 RTC; //this is the type of RTC we got, so you must declare it

//Led Color Meter Variables
String colorBuffer; //this is allocated space when needed in order to reduce memory strain
int LedColor;


//load number
unsigned int LoadNumber = 0;
char loadNumberAscii[6];
char loadNumberitoa[6];


//This determines how many times the menu for choosing devices to measure
//from is displayed, this determines how many devices can hook up to the arduino,
//Right now it is 3 devices, so menuCounter is set to 3 somewhere else in the codes
int menuCounter;


//serial4                               RX, TX 
#define rxMasterPin  A15
#define txMasterPin  A14
//SoftwareSerial masterAndGradestarSerial(6, 7); //this is the connection to the master or to gradestar

SoftwareSerial masterAndGradestarSerial(rxMasterPin, txMasterPin); //this is the connection to the master or to gradestar

//pH settings
char rate[3] = {"3"}; //seconds
char epsilon[6] = {"0.005"}; //difference in measures
char timeout[3] = {"30"}; //seconds


//Menu
//This has all the menu options under an array of strings
#define MAINMENUSIZE 14
const __FlashStringHelper* main_menu[MAINMENUSIZE];

char test_c[25];
String test_s;
String test_s2;


byte DMENUSIZE = 7;
const __FlashStringHelper* device_menu[7];

String measurements_menu[14]; //right now only 12 possible lines - gotta include the load number

int measurements_menu_size = 1;
int menu_frame = 0;
bool measureFlag = false;

byte LOADNUMBERLINE = 0;
byte PHLINE = 1;
byte COLORLINE = 2;
byte REFRACLINE = 3;
byte WEIGHTLINE = 4;


//buffer used for pH
String phBuffer = "";
long unsigned int PH_STABLE_TIMEOUT;
int PH_READING_RATE;
float PH_EPSILON;

bool ExitFlag = false; //used in the while(1) loop in MeasuringAR20 to determine when to stop measuring
float* phReadings; // this is eventually allocated to an array float[50] and erased so preserve memory
int phReadingCount = 0; // this is the index counter to phReadings

//==============================================================================

//Refractometer values
#define DIGIREAD 'R'
#define DIGITEMPREAD 'T'
#define DIGIRESEND 'D'

//refractometer Buffer 
int const refrac_buffer_size = 7;
String refracBuffer = "";

//==============================================================================
//Hopper - VC505
String weightBuffers[10];
byte weightBuffers_size = 0;
//==============================================================================
#define L1  0
#define L2  64
#define L3  20
#define L4  84

byte incoming = NODATA;
byte counter = 0;
char key;
byte arduinoFunction = 0;
byte flagSet = 0;
//==============================================================================
//MajorButton

#define button1Pin  44
#define buttonLED  46
#define FAST_BLINK 50
#define MEDIUM_BLINK 150
#define SLOW_BLINK 350

//Indicator Light
#define indicatorRed 50
#define indicatorGreen 48


#define dsrPin1  27
#define dsrPin2  26
#define dsrPin3  34

//==============================================================================
//LCD GLOBAL
#define lcdPin  42 // TX digital pin for software serial
SoftwareSerial lcdSerial = SoftwareSerial(lcdPin, lcdPin); //Setting up software Serial for LCD

#define B_com  128 // you must send this to the screen before giving it a brightness value
#define full_brightness 255 // screen with 100% brightness
#define display_on  12

#define ClearLCD 1 // command to clear the lcd screen
#define TX_com  254 //command to tell LCD to listen
#define TX_idi  128 //number to add in order to make two's complement

#define L1Ta  128 //first position for line one of  char/numbs  for field a
#define L2Ta  192 //first position for line two of  char/numbs  for field a
#define L3Ta  148 //first position for line three of char/numbs for field a
#define L4Ta  212 //first position for line four  of char/numbs for field a

byte lines[4] = {L1Ta, L2Ta, L3Ta, L4Ta};

//==============================================================================
//KEYBOARD GLOBAL

const byte ROWS = 4; // Four rows
const byte COLS = 4; // For columns
char keys[ROWS][COLS] = //Define keys for keypad, i.e. Keymap
{
    {
        '1', '2', '3', '<'
    }
    ,
    {
        '4', '5', '6', '>'
    }
    ,
    {
        '7', '8', '9', 'T'
    }
    ,
    {
        '*', '0', '?', '$'
    }
}; // Keypad object created called "kpd"
byte rowPins[ROWS] = {
    35, 37, 39, 41
}; //C0 C1 C2 C3 to Mega pins
byte colPins[COLS] = {
    43, 45, 47, 49
}; //R0 R1 R2 R3 to Mega pins
Keypad pad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

//This tells the master how many of the current load's measurements it has received
#define NUMBEROFMEASUREMENTS 11
bool measurementsReceived[NUMBEROFMEASUREMENTS];

bool AlreadySentMyMeasurements = false;

void clearMeasurementsReceived() {
    for (int i = 0; i < NUMBEROFMEASUREMENTS; i++) {
        measurementsReceived[i] = false;
    }
}

bool weveReceivedAllMeasurements() {
    for (int i = 0; i < NUMBEROFMEASUREMENTS; i++) {
        if (!measurementsReceived[i] && expectedDevices[i]) {
            Serial.print(F("Have not received measurement: "));
            Serial.println(i);
            return false;
        }
    }
    return true;
}

char getKey() {
    key = pad.getKey();
    return key;
}

void clearListenFlags() {
    //Device Listening Flags
    listenPh = false;
    listenRefrac = false;
    listenColor = false;
    listenWeight = false;

    listenRFM190 = false;
    listenAR20 = false;
    listenVC505 = false;
    listenLED = false;
}



#define IND_SETUP 'C'

#endif	/* GLOBALVARIABLES_H */

