/* 
 * File:   Button.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 10:28 AM
 */

#ifndef BUTTON_H
#define	BUTTON_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"



//==============================================================================
//BUTTON FUNCTIONS

bool button1IsPressed() {//checks if the measurement button is pressed
    if (digitalRead(button1Pin) == LOW)
        return true;
    return false;
}

bool blink_flag = false; //this flag is used to alternate between on and off

//This function is meant to be called from a timer every x milliseconds as to
//look like the led if blinking, by calling this function using a timer 
// we can blink and simulatenously do two things

void Blink(int pinNumber) {
    if (blink_flag) {
        digitalWrite(pinNumber, HIGH);
        blink_flag = false;
    } else {
        digitalWrite(pinNumber, LOW);
        blink_flag = true;
    }
};

void redIndicatorBlink() {
    digitalWrite(indicatorGreen, LOW);

    if (blink_flag) {
        digitalWrite(indicatorRed, HIGH);
        blink_flag = false;
    } else {
        digitalWrite(indicatorRed, LOW);
        blink_flag = true;
    }
}

void greenIndicatorBlink() {
    digitalWrite(indicatorRed, LOW);

    if (blink_flag) {
        digitalWrite(indicatorGreen, HIGH);
        blink_flag = false;
    } else {
        digitalWrite(indicatorGreen, LOW);
        blink_flag = true;
    }

}

void startBlinkingEvent(void (*function) (void)) {//takes a function that is passed onto the function
    if (!CurrentlyBlinking) {
        CurrentlyBlinking = true;
        blinkDuringOngoing = blinkTimer.every(200, function);
    }
    
}

void stopBlinkingEvent() {//takes a function that is passed onto the function
    if (CurrentlyBlinking) {
        CurrentlyBlinking = false;
        blinkTimer.stop(blinkDuringOngoing);
    }
}

#endif	/* BUTTON_H */

