/* 
 * File:   HopperScanner.cpp
 * Author: Chris
 * 
 * Created on September 25, 2013, 1:26 PM
 */

#include "HopperScanner.h"


//This function will read all of the weight output and put them into the correct
//index in Fields[], after we are done reading then we will check to see what type
//of mot it was

void HopperScanner::ReadHopperWeights() {

    bool gotDate = false;
    bool gotTime = false;
    bool gotLoad = false;

    int fieldCounter = 0;
    int expectedNumberOfFields = countWeightsExpected();
    Serial.print("The expected number of fields is: ");
    Serial.println(expectedNumberOfFields);

    while (fieldCounter < expectedNumberOfFields) {
        String HopperLine = getNextWeightLine();

        if (HopperLine.length() > 5) {//not an empty line
            //Serial.println(HopperLine);

            if (!gotDate && weightFieldIsOn[DATE] && isDate(HopperLine)) {
                HopperLine.trim();
                Fields[DATE] = "DATE: ";
                Fields[DATE] += correctDateFormat(HopperLine);
                gotDate = true;
                fieldCounter++;
                //Serial.print("Date was: ");
                //Serial.println(Fields[DATE]);
                continue;
            } else if (!gotTime && weightFieldIsOn[TIME] && isTime(HopperLine)) {
                HopperLine.trim();
                Fields[TIME] = "TIME: ";
                Fields[TIME] += correctTimeFormat(HopperLine);
                gotTime = true;
                fieldCounter++;
                //Serial.print("time was: ");
                //Serial.println(Fields[TIME]);
                continue;
            } else if (!gotLoad && isLoadNumber(HopperLine)) {
                gotLoad = true;
                continue;
            }

            String Category = getCategoryFromString(HopperLine);
            String Measurement = getMeasurementFromString(HopperLine);

            //The format that we want this saved as is Category: data
            String tempField = Category;
            tempField += ": ";
            tempField += Measurement;

            tempField.trim();
            //Then we put it in the right place
            if (weightsyntaxRecognition(tempField)) {
                fieldCounter++;
            }
            //Serial.println();
            //Serial.print("The field is: ");
            //Serial.println(tempField);
            // Serial.println();
        }
        //Serial.println(fieldCounter);
    }//reading in all fields

    //now we have to check what type of mot we got - if we got mot
    motIsBoth = motIsDirt = motIsExtra = false;
    if (weightFieldIsOn[MOT]) {
        motIsBoth = fieldIsBoth(Fields[MOT]);
        motIsDirt = fieldIsDirt(Fields[MOT]);
        motIsExtra = fieldIsExtra(Fields[MOT]);
    }
    delay(500);
    while (hopperSerial->available()) {
        Serial.println("Flushing");
        Serial.print(hopperSerial->read());
        delay(20);
    }
}//reading all weights

String HopperScanner::correctDateFormat(String date) {

    int firstSlashPosition = 0;
    while (date[firstSlashPosition++] != '/');

    int secondSlashPosition = firstSlashPosition;
    while (date[secondSlashPosition++] != '/');


    String Month = "";
    for (int i = 0; i < firstSlashPosition; i++)
        Month += date[i];

    String Day = "";
    for (int i = firstSlashPosition; i < secondSlashPosition; i++) {\
        Day += date[i];
    }

    String Year = "";
    for (int i = secondSlashPosition; i < date.length(); i++)
        Year += date[i];

    //Remove all whitespace
    Month.trim();
    //Serial.println(Month);

    Day.trim();
    //Serial.println(Day);
    Year.trim();
    //Serial.println(Year);

    if (Year.toInt() < 2000) {
        int year_int = Year.toInt();
        Year = "";
        Year += year_int + 2000;
    }

    if (Month.toInt() < 10 && Month.length() < 2)
        Month = String("0") + Month;
    if (Day.toInt() < 10 && Day.length() < 2)
        Day = String("0") + Day;
    if (Year.toInt() < 10 && Year.length() < 2)
        Year = String("0") + Year;

    String correctDate = Month;
    correctDate += Day;
    correctDate += Year;

    return correctDate;
}

bool HopperScanner::timeIsPm(String time) {
    for (int i = 0; i < time.length(); i++) {
        if (time[i] == 'P' || time[i] == 'p')
            return true;
    }
    return false;
}

String HopperScanner::correctTimeFormat(String time) {

    time.trim();
    if (timeIsPm(time)) {
        String hour_s = "";
        int i = 0;
        while (time[i] != ':') {
            hour_s += time[i++];
        }
        int colonPosition = i;
        int hour = hour_s.toInt(); //to convert to military time
        if (hour != 12)
            hour += 12;


        hour_s = "";
        hour_s += hour;

        for (int i = colonPosition; i <= colonPosition + 2; i++) {
            hour_s += time[i];
        }

        if (hour_s.length() < 8)//if the time doesn't have a seconds part
            hour_s += ":00";

        //Serial.println("this is the time afternoon");
        //Serial.println(hour_s);

        return hour_s;

    } else {//it is morning time

        String hour_s = "";
        int i = 0;

        while (time[i] != ':') {
            hour_s += time[i++];
        }

        int colonPosition = i;
        int hour = hour_s.toInt(); //to convert to military time
        if (hour == 12)
            hour -= 12;

        hour_s = "";
        if (hour < 10)
            hour_s += "0";
        hour_s += hour;
        for (int i = colonPosition; i <= colonPosition + 2; i++) {
            hour_s += time[i];
        }

        if (hour_s.length() < 8)//if the time doesn't have a seconds part
            hour_s += ":00";

        //Serial.println("this is the time afternoon");
        //Serial.println(hour_s);

        return hour_s;
    }
}

String HopperScanner::getNextWeightLine() {
    String buffer = "";
    while (true) {//keeps adding to the buffer while the incoming data is not \n
        incoming = hopperSerial->read();

        if (incoming != NODATA) {//only add to the buffer if the incoming is valid
            buffer += (char) incoming;

            //if the last thing added to the buffer was a new line then quit and return the buffer
            if (incoming == 10) {
                return buffer;
            }
        }
    }//while(true)
}//getnexWeightLine

String HopperScanner::getDate() {
    return Fields[DATE];
}

String HopperScanner::getTime() {
    return Fields[TIME];
}

String HopperScanner::getHopperWeight() {
    return Fields[HOPPERWT];
}

String HopperScanner::getWorm() {
    return Fields[WORM];
}

String HopperScanner::getMold() {
    return Fields[MOLD];
}

String HopperScanner::getGreen() {
    return Fields[GREEN];
}

String HopperScanner::getMot() {
    return Fields[MOT];
}

String HopperScanner::getLimitedUse() {
    return Fields[LU];
}

String HopperScanner::getOtherDefect1() {
    return Fields[SPARE_1];
}

String HopperScanner::getOtherDefect2() {
    return Fields[SPARE_2];
}

String HopperScanner::getField(int field) {
    return Fields[field];
}

String HopperScanner::getCategoryFromString(String& input) {
    String buffer = "";
    int i = 0;
    while (input[i++] != 'b'); //skips forward until the b

    //This puts the category into the buffer
    for (; i < input.length(); i++) {
        buffer += input[i];
    }

    buffer.trim(); //remove whitespace that is trailing or leading

    return buffer;
}

String HopperScanner::getMeasurementFromString(String& input) {
    String buffer = "";
    int i = 0;
    do {
        buffer += input[i++];
    } while (input[i] != 'l' || input[i] == 'L');

    buffer.trim(); //remove whitespace that is trailing or leading

    return buffer;
}

bool HopperScanner::isLoadNumber(String& input) {
    input.toUpperCase();
    String LoadSyntax = "LOAD";
    for (int i = 0; i < input.length(); i++) {
        if (input.substring(i).startsWith(LoadSyntax))
            return true;
    }
    return false;
}

bool HopperScanner::isDate(String& input) {
    for (int i = 0; i < input.length(); i++) {
        if (input[i] == '/')
            return true;
    }
    return false;
}

bool HopperScanner::isTime(String& input) {
    for (int i = 0; i < input.length(); i++) {
        if (input[i] == ':')
            return true;
    }
    return false;
}

//this will place the measurements into their desired locations

bool HopperScanner::weightsyntaxRecognition(String& input) {

    //We are assuming that the string incoming has a category attached to to it
    //The formatincoming is like this: "Category: (data), e.g. : WORM: 1.24

    input.toUpperCase();

    if (input.startsWith(weightFields[DATE]) && weightFieldIsOn[DATE]) {//date
        Fields[0] = input;
        return true;
    } else if (input.startsWith(weightFields[TIME]) && weightFieldIsOn[TIME]) {//time
        Fields[1] = input;
        return true;
    } else if (input.startsWith(weightFields[HOPPERWT]) && weightFieldIsOn[HOPPERWT]) {//hopper wt
        Fields[2] = input;
        return true;
    } else if (input.startsWith(weightFields[WORM]) && weightFieldIsOn[WORM]) {//WORM
        Fields[3] = input;
        return true;
    } else if (input.startsWith(weightFields[MOLD]) && weightFieldIsOn[MOLD]) {//MOLD
        Fields[4] = input;
        return true;
    } else if (input.startsWith(weightFields[GREEN]) && weightFieldIsOn[GREEN]) {//GREEN
        Fields[5] = input;
        return true;
    } else if (input.startsWith(weightFields[MOT]) && weightFieldIsOn[MOT]) {//MOT
        Fields[6] = input;
        return true;
    } else if (input.startsWith(weightFields[LU]) && weightFieldIsOn[LU]) {//LU
        Fields[7] = input;
        return true;
    } else if (input.startsWith(weightFields[SPARE_1]) && weightFieldIsOn[SPARE_1]) {//SPARE 1
        Fields[8] = input;
        return true;
    } else if (input.startsWith(weightFields[SPARE_2]) && weightFieldIsOn[SPARE_2]) {//SPARE 2
        Fields[9] = input;
        return true;
    }

    Serial.print("This field could not be put into a categroy");
    Serial.println(input);
    return false;

}//weight syntax recognition

int HopperScanner::countWeightsExpected() {
    int counter = 0;
    for (int i = 0; i < 10; i++) {
        if (weightFieldIsOn[i])
            counter++;
    }
    return counter;
}

bool HopperScanner::fieldIsBoth(String& input) {

    int length = input.length();
    for (int i = 0; i < length; i++) {
        if (input.charAt(i) == 'b' ||
                input.charAt(i) == 'B') {
            return true;
        }
    }
    return false;
}

bool HopperScanner::fieldIsExtra(String& input) {
    int length = input.length();
    for (int i = 0; i < length; i++) {
        if (input.charAt(i) == 'e' ||
                input.charAt(i) == 'E') {
            return true;
        }
    }
    return false;
}

bool HopperScanner::fieldIsDirt(String& input) {

    int length = input.length();
    for (int i = 0; i < length; i++) {
        if (input.charAt(i) == 'D' ||
                input.charAt(i) == 'd') {
            return true;
        }
    }
    return false;
}