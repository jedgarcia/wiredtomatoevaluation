/* 
 * File:   LedColorMeter.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on July 23, 2013, 10:44 AM
 */

#ifndef LEDCOLORMETER_H
#define	LEDCOLORMETER_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"


//This just reads the gradestar output and parses out the color part
//Really dont worry too much about this because it is not done yet

int readFromLedColorMeter() {

    while (1) {//reads the whole color meter message

        if (colorSerial.available()) {//if there is a byte in the buffer read it
            incoming = colorSerial.read();
            colorBuffer += (char) incoming;

            //There is only one P in the message from the led to gradestar, which 
            //is when it is stating the ph, which is out flag to stop listening
            if (incoming == 'P') {
                while (colorSerial.available())
                    colorSerial.read();
                break;
            }

        }
    }

    char colorMeasurement[3] = {'\0'};

    //by this point we have gotten all we need in the colorBuffer
    int stringLength = colorBuffer.length();
    for (int i = 0; i < stringLength; i++) {
        if (colorBuffer[i] == 'C' &&
                colorBuffer[i + 1] == 'O' &&
                colorBuffer[i + 2] == 'L' &&
                colorBuffer[i + 3] == 'O' &&
                colorBuffer[i + 4] == 'R' &&
                colorBuffer[i + 5] == '=') {//were at the part of the string that reads COLOR=XX,
            colorMeasurement[0] = colorBuffer[i + 6];
            colorMeasurement[1] = colorBuffer[i + 7];
            colorMeasurement[2] = '\0';
        }
    }



    //need to convert from ascii int
    return LedColor = atoi(colorMeasurement);

}//while(1)




#endif	/* LEDCOLORMETER_H */

