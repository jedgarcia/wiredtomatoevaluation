/* 
 * File:   SimpleVector.h
 * Author: Chris
 *
 * Created on September 13, 2013, 1:40 PM
 */

#ifndef SIMPLEVECTOR_H
#define	SIMPLEVECTOR_H
#include <iostream>

using namespace std;

template<class t> class SimpleVector {
    //Some of the usage is inefficient, but it should be negligeable due to the
    //small expect size of these Vectors - meant to be used as a queue for loads
    //There are no duplicates
public:
    SimpleVector(int inputSize);
    ~SimpleVector();
    int push(t input); //pushes data onto vector, returns its position
    t& get(int pos); //returns a reference to the data
    int getSize(); //returns the size of the vector
    void remove(int position);
    bool thereExists(t data);
    int positionOf(t data);
    void removeData(t data);
    

private:
    //functions
    void removeGaps();
    void reSort(); //this puts the entries in order least to greatest without gaps

    //variables
    int maxSize;
    int size;
    bool* empty;
    t* array;
};

template<class t>
SimpleVector<t>::SimpleVector(int inputSize) {
    maxSize = inputSize;
    size = 0;
    array = new t[maxSize];
    empty = new bool[maxSize];
    for (int i = 0; i < maxSize; i++) {
        empty[i] = true;
    }
}

template<class t>
SimpleVector<t>::~SimpleVector() {
    delete array;
    delete empty;
}

template<class t>
int SimpleVector<t>::push(t input) {
    //puts the data into a spot, and then it removes gaps and sorts
    //if (size >= maxSize) return 0;
    if (input <= 0)//this is a queue for loads, a load cannot be 0
        return 0;
    if (thereExists(input))
        return 0;

    for (int i = 0; i < maxSize; i++) {
        if (empty[i]) {
            size++;
            empty[i] = false;
            array[i] = input;
            //removeGaps();
            reSort();
            return 0;
        }
    }


}

template<class t>
void SimpleVector<t>::reSort() {
    int i, j, tmp;
    for (i = 1; i < size; i++) {
        j = i;
        while (j > 0 && array[j - 1] > array[j]) {
            tmp = array[j];
            array[j] = array[j - 1];
            array[j - 1] = tmp;
            j--;
        }//end of while loop
    }//end of for loop
}//end of insertion_sort.

template<class t>
void SimpleVector<t>::removeGaps() {//don't judge how this is n^2, remember its only for 10 or so elements

    t arrayTemp[maxSize];
    int tempIt = 0;

    //puts all elements into a temp array without gaps
    for (int i = 0; i < maxSize; i++) {
        if (!empty[i]) {
            arrayTemp[tempIt++] = array[i];
        }
    }

    //puts that same info gapless array back into the original array
    //clears empty
    for (int i = 0; i < maxSize; i++) {
        array[i] = arrayTemp[i];
        empty[i] = true;
    }

    for (int i = 0; i < tempIt; i++) {
        empty[i] = false; //makes sure that we update which ones are empty
    }


}

template<class t>
t& SimpleVector<t>::get(int pos) {
    t empty = 0;
    if (pos >= size)
        return empty;
    
    return array[pos];
}

template<class t>
int SimpleVector<t>::getSize() {
    return size;
}

template<class t>
void SimpleVector<t>::remove(int pos) {
    empty[pos] = true;
    --size;
    array[pos] = 0;
    removeGaps();
}

template<class t>
bool SimpleVector<t>::thereExists(t data) {
    for (int i = 0; i < size; i++) {
        if (array[i] == data) return true;
    }
    return false;
}

template<class t>
int SimpleVector<t>::positionOf(t data) {
    for (int i = 0; i < size; i++) {
        if (array[i] == data) return i;
    }
    return -1;
}

template<class t>
void SimpleVector<t>::removeData(t data) {
    remove(positionOf(data));
}

#endif	/* SIMPLEVECTOR_H */

