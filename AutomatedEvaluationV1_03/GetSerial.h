/* 
 * File:   GetSerial.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 10:26 AM
 */

#ifndef GETSERIAL_H
#define	GETSERIAL_H



//==============================================================================
//getIncoming Functions


//***************************************
void getIncoming0() {
    //incoming is a global variable
    incoming = Serial.read();
}
//***************************************

void getIncoming1() {
    //incoming is a global variable
    incoming = Serial1.read();
}
//***************************************

void getIncoming2() {
    //incoming is a global variable
    incoming = Serial2.read();
}
//***************************************

void getIncoming3() {
    //incoming is a global variable
    incoming = Serial3.read();
}
//***************************************

void getRFM190() {
    //incoming is a global variable
    incoming = refracSerial.read();
}
//***************************************

void getAR20() {
    //incoming is a global variable
    incoming = phSerial.read();
    if(incoming != NODATA)
        Serial.print((char)incoming);
}
//==============================================================================



#endif	/* GETSERIAL_H */

