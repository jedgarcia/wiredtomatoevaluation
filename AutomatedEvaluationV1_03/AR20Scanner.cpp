/* 
 * File:   AR20Scanner.cpp
 * Author: Chris
 * 
 * Created on September 26, 2013, 5:46 PM
 */

#include "AR20Scanner.h"


String AR20Scanner::getPh() {
    unresponsive = false;
    String phLine = "";
    String startOfPhLineSyntax = "pH measurement:";
    AR20Serial->flushBuffer();

    int counter = 0; //This will count how many lines we've had to go through to get the ph
    while (1) {
        phLine = getNextLine();
        counter++;
        if (phLine.startsWith(startOfPhLineSyntax)) {
            String ph = phLine.substring(15, phLine.length());
            ph.trim();
            return ph;
        }

        if (counter > 100) {
            unresponsive = true;
            String unresponsive = "unresponsive";
            return unresponsive;
        }

    }

}

String AR20Scanner::getNextLine() {
    String buffer = "";
    String empty = " ";
    byte incoming;

    int count = 0;

    while (true) {
        count++;
        if (AR20Serial->available()) {
            incoming = AR20Serial->read();

            if (incoming == 10 || incoming == '\r' || incoming == '\n') {
                return buffer;
            } else {
                buffer += (char) incoming;
            }
        }
        if (count > 20000) {
            return String("");
        }

    }

}
