/* 
 * File:   Algorithms.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on September 20, 2013, 10:25 AM
 */

#ifndef ALGORITHMS_H
#define	ALGORITHMS_H

template <typename t>
void insertionSort(t array[], int size) {
    int i, j;
    t tmp;
    for (i = 1; i < size; i++) {
        j = i;
        while (j > 0 && array[j - 1] > array[j]) {
            tmp = array[j];
            array[j] = array[j - 1];
            array[j - 1] = tmp;
            j--;
        }//end of while loop
    }//end of for loop
}


#endif	/* ALGORITHMS_H */

