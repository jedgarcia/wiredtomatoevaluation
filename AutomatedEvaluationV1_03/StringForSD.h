/* 
 * File:   StringForSD.h
 * Author: Chris
 *
 * Created on September 20, 2013, 10:13 AM
 */

#ifndef STRINGFORSD_H
#define	STRINGFORSD_H
#include <Arduino.h>

class StringForSD {
public:
    StringForSD(String message_in, int commaPosition_in);
    StringForSD(char* message_in, int commaPosition_in);
    StringForSD();
    
    void add(String message_in, int commaPosition_in);
    void add(char*  message_in, int commaPosition_in);
    
    void trim();

    StringForSD(const StringForSD& orig);

    String message;
    int commaPosition;

    //overloading operators

    inline bool operator<(const StringForSD& rhs) {
        return commaPosition < rhs.commaPosition;
    }

    inline bool operator>(const StringForSD& rhs) {
        return commaPosition > rhs.commaPosition;
    }

    inline bool operator==(const StringForSD& rhs) {
        return commaPosition == rhs.commaPosition;
    }

    inline bool operator<=(const StringForSD& rhs) {
        return commaPosition <= rhs.commaPosition;
    }

    inline bool operator>=(const StringForSD& rhs) {
        return commaPosition >= rhs.commaPosition;
    }

    inline bool operator=(const StringForSD& rhs) {
        message = rhs.message;
        commaPosition = rhs.commaPosition;
    }

    operator bool() {
        return commaPosition;
    }




};

#endif	/* STRINGFORSD_H */

