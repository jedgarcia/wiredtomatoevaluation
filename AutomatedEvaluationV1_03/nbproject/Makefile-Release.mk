#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=avr-gcc
CCC=avr-g++
CXX=avr-g++
FC=gfortran
AS=avr-as

# Macros
CND_PLATFORM=Arduino-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1580121393/AR20Scanner.o \
	${OBJECTDIR}/_ext/1580121393/HopperScanner.o \
	${OBJECTDIR}/_ext/1580121393/MemoryFree.o \
	${OBJECTDIR}/_ext/1580121393/SimpleVector.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/automatedevaluationv1_03.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/automatedevaluationv1_03.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/automatedevaluationv1_03 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/1580121393/AR20Scanner.o: /cygdrive/C/Users/Chris/Desktop/wiredtomatoevaluation/AutomatedEvaluationV1_03/AR20Scanner.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1580121393
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1580121393/AR20Scanner.o /cygdrive/C/Users/Chris/Desktop/wiredtomatoevaluation/AutomatedEvaluationV1_03/AR20Scanner.cpp

${OBJECTDIR}/_ext/1580121393/HopperScanner.o: /cygdrive/C/Users/Chris/Desktop/wiredtomatoevaluation/AutomatedEvaluationV1_03/HopperScanner.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1580121393
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1580121393/HopperScanner.o /cygdrive/C/Users/Chris/Desktop/wiredtomatoevaluation/AutomatedEvaluationV1_03/HopperScanner.cpp

${OBJECTDIR}/_ext/1580121393/MemoryFree.o: /cygdrive/C/Users/Chris/Desktop/wiredtomatoevaluation/AutomatedEvaluationV1_03/MemoryFree.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1580121393
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1580121393/MemoryFree.o /cygdrive/C/Users/Chris/Desktop/wiredtomatoevaluation/AutomatedEvaluationV1_03/MemoryFree.cpp

${OBJECTDIR}/_ext/1580121393/SimpleVector.o: /cygdrive/C/Users/Chris/Desktop/wiredtomatoevaluation/AutomatedEvaluationV1_03/SimpleVector.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1580121393
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1580121393/SimpleVector.o /cygdrive/C/Users/Chris/Desktop/wiredtomatoevaluation/AutomatedEvaluationV1_03/SimpleVector.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/automatedevaluationv1_03.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
