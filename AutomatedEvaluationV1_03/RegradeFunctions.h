/* 
 * File:   RegradeFunctions.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on August 27, 2013, 1:48 PM
 */

#ifndef REGRADEFUNCTIONS_H
#define	REGRADEFUNCTIONS_H

#include "ForwardDeclarations.h"
#include "GlobalVariables.h"

void regrade() {
    //This is when they want to re measure a load from a certain date
    char month_c[] = {"  "};

    char day_c[3] = {"  "};

    String load_s = "";
    char load_c[3] = {"  "};

    //getting the info
    lcd_clear();
    lcd_print(L1Ta, F("       Regrade      "));

    lcd_print(L2Ta, F("Month(MM): "));
    numberPad(L2Ta + 11, 2, month_c);
    if (ExitFlag) {
        ExitFlag = false;
        return;
    }

    lcd_print(L3Ta, F("Day(DD): "));
    numberPad(L3Ta + 11, 2, day_c);

    if (ExitFlag) {
        ExitFlag = false;
        return;
    }

    lcd_print(L4Ta, F("Load #: "));
    numberPad(L4Ta + 11, 3, load_c);

    if (ExitFlag) {
        ExitFlag = false;
        return;
    }
    load_s += load_c;
    int LoadNumber_temp = LoadNumber; //saving the previous load number
    LoadNumber = load_s.toInt();


    pathToRegradeFile = "";
    pathToRegradeFile += "loads/";
    pathToRegradeFile += month_c;
    pathToRegradeFile += "/";
    pathToRegradeFile += day_c;
    pathToRegradeFile += "/";
    pathToRegradeFile += month_c;
    pathToRegradeFile += day_c;
    pathToRegradeFile += "L";
    pathToRegradeFile += atoi(load_c);
    pathToRegradeFile += ".txt";

    Serial.print(F("The path is: "));
    Serial.println(pathToRegradeFile);
    loadIsRegrade = true;

    masterReceiver();

    LoadNumber = LoadNumber_temp;

}


#endif	/* REGRADEFUNCTIONS_H */

