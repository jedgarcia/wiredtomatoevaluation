/* 
 * File:   sampleFormats.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on September 6, 2013, 8:00 AM
 */

#ifndef SAMPLEFORMATS_H
#define	SAMPLEFORMATS_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"

void gradestarSampleMenu() {
    int sample_menu_size = 7;
    const __FlashStringHelper * sample_menu[sample_menu_size];

    sample_menu[0] = F("01. Empty");
    sample_menu[1] = F("02. Time fields");
    sample_menu[2] = F("03. pH");
    sample_menu[3] = F("04. Comm");
    sample_menu[4] = F("05. Refrac");
    sample_menu[5] = F("06. Weights");
    sample_menu[6] = F("07. Everything");
    do {

        mainMenu(sample_menu, 7);
        switch (key) {
            case 1:SampleFormat(false, false, false, false);
                break;
            case 2:SampleFormat(false, false, false, false);
                break;
            case 3:SampleFormat(true, false, false, false);
                break;
            case 4:SampleFormat(false, true, false, false);
                break;
            case 5:SampleFormat(false, false, true, false);
                break;
            case 6:SampleFormat(false, false, false, true);
                break;
            case 7:SampleFormat(true, true, true, true);
                break;
        }
    } while ( key != CLEAR);

}

void SampleFormat(bool pH, bool comm, bool refrac, bool weights) {

    SoftwareSerial& gradestar = masterAndGradestarSerial;
    gradestar.begin(9600);

    //Starting the real time clock
    RTC_DS1307 RTC; //this is the type of RTC we got, so you must declare it
    Wire.begin(); //this must be done in order to use SCL & SDA
    RTC.begin(); //You must begin the RTC
    now = RTC.now();

    String clockBuffer = "";

    gradestar.write(2);
    gradestar.println(F("<xml>"));

    gradestar.print(F("<loadnumber>"));
    gradestar.print(LoadNumber); //minus one because this is called after the load number is incremented
    gradestar.println(F("</loadnumber>"));

    gradestar.print(F("<regrade>"));
    gradestar.print(F("n"));
    gradestar.println(F("</regrade>"));

    //DATE---------------------------------------------
    gradestar.print(F("<date>"));

    //month
    if (now.month() < 10) {//If the month needs an extra 0
        clockBuffer = "0";
        clockBuffer += now.month();
        gradestar.print(clockBuffer);
    } else {
        gradestar.print(now.month()); //does not need a leading 0
    }

    gradestar.print('/');

    //day
    if (now.day() < 10) {
        clockBuffer = "0";
        clockBuffer += now.day();
        gradestar.print(clockBuffer);
    } else {
        gradestar.print(now.day(), DEC);
    }

    //year
    gradestar.print('/');
    gradestar.print(now.year(), DEC);

    gradestar.println(F("</date>"));
    //END-OF-DATE-----------------------------------------


    //TIME------------------------------------------------
    gradestar.print(F("<time>"));
    if (now.hour() < 10) {//If the month needs an extra 0
        clockBuffer = "0";
        clockBuffer += now.hour();
        gradestar.print(clockBuffer);
    } else {
        gradestar.print(now.hour()); //does not need a leading 0
    }

    gradestar.print(F(":"));

    if (now.minute() < 10) {//If the month needs an extra 0
        clockBuffer = "0";
        clockBuffer += now.minute();
        gradestar.print(clockBuffer);
    } else {
        gradestar.print(now.minute()); //does not need a leading 0
    }

    gradestar.print(F(":"));

    if (now.second() < 10) {//If the month needs an extra 0
        clockBuffer = "0";
        clockBuffer += now.second();
        gradestar.print(clockBuffer);
    } else {
        gradestar.print(now.second()); //does not need a leading 0
    }
    gradestar.println(F("</time>"));
    //END-OF-TIME -----hehehe--------------------------------------

    if (weights) {
        gradestar.print(F("<hopperwt>"));
        gradestar.print(F("50.25"));
        gradestar.println(F("</hopperwt>"));


        gradestar.print(F("<wormwt>"));
        gradestar.print(F("1.31"));
        gradestar.println(F("</wormwt>"));


        gradestar.print(F("<moldwt>"));
        gradestar.print(F("0.45"));
        gradestar.println(F("</moldwt>"));


        gradestar.print(F("<greenwt>"));
        gradestar.print(F("0.25"));
        gradestar.println(F("</greenwt>"));

        //MOT--------------------------------------------------------
        gradestar.print(F("<motwt dirt=\""));
        gradestar.print(F("y\" extr=\"y\">"));
        //<motwt dirt="y" extr="n">
        gradestar.print(F("2.32"));
        gradestar.println(F("</motwt>"));
        //END-OF-MOT---------------------------------------------

        //Limited USE---------------------------------------------
        gradestar.print(F("<luwt>"));
        gradestar.print(F("4.24"));
        gradestar.println(F("</luwt>"));
        //END-OF-LIMITED-USE----------------------------------

    } else {
        gradestar.print(F("<hopperwt>"));
        gradestar.print(F("-1"));
        gradestar.println(F("</hopperwt>"));


        gradestar.print(F("<wormwt>"));
        gradestar.print(F("-1"));
        gradestar.println(F("</wormwt>"));


        gradestar.print(F("<moldwt>"));
        gradestar.print(F("-1"));
        gradestar.println(F("</moldwt>"));


        gradestar.print(F("<greenwt>"));
        gradestar.print(F("-1"));
        gradestar.println(F("</greenwt>"));

        //MOT--------------------------------------------------------
        gradestar.print(F("<motwt dirt=\""));
        gradestar.print(F("n\" extr=\"n\">"));
        //<motwt dirt="y" extr="n">
        gradestar.print(F("-1"));
        gradestar.println(F("</motwt>"));
        //END-OF-MOT---------------------------------------------


        //Limited USE---------------------------------------------
        gradestar.print(F("<luwt>"));
        gradestar.print(F("-1"));
        gradestar.println(F("</luwt>"));
        //END-OF-LIMITED-USE----------------------------------
    }

    if (comm) {
        gradestar.print(F("<comm>"));
        gradestar.print(F("29"));
        gradestar.println(F("</comm>"));
    } else {
        gradestar.print(F("<comm>"));
        gradestar.print(F("-1"));
        gradestar.println(F("</comm>"));
    }

    if (refrac) {
        gradestar.print(F("<solids>"));
        gradestar.print(F("4.9"));
        gradestar.println(F("</solids>"));
    } else {
        gradestar.print(F("<solids>"));
        gradestar.print(F("-1"));
        gradestar.println(F("</solids>"));
    }

    if (pH) {
        gradestar.print(F("<ph>"));
        gradestar.print(F("3.14"));
        gradestar.println(F("</ph>"));
    } else {
        gradestar.print(F("<ph>"));
        gradestar.print(F("-1"));
        gradestar.println(F("</ph>"));
    }

    if (weights) {
        gradestar.print(F("<otherdefect1>"));
        gradestar.print(F("20.20"));
        gradestar.println(F("</otherdefect1>"));


        gradestar.print(F("<otherdefect2>"));
        gradestar.print(F("10.10"));
        gradestar.println(F("</otherdefect2>"));


        gradestar.print(F("<otherdefect3>"));
        gradestar.println(F("</otherdefect3>"));


        gradestar.print(F("<otherdefect4>"));
        gradestar.println(F("</otherdefect4>"));


        gradestar.print(F("<otherdefect5>"));
        gradestar.println(F("</otherdefect5>"));


        gradestar.print(F("<otherdefect6>"));
        gradestar.println(F("</otherdefect6>"));


        gradestar.print(F("<otherdefect7>"));
        gradestar.println(F("</otherdefect7>"));


        gradestar.print(F("<otherdefect8>"));
        gradestar.println(F("</otherdefect8>"));


        gradestar.print(F("<otherdefect9>"));
        gradestar.println(F("</otherdefect9>"));

    } else {
        gradestar.print(F("<otherdefect1>"));
        gradestar.println(F("</otherdefect1>"));


        gradestar.print(F("<otherdefect2>"));
        gradestar.println(F("</otherdefect2>"));


        gradestar.print(F("<otherdefect3>"));
        gradestar.println(F("</otherdefect3>"));


        gradestar.print(F("<otherdefect4>"));
        gradestar.println(F("</otherdefect4>"));


        gradestar.print(F("<otherdefect5>"));
        gradestar.println(F("</otherdefect5>"));


        gradestar.print(F("<otherdefect6>"));
        gradestar.println(F("</otherdefect6>"));


        gradestar.print(F("<otherdefect7>"));
        gradestar.println(F("</otherdefect7>"));


        gradestar.print(F("<otherdefect8>"));
        gradestar.println(F("</otherdefect8>"));


        gradestar.print(F("<otherdefect9>"));
        gradestar.println(F("</otherdefect9>"));
    }
    gradestar.println(F("</xml>"));
    gradestar.write(3);
    incrementLoadNumber();
}


#endif	/* SAMPLEFORMATS_H */

