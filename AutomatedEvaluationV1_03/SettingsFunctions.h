/* 
 * File:   SettingsFunctions.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on August 19, 2013, 2:56 PM
 */

#ifndef SETTINGSFUNCTIONS_H
#define	SETTINGSFUNCTIONS_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"

void setDevicesMasterIsExpecting() {
    do {
        const __FlashStringHelper * expectedDevicesMenu[10];
        int i = 1;

        expectedDevicesMenu[0] = F("Data Expected: ");

        if (expectedDevices[0]) {//color is expected
            expectedDevicesMenu[i++] = F("-Color");
        }
        if (expectedDevices[1]) {//pH is expected
            expectedDevicesMenu[i++] = F("-pH");
        }
        if (expectedDevices[2]) {//refrac is expected
            expectedDevicesMenu[i++] = F("-Solids Refraction");
        }
        if (expectedDevices[3]) {//weights are expected
            expectedDevicesMenu[i++] = F("-Weights");
        }
        int k = i;
        if (i < 2) {//no devices are selected
            expectedDevicesMenu[i++] = F("-None selected");
        }

        expectedDevicesMenu[i++] = F("--------------------");
        expectedDevicesMenu[i++] = F("To add a type:");
        expectedDevicesMenu[i++] = F("           -Press 1");
        expectedDevicesMenu[i++] = F("To remove a type:");
        expectedDevicesMenu[i++] = F("           -Press 2");
        expectedDevicesMenu[i++] = F("To exit press CLEAR");

        do {//shows the menu while the user is deciding what to choose
            Menu(expectedDevicesMenu, i);
        } while (key != '1' && key != '2' && key != CLEAR);

        if (key == '1') {// the user wants to add a type
            lcd_clear();
            lcd_print(L1Ta, F("1. Color"));
            lcd_print(L2Ta, F("2. pH"));
            lcd_print(L3Ta, F("3. Solids Refrac"));
            lcd_print(L4Ta, F("4. Weights"));

            do {//shows the menu while the user is deciding what to choose
                getKey();
                if (k == 5)
                    key = 99;
            } while ((key < '1' || key > '4') && key != 99 && key != CLEAR);

            switch (key) {
                case '1'://color
                    colorBuffer = "";
                    expectedDevices[0] = true;
                    break;
                case '2'://ph
                    phBuffer = "";
                    expectedDevices[1] = true;
                    break;
                case '3'://refrac
                    refracBuffer = "";
                    expectedDevices[2] = true;
                    break;
                case '4'://weight
                    for (int i = 0; i < 10; i++) {
                        weightBuffers[i] = "";
                        expectedDevices[3 + i] = true;
                    }
                    break;
                case 99://all devices are already enabled - can't add any more
                    lcd_clear();
                    lcd_print(L1Ta, F("All devices are"));
                    lcd_print(L2Ta, F("already enabled!"));
                    delay(1000);
                    break;
                case CLEAR:
                    lcd_clear();
                    lcd_print(L1Ta, F("Saving changes..."));
                    saveCurrentConfiguration(MAIN_MENU_MODE);
                    sendFinishedQueuedLoads(false);
                    lcd_clear();

                    return;
                    break;
            }


        } else if (key == '2') {//the user wants to remove a type
            String typesEnabled[4];
            lcd_clear();
            lcd_print(L1Ta, F("1. Color"));
            lcd_print(L2Ta, F("2. pH"));
            lcd_print(L3Ta, F("3. Solids Refrac"));
            lcd_print(L4Ta, F("4. Weights"));


            do {//shows the menu while the user is deciding what to choose
                getKey();
                if (k == 1)
                    key = 99;
            } while ((key < '1' || key > '4') && key != 99 && key != CLEAR);

            switch (key) {
                case '1':
                    colorBuffer = "-1";
                    expectedDevices[0] = false;
                    break;
                case '2':
                    phBuffer = "-1";
                    expectedDevices[1] = false;
                    break;
                case '3':
                    refracBuffer = "-1";
                    expectedDevices[2] = false;
                    break;
                case '4':
                    for (int i = 0; i < 10; i++) {
                        expectedDevices[i + 3] = false;
                        weightBuffers[i] = "-1";
                    }
                    break;
                case 99://all devices are already enabled - can't add any more
                    lcd_clear();
                    lcd_print(L1Ta, F("All devices are"));
                    lcd_print(L2Ta, F("already disabled!"));
                    delay(1000);
                    lcd_clear();
                    break;
                case CLEAR:
                    lcd_clear();
                    lcd_print(L1Ta, F("Saving changes..."));
                    saveCurrentConfiguration(MAIN_MENU_MODE);
                    sendFinishedQueuedLoads(false);
                    lcd_clear();

                    return;
                    break;
            }
        }
    } while (key != CLEAR);

    //saving the changes 
    lcd_clear();
    lcd_print(L1Ta, F("Saving changes..."));
    saveCurrentConfiguration(MAIN_MENU_MODE);
    sendFinishedQueuedLoads(false);
    lcd_clear();
}

void setStationCode() {
    char code_chararray[3]; //expecting station codes to be between 0-99
    lcd_clear();
    lcd_print(L1Ta, F("Station code: "));
    lcd_print(L1Ta + 14, stationCode);
    lcd_print(L3Ta, F("new code: "));
    numberPad(L3Ta + 10, 2, code_chararray);

    stationCode = atoi(code_chararray);
    lcd_clear();
    lcd_print(L1Ta, F("Station code set!"));
    lcd_print(L2Ta, F("new code: "));
    lcd_print(L2Ta + 10, stationCode);
    delay(2000);
}


#endif	/* SETTINGSFUNCTIONS_H */

