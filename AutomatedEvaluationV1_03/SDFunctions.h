/* 
 * File:   SDFunctions.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on July 29, 2013, 1:49 PM
 */

#ifndef SDFUNCTIONS_H
#define	SDFUNCTIONS_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "SimpleVector.h"
#include "StringForSD.h"

bool loadIsDone(char* filePath, bool isRegrade) {//still need to build the regrade option

    bool needToAddDate = false;
    bool needToAddTime = false;

    File file = SD.open(filePath, FILE_READ);
    file.seek(0);

    if (!file) {

        Serial.println("We didnt open the file");
    }
    bool isDone = true;
    String buffer;


    for (int i = 0; i < 2; i++)//first two are already filled out by default
        readNextLoadValue(file);



    if (expectedDevices[3]) {//weight is expected
        Serial.println("Weights are expected");

        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (weightFieldIsOn[DATE]) {//check if date is there
            if (buffer.toInt() == -1)
                return false;
        }

        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (weightFieldIsOn[TIME]) {//check if time is there
            if (buffer.toInt() == -1)
                return false;
        }

        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (weightFieldIsOn[HOPPERWT]) {//check if hopper weight is there
            if (buffer.toInt() == -1)
                return false;
        }

        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (weightFieldIsOn[WORM]) {//check if worm weight is there
            if (buffer.toInt() == -1)
                return false;
        }

        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (weightFieldIsOn[MOLD]) {//check if mold weight is there
            if (buffer.toInt() == -1)
                return false;
        }

        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (weightFieldIsOn[GREEN]) {//check if green is there
            if (buffer.toInt() == -1)
                return false;
        }

        if (weightFieldIsOn[MOT]) {//check if MOT is there

            //check if DIRT is there
            buffer = readNextLoadValue(file);
            buffer.trim();
            Serial.println(buffer);
            if (buffer.toInt() == -1)
                return false;

            //check if EXTR is there
            buffer = readNextLoadValue(file);
            buffer.trim();
            Serial.println(buffer);
            if (buffer.toInt() == -1)
                return false;

            //check if MOT weight is there
            buffer = readNextLoadValue(file);
            buffer.trim();
            Serial.println(buffer);
            if (buffer.toInt() == -1)
                return false;
        }

        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (weightFieldIsOn[LU]) {//check if LU is there
            if (buffer.toInt() == -1)
                return false;
        }
    }//if weight
    else {//check if we have date and time, if we dont put them in yourself

        //date
        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (buffer.toInt() == -1) {
            needToAddDate = true;
        }

        //time
        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (buffer.toInt() == -1) {
            needToAddTime = true;
        }

        for (int i = 0; i < 8; i++)//skips past the rest of the weights
            readNextLoadValue(file);

    }


    Serial.println("This is the color coming up: ");
    buffer = readNextLoadValue(file);
    buffer.trim();
    Serial.println(buffer.toInt());
    if (expectedDevices[0]) {//if expecting comm
        if (buffer.toInt() < 0) {
            return false;
        }
    }


    Serial.println("This is the refrac coming up: ");
    buffer = readNextLoadValue(file);
    buffer.trim();
    Serial.println(buffer);
    if (expectedDevices[2]) {//if expecting refrac
        if (buffer.toInt() < 0) {
            Serial.println("This is not coo");
            return false;
        }
    }

    Serial.println("This is the pH coming up: ");
    buffer = readNextLoadValue(file);
    buffer.trim();
    Serial.println(buffer);
    if (expectedDevices[1]) {//if expecting ph
        if (buffer.toInt() < 0) {
            return false;
        }
    }


    if (expectedDevices[3]) {//checking for spare 1 and spare 2

        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (weightFieldIsOn[SPARE_1]) {//check if spare1 is there
            if (buffer.length() < 3) {
                return false;
            }
        }
        buffer = readNextLoadValue(file);
        buffer.trim();
        Serial.println(buffer);
        if (weightFieldIsOn[SPARE_2]) {//check if spare 2 is there
            if (buffer.length() < 3) {
                return false;
            }
        }
    }
    file.close();

    //Starting the real time clock
    makeSureDateIsInTheFile(filePath);

    return true;
}

String getTimeFromDateTime() {
    DateTime now = RTC.now();
    delay(500);
    String time = "";
    if (now.hour() < 10) {//If the month needs an extra 0
        time = "0";
        time += now.hour();
    } else
        time += now.hour();

    time += ":";

    if (now.minute() < 10) {//If the month needs an extra 0
        time = "0";
        time += now.minute();
    } else
        time += now.minute();


    time += ":";

    if (now.second() < 10) {//If the month needs an extra 0
        time = "0";
        time += now.second();
    } else
        time += now.second();

    return time;
}

String getDateFromDateTime() {
    DateTime now = RTC.now();
    delay(500);
    String date = "";
    //Date
    if (now.month() < 10) {//If the month needs an extra 0
        date = "0";
        date += now.month();
    } else
        date += now.month();

    date += "/";

    if (now.day() < 10) {
        date = "0";
        date += now.day();
    } else
        date += now.day();

    date += "/";

    date += now.year();

    return date;
}

void createDefaultLoad(File& file) {
    int oldPosition = file.position();

    file.seek(0);

    file.print(stationCode);
    file.print(F(","));

    file.print(LoadNumber);
    file.print(F(","));

    file.print(F("-1,-1,-1,-1,-1,-1,n,n,-1,-1,-1,-1,-1,,,,,,,,,,n,"));

    file.seek(oldPosition);
}

void recordLoadDataToSD() {
    //These must be confired like this in order for the SD library to work
    pinMode(53, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(10, OUTPUT);

    String clockBuffer = "";

    //Starting the real time clock
    RTC_DS1307 RTC; //this is the type of RTC we got, so you must declare it
    Wire.begin(); //this must be done in order to use SCL & SDA
    RTC.begin(); //You must begin the RTC

    if (!RTC.isrunning()) { //checks to see that the clock is running well
        lcd_clear();
        lcd_print(L1Ta, F("Clock not working"));
        lcd_print(L2Ta, F("load not recorded"));
        digitalWrite(indicatorGreen, LOW);
        digitalWrite(indicatorRed, HIGH);
        delay(5000);
        return;
    } else {//real time clock is working!    
    }

    //makes a file called "MMDDL###.txt" - x is the load number  
    char pathCharArray[25] = {'\0'};
    String pathBuffer = "loads/";

    //month
    if (now.month() < 10) {//If the month needs an extra 0
        pathBuffer += "0";
        pathBuffer += now.month();
    } else
        pathBuffer += now.month();

    pathBuffer += "/";

    //day
    if (now.day() < 10) {
        pathBuffer += "0";
        pathBuffer += now.day();
    } else
        pathBuffer += now.day();

    pathBuffer += "/";

    pathBuffer.toCharArray(pathCharArray, 25);
    SD.mkdir(pathCharArray); //creates whatever folders dont exist in "loads/mm/day/"

    //done with the folder part of the path - starting the text file name

    //month
    if (now.month() < 10) {//If the month needs an extra 0
        pathBuffer += "0";
        pathBuffer += now.month();
    } else
        pathBuffer += now.month();
    //day
    if (now.day() < 10) {
        pathBuffer += "0";
        pathBuffer += now.day();
    } else
        pathBuffer += now.day();

    pathBuffer += "L";
    pathBuffer += LoadNumber;
    pathBuffer += ".txt";

    pathBuffer.toCharArray(pathCharArray, 25);



    File file;
    if (!loadIsRegrade) {
        if (SD.exists(pathCharArray)) {//if the SD file already exists
            //We need to add the information we just got to it



        }
    } else {//this is a regrade use a previous loads path
        Serial.println(F("This is a regrade!"));
        pathToRegradeFile.toCharArray(pathCharArray, 25);
        //File file = SD.open(pathCharArray, FILE_WRITE);
    }

    if (!SD.exists(pathCharArray)) {//if it is a new load, the file wasn't created, if it is a regrade then there wasn't a load under that already
        lcd_clear();
        lcd_print(L1Ta, F("SD card error"));
        lcd_print(L2Ta, F("load not recorded"));
        digitalWrite(indicatorRed, HIGH);
        digitalWrite(indicatorGreen, LOW);
        delay(5000);
    }

    //Do not do now = rtc.now(), this has been done in the calling function
    //so that we have consistent times
    file = SD.open(pathCharArray, FILE_WRITE);
    if (loadIsRegrade)
        file.println(); //put a new line between old load and the new one

    //Station
    file.print(stationCode);
    file.print(F(","));

    //Load Number
    file.print(LoadNumber);
    file.print(F(","));

    //Date
    if (now.month() < 10) {//If the month needs an extra 0
        clockBuffer = "0";
        clockBuffer += now.month();
        file.print(clockBuffer);
    } else {
        file.print(now.month()); //does not need a leading 0
    }
    file.print(F("/"));
    if (now.day() < 10) {
        clockBuffer = "0";
        clockBuffer += now.day();
        file.print(clockBuffer);
    } else {
        file.print(now.day(), DEC);
    }

    file.print(F("/"));
    file.print(now.year(), DEC);
    file.print(F(","));

    //TIME
    if (now.hour() < 10) {//If the month needs an extra 0
        clockBuffer = "0";
        clockBuffer += now.hour();
        file.print(clockBuffer);
    } else {
        file.print(now.hour()); //does not need a leading 0
    }

    file.print(F(":"));

    if (now.minute() < 10) {//If the month needs an extra 0
        clockBuffer = "0";
        clockBuffer += now.minute();
        file.print(clockBuffer);
    } else {
        file.print(now.minute()); //does not need a leading 0
    }

    file.print(F(":"));

    if (now.second() < 10) {//If the month needs an extra 0
        clockBuffer = "0";
        clockBuffer += now.second();
        file.print(clockBuffer);
    } else {
        file.print(now.second()); //does not need a leading 0
    }
    file.print(F(","));


    //weight
    if (expectedDevices[3]) {//expecting weights
        //Weight Buffer - Hopper WT
        file.print(RemoveCategoryFromWeight(weightBuffers[0]));
        file.print(F(","));

        //Weight Buffer - WORM
        file.print(RemoveCategoryFromWeight(weightBuffers[1]));
        file.print(F(","));

        //Weight Buffer - MOLD
        file.print(RemoveCategoryFromWeight(weightBuffers[2]));
        file.print(F(","));

        //Weight Buffer - GREEN
        file.print(RemoveCategoryFromWeight(weightBuffers[3]));
        file.print(F(","));

        //Weight Buffer - MOT
        if (MotISBoth()) {
            file.print(F("Y,Y,"));
            file.print(RemoveCategoryFromWeight(weightBuffers[4]));
            file.print(F(","));
        } else if (MotIsDirt()) {
            file.print(F("Y,N,"));
            file.print(RemoveCategoryFromWeight(weightBuffers[4]));
            file.print(F(","));
        } else if (MotISExtra()) {
            file.print(F("N,Y,"));
            file.print(RemoveCategoryFromWeight(weightBuffers[4]));
            file.print(F(","));
        } else {//it is undefined mot
            file.print(F("N,N,"));
            file.print(RemoveCategoryFromWeight(weightBuffers[4]));
            file.print(F(","));
        }

        //Weight Buffer - LU
        file.print(RemoveCategoryFromWeight(weightBuffers[5]));
        file.print(F(","));

    } else {//not expecting the weights
        file.print(F(" , , , , , , , ,"));
    }

    //Color!
    if (expectedDevices[0]) {//expected Color
        file.print(colorBuffer);
        file.print(F(","));
    } else {//did not expect Color
        file.print(F(" ,"));
    }


    //refrac!
    if (expectedDevices[2]) {//expected Refrac
        if (refracBuffer.length() > 4) {
            file.print(refracBuffer.substring(0, refracBuffer.length() - 1));
        } else {
            file.print(refracBuffer);
        }
        file.print(F(","));
    } else { //did not expect refrac
        file.print(F(" ,"));
    }

    //ph
    if (expectedDevices[1]) {//expected ph
        if (phBuffer.length() > 4) {
            file.print(phBuffer.substring(0, phBuffer.length() - 1));
        } else {
            file.print(phBuffer);
        }
        file.print(F(","));
    } else {//did not expect ph
        file.print(F(" ,"));
    }


    if (expectedDevices[3]) {
        //Weight Buffer - Spare 1
        file.print(RemoveCategoryFromWeight(weightBuffers[6]));
        file.print(F(","));


        //Weight Buffer - Spare 2
        file.print(RemoveCategoryFromWeight(weightBuffers[7]));
        file.print(F(","));


        //Weight Buffer - Spare 3
        file.print(F(" "));
        file.print(F(","));


        //Weight Buffer - Spare 4
        file.print(F(" "));
        file.print(F(","));


        //Weight Buffer - Spare 5
        file.print(F(" "));
        file.print(F(","));


        //Weight Buffer - Spare 6
        file.print(F(" "));
        file.print(F(","));


        //Weight Buffer - Spare 7
        file.print(F(" "));
        file.print(F(","));


        //Weight Buffer - Spare 8
        file.print(F(" "));
        file.print(F(","));


        //Weight Buffer - Spare 9
        file.print(F(" "));
        file.print(F(","));
    } else {
        file.print(F(" , , , , , , , , ,"));
    }
    if (loadIsRegrade) {//This is a regrade 
        Serial.println(F("Saved to sd as a regrade"));
        file.print("Y,");
        loadIsRegrade = false;
        main_menu_flag = true;
    } else {

        file.print("N,");
    }

    file.close();
    Serial.println(F("FINISHED WRITING TO THE SD CARD"));

}

void readFromLoadDataFromSD(char charArrayPath[]) {

    lcd_clear();

    if (SD.exists(charArrayPath)) {
        Serial.println(F("The load file does exist"));
        //all clear we can open the file
    } else {
        lcd_clear();
        lcd_print(L1Ta, F("NOT VALID LOAD"));
        delay(4000);
        return;
    }
    int SD_data_counter = 0;
    String SD_Data[46];

    File file = SD.open(charArrayPath, FILE_READ);

    //1 - Station
    Serial.println(F("Right before writing the the array"));
    SD_Data[SD_data_counter] = "Station code: ";
    Serial.println(F("Right before going to read the thing"));
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    Serial.println(F("Got the station code!"));

    //2 - Load Number
    SD_Data[SD_data_counter] = "Load#: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);

    Serial.println(SD_Data[SD_data_counter - 1]);
    Serial.println(F("Got the load"));

    //3
    SD_Data[SD_data_counter] = "Date: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);
    Serial.println(F("Got the date"));


    //4 - Time
    SD_Data[SD_data_counter] = "Time: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    Serial.println(F("Got the time"));

    //5 - Hopper Weight
    SD_Data[SD_data_counter] = "Hopper Wt: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);
    Serial.println(F("Got the hopper weight"));

    //6 - Worm
    SD_Data[SD_data_counter] = "Worm: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);


    //7 - Mold
    SD_Data[SD_data_counter] = "Mold: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //8 - Green
    SD_Data[SD_data_counter] = "Green: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);


    //9 - MOT
    SD_Data[SD_data_counter] = "MOT D:";
    SD_Data[SD_data_counter] += readNextLoadValue(file);

    SD_Data[SD_data_counter] += " E:";
    SD_Data[SD_data_counter] += readNextLoadValue(file);

    SD_Data[SD_data_counter] += " :";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);

    Serial.println(SD_Data[SD_data_counter - 1]);
    Serial.println(F("Got the mot"));

    //10 - LU
    SD_Data[SD_data_counter] = "LU: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //11 - Color
    SD_Data[SD_data_counter] = "Color: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //12 - refrac
    SD_Data[SD_data_counter] = "Refrac: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //13 - pH
    SD_Data[SD_data_counter] = "pH: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //14 - Spare 1
    SD_Data[SD_data_counter] = "Spare1: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //15
    SD_Data[SD_data_counter] = "Spare2: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //16
    SD_Data[SD_data_counter] = "Spare3: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //17
    SD_Data[SD_data_counter] = "Spare4: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //18
    SD_Data[SD_data_counter] = "Spare5: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //19
    SD_Data[SD_data_counter] = "Spare6: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //20
    SD_Data[SD_data_counter] = "Spare7: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //21
    SD_Data[SD_data_counter] = "Spare8: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //22
    SD_Data[SD_data_counter] = "Spare9: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    //23
    SD_Data[SD_data_counter] = "Regrade: ";
    SD_Data[SD_data_counter++] += readNextLoadValue(file);
    Serial.println(SD_Data[SD_data_counter - 1]);

    if (file.read() == '\n' || file.read() == '\n') {
        Serial.println(F("This was a regrade"));
        //1
        Serial.println(F("Right before writing the the array"));
        SD_Data[SD_data_counter] = "Station code: ";
        Serial.println(F("Right before going to read the thing"));
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        Serial.println(F("Got the station code!"));

        //2
        SD_Data[SD_data_counter] = "Load#: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);

        Serial.println(SD_Data[SD_data_counter - 1]);
        Serial.println(F("Got the load"));

        //3
        SD_Data[SD_data_counter] = "Date: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);
        Serial.println(F("Got the date"));


        //4
        SD_Data[SD_data_counter] = "Time: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        Serial.println(F("Got the time"));

        //5
        SD_Data[SD_data_counter] = "Hopper Wt: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);
        Serial.println(F("Got the hopper weight"));

        //6
        SD_Data[SD_data_counter] = "Worm: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);


        //7
        SD_Data[SD_data_counter] = "Mold: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //8
        SD_Data[SD_data_counter] = "Green: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);


        //9
        SD_Data[SD_data_counter] = "MOT D:";
        SD_Data[SD_data_counter] += readNextLoadValue(file);

        SD_Data[SD_data_counter] += " E:";
        SD_Data[SD_data_counter] += readNextLoadValue(file);

        SD_Data[SD_data_counter] += " :";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);

        Serial.println(SD_Data[SD_data_counter - 1]);
        Serial.println(F("Got the mot"));

        //10
        SD_Data[SD_data_counter] = "LU: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //11
        SD_Data[SD_data_counter] = "Color: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //12
        SD_Data[SD_data_counter] = "Refrac: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //13
        SD_Data[SD_data_counter] = "pH: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //14
        SD_Data[SD_data_counter] = "Spare1: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //15
        SD_Data[SD_data_counter] = "Spare2: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //16
        SD_Data[SD_data_counter] = "Spare3: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //17
        SD_Data[SD_data_counter] = "Spare4: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //18
        SD_Data[SD_data_counter] = "Spare5: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //19
        SD_Data[SD_data_counter] = "Spare6: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //20
        SD_Data[SD_data_counter] = "Spare7: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //21
        SD_Data[SD_data_counter] = "Spare8: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //22
        SD_Data[SD_data_counter] = "Spare9: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //23
        SD_Data[SD_data_counter] = "Regrade: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        key = 0;
        while (key != CLEAR && key != ENTER && key != BUTTONNUMBER) {
            MenuButton(SD_Data, 46);
        }
        file.close();
    } else {//not a regrade
        key = 0;

        while (key != CLEAR && key != ENTER && key != BUTTONNUMBER) {
            MenuButton(SD_Data, 23);
        }
        file.close();
    }

}

bool isEmpty(File& file) {
    int oldPos = file.position();
    String buffer = readNextLoadValue(file);
    buffer.trim();

    if (buffer.toInt() == -1) {//false it is empty
        return true;
    } else {
        file.seek(oldPos);
        return false;
    }
}

void readFromLoadDataFromSD() {
    //These must be confired like this in order for the SD library to work
    pinMode(53, OUTPUT);
    //    pinMode(4, OUTPUT);
    //    pinMode(10, OUTPUT);

    lcd_clear();
    lcd_print(L1Ta, F("  View Load Data  "));

    char month[3] = "  ";
    lcd_print(L2Ta, F("Month(MM): "));
    numberPad(L2Ta + 11, 2, month);

    char day[3] = "  ";
    lcd_print(L3Ta, F("Day (DD): "));
    numberPad(L3Ta + 11, 2, day);

    lcd_print(L4Ta, F("Load#: "));
    numberPad(L4Ta + 11, 3, loadNumberitoa);

    //I do atoi in order to get rid of 0's the user might have input
    //for example 004.txt is not a file but 4.txt is a file
    //so atoi removes those 0s
    int intLoadNumber = atoi(loadNumberitoa);

    //loads/MM/DD/MMDDL###.txt
    String pathBuffer = "";
    pathBuffer += "loads/";
    pathBuffer += month;
    pathBuffer += "/";
    pathBuffer += day;
    pathBuffer += "/";
    pathBuffer += month;
    pathBuffer += day;
    pathBuffer += "L";
    pathBuffer += intLoadNumber;
    pathBuffer += ".TXT";

    char charArrayPath[25];
    pathBuffer.toCharArray(charArrayPath, 25);

    if (SD.exists(charArrayPath)) {
        Serial.println(F("The load file does exist"));
        //all clear we can open the file
    } else {
        lcd_clear();
        lcd_print(L1Ta, F("NOT VALID LOAD"));
        delay(4000);
        return;
    }

    int SD_data_counter = 0;
    String SD_Data[46];

    File file = SD.open(charArrayPath, FILE_READ);

    //1
    Serial.println(F("Right before writing the the array"));
    SD_Data[SD_data_counter] = "Station code: ";
    Serial.println(F("Right before going to read the thing"));
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    Serial.println(F("Got the station code!"));

    //2
    SD_Data[SD_data_counter] = "Load#: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);
    Serial.println(F("Got the load"));

    //3
    SD_Data[SD_data_counter] = "Date: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);
    Serial.println(F("Got the date"));


    //4
    SD_Data[SD_data_counter] = "Time: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    Serial.println(F("Got the time"));

    //5
    SD_Data[SD_data_counter] = "Hopper Wt: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);
    Serial.println(F("Got the hopper weight"));

    //6
    SD_Data[SD_data_counter] = "Worm: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);


    //7
    SD_Data[SD_data_counter] = "Mold: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //8
    SD_Data[SD_data_counter] = "Green: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);


    //9
    SD_Data[SD_data_counter] = "MOT D:";
    if (!isEmpty(file))
        SD_Data[SD_data_counter] += readNextLoadValue(file);
    else
        ++SD_data_counter;

    SD_Data[SD_data_counter] += " E:";
    if (!isEmpty(file))
        SD_Data[SD_data_counter] += readNextLoadValue(file);
    else
        ++SD_data_counter;

    SD_Data[SD_data_counter] += " :";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;

    Serial.println(SD_Data[SD_data_counter - 1]);
    Serial.println(F("Got the mot"));

    //10
    SD_Data[SD_data_counter] = "LU: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //11
    SD_Data[SD_data_counter] = "Color: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //12
    SD_Data[SD_data_counter] = "Refrac: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //13
    SD_Data[SD_data_counter] = "pH: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //14
    SD_Data[SD_data_counter] = "Spare1: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //15
    SD_Data[SD_data_counter] = "Spare2: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //16
    SD_Data[SD_data_counter] = "Spare3: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //17
    SD_Data[SD_data_counter] = "Spare4: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //18
    SD_Data[SD_data_counter] = "Spare5: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //19
    SD_Data[SD_data_counter] = "Spare6: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //20
    SD_Data[SD_data_counter] = "Spare7: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //21
    SD_Data[SD_data_counter] = "Spare8: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //22
    SD_Data[SD_data_counter] = "Spare9: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    //23
    SD_Data[SD_data_counter] = "Regrade: ";
    if (!isEmpty(file))
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
    else
        ++SD_data_counter;
    Serial.println(SD_Data[SD_data_counter - 1]);

    if (file.read() == '\n' || file.read() == '\n') {
        Serial.println(F("This was a regrade"));
        //1
        Serial.println(F("Right before writing the the array"));
        SD_Data[SD_data_counter] = "Station code: ";
        Serial.println(F("Right before going to read the thing"));
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        Serial.println(F("Got the station code!"));

        //2
        SD_Data[SD_data_counter] = "Load#: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);

        Serial.println(SD_Data[SD_data_counter - 1]);
        Serial.println(F("Got the load"));

        //3
        SD_Data[SD_data_counter] = "Date: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);
        Serial.println(F("Got the date"));


        //4
        SD_Data[SD_data_counter] = "Time: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        Serial.println(F("Got the time"));

        //5
        SD_Data[SD_data_counter] = "Hopper Wt: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);
        Serial.println(F("Got the hopper weight"));

        //6
        SD_Data[SD_data_counter] = "Worm: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);


        //7
        SD_Data[SD_data_counter] = "Mold: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //8
        SD_Data[SD_data_counter] = "Green: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);


        //9
        SD_Data[SD_data_counter] = "MOT D:";
        SD_Data[SD_data_counter] += readNextLoadValue(file);

        SD_Data[SD_data_counter] += " E:";
        SD_Data[SD_data_counter] += readNextLoadValue(file);

        SD_Data[SD_data_counter] += " :";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);

        Serial.println(SD_Data[SD_data_counter - 1]);
        Serial.println(F("Got the mot"));

        //10
        SD_Data[SD_data_counter] = "LU: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //11
        SD_Data[SD_data_counter] = "Color: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //12
        SD_Data[SD_data_counter] = "Refrac: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //13
        SD_Data[SD_data_counter] = "pH: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //14
        SD_Data[SD_data_counter] = "Spare1: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //15
        SD_Data[SD_data_counter] = "Spare2: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //16
        SD_Data[SD_data_counter] = "Spare3: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //17
        SD_Data[SD_data_counter] = "Spare4: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //18
        SD_Data[SD_data_counter] = "Spare5: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //19
        SD_Data[SD_data_counter] = "Spare6: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //20
        SD_Data[SD_data_counter] = "Spare7: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //21
        SD_Data[SD_data_counter] = "Spare8: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //22
        SD_Data[SD_data_counter] = "Spare9: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);

        //23
        SD_Data[SD_data_counter] = "Regrade: ";
        SD_Data[SD_data_counter++] += readNextLoadValue(file);
        Serial.println(SD_Data[SD_data_counter - 1]);
        file.close();

        Menu(SD_Data, 46);

    } else {//not a regrade

        file.close();
        Menu(SD_Data, 23);
    }

}

String getLine(File file) {
    String lineBuffer = "";
    while ((incoming = file.read()) != '\n' && file.available()) {

        lineBuffer += (char) incoming;
    }
    return lineBuffer;
}

String readNextLoadValue(File& file) {
    String loadBuffer = "";

    while ((incoming = file.read()) != ',' && file.available()) {
        loadBuffer += (char) incoming;
    }

    return loadBuffer;
}

void readFromConfigFile(File file) {
    //These must be confired like this in order for the SD library to work
    pinMode(53, OUTPUT);

    String configValue = "";
    Serial.println(F("About to do it"));

    ExitFlag = false;

    //data expected color
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("data expected color: "));
            Serial.println(expectedDevices[0] = configValue.toInt());
        }
    }
    if (!expectedDevices[0]) {//were not expecting to get color readings
        colorBuffer = " ";
    }


    //data expected ph
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("data expected ph: "));
            Serial.println(expectedDevices[1] = configValue.toInt());
        }
    }
    if (!expectedDevices[1]) {//were not expecting to get color readings
        phBuffer = "";
    }


    //data expected refrac
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("data expected refrac: "));
            Serial.println(expectedDevices[2] = configValue.toInt());
        }
    }
    if (!expectedDevices[2]) {//were not expecting to get color readings
        refracBuffer = "";
    }

    //data expected weights
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("data expected weights:"));
            Serial.println(expectedDevices[3] = configValue.toInt());
        }
    }

    //Setting values based on input
    if (!expectedDevices[3]) {//were not expecting to get weight readings
        for (int i = 0; i < 10; i++) {
            weightBuffers[i] = "";
            expectedDevices[3 + i] = false;
        }
    } else {//we do expect to get weights
        for (int i = 0; i < 10; i++) {
            weightBuffers[i] = "";
            expectedDevices[3 + i] = true;
        }
    }

    //station code
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("Station code: "));
            Serial.println(stationCode = configValue.toInt());
        }
    }

    //ph timeout
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("ph timeout(ms): "));
            Serial.println(PH_STABLE_TIMEOUT = configValue.toInt());
        }
    }

    //ph epsilon
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("pH epsilon: "));
            Serial.println(PH_EPSILON = (configValue.toInt() / 1000.000), 3);
        }
    }

    //ph reading Rate
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("ph reading rate(ms): "));
            Serial.println(PH_READING_RATE = configValue.toInt());
        }
    }

    //Load Number
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("Load Number: "));
            Serial.println(LoadNumber = configValue.toInt());
        }
    }

    //Arduino Function
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("Arduino function: "));
            Serial.println(arduinoFunction = config_arduino_function = configValue.toInt());
        }
    }

    //---------------------------------------------------

    //Listen Flags

    //Color listen Flag
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("Color listen flag: "));
            Serial.println(listenColor = configValue.toInt());
        }
    }

    //pH Listen Flag
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("pH listen flag: "));
            Serial.println(listenPh = configValue.toInt());
        }
    }

    //refrac Listen Flag
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("Refrac listen flag: "));
            Serial.println(listenRefrac = configValue.toInt());
        }
    }

    //weight Listen Flag
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (!ExitFlag) {
            Serial.print(F("Weight listen flag: "));
            Serial.println(listenWeight = configValue.toInt());
        }
    }



    //----------------------------------------------------
    //Device Flags
    Serial.println(F("Trying to get AR20 flag"));

    //Listen AR20
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if ((listenAR20 = configValue.toInt())) {//if AR20 is enabled

            Serial.print(F("ar20 flag: "));
            Serial.println(configValue.toInt());

            if (SD.exists("pSerial.txt")) {
                Serial.println(F("Getting ph from file"));
                getSerialXFrom(phSerial, "pSerial.txt");
            } else {
                Serial.println(F("Making a ph serial because the file didnt exist!"));
                saveSerialToSD(phSerial, "pSerial.txt");
            }
        } else {
            Serial.println(F("AR20 : 0"));
        }
    }

    Serial.println(F("Trying to get the LED flag"));

    //listen LED
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (listenLED = configValue.toInt()) {

            Serial.print(F("LED flag: "));
            Serial.println(configValue.toInt());

            if (SD.exists("cSerial.txt")) {
                Serial.println(F("Getting color from file"));
                getSerialXFrom(colorSerial, "cSerial.txt");
            } else {
                Serial.println(F("Making color file because it didnt exist"));
                saveSerialToSD(colorSerial, "cSerial.txt");
            }
        } else {
            Serial.println(F("LED: 0"));
        }
    }

    Serial.println(F("Trying to get rfm190 flag"));

    //listen RFM190
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);
        if (listenRFM190 = configValue.toInt()) {

            Serial.print(F("RFM190 flag: "));
            Serial.println(configValue.toInt());

            if (SD.exists("rSerial.txt")) {
                Serial.println(F("Getting refrac from file"));
                getSerialXFrom(refracSerial, "rSerial.txt");
            } else {
                Serial.println(F("making refrac file because it didnt exist"));
                saveSerialToSD(refracSerial, "rSerial.txt");
            }
        } else {
            Serial.println(F("RFM190: 0"));
        }
    }

    Serial.println(F("Trying to get the vc505 flag"));

    //listen VC505
    if (!ExitFlag) {
        configValue = getNextConfigValue(file);

        if (listenVC505 = configValue.toInt()) {

            Serial.print(F("VC505 flag: "));
            Serial.println(configValue.toInt());

            if (SD.exists("wSerial.txt")) {
                Serial.println(F("Getting weight from file"));
                getSerialXFrom(weightSerial, "wSerial.txt");
            } else {

                Serial.println(F("Making own weight file because it didnt exist"));
                saveSerialToSD(weightSerial, "wSerial.txt");
            }
        } else {

            Serial.println(F("VC505: 0"));
        }
    }

    //getting the LoadQueue-------------------------
    for (int i = 0; i < 10; i++) {
        if (!ExitFlag) {
            configValue = getNextConfigValue(file);
            if (!ExitFlag) {
                loadQueue.push(configValue.toInt());
            }
        }
    }

    //getting Weight Names --------------------------
    for (int i = 0; i < 10; i++) {
        if (!ExitFlag) {
            configValue = getNextConfigValue(file);
            if (!ExitFlag) {
                configValue.trim();
                weightFields[i] = configValue;
            }
        }
    }

    //Getting which Weights are turned on
    for (int i = 0; i < 10; i++) {
        if (!ExitFlag) {
            configValue = getNextConfigValue(file);
            if (!ExitFlag) {
                configValue.trim();
                weightFieldIsOn[i] = (bool) configValue.toInt();
            }
        }
    }



    ExitFlag = false;
    file.close();
}

void getSerialXFrom(HardwareSerial& destinationSerial, char* filepath) {

    File file = SD.open(filepath, FILE_READ);
    byte savedSerial[sizeof (HardwareSerial)];
    for (int i = 0; i < sizeof (HardwareSerial); i++) {

        savedSerial[i] = file.read();
    }

    //We have now finished reading in our serial variable from the file
    //we must now copy it

    destinationSerial = (*(HardwareSerial*) savedSerial);

}

void saveSerialToSD(HardwareSerial& inputSerial, char* filepath) {

    blinkTimer.update();

    SD.remove(filepath);
    delay(10);

    File file = SD.open(filepath, FILE_WRITE);
    file.close();

    if (!SD.exists(filepath)) {
        Serial.print(filepath);
        Serial.println(F(" was not made!"));
    } else {

        Serial.print(filepath);
        Serial.println(F(" was made successfully!"));
    }


    file = SD.open(filepath, FILE_WRITE);

    file.write((byte*) & inputSerial, sizeof (HardwareSerial));
    file.close();


    Serial.print(filepath);
    Serial.println(F(" Saved"));
}

String getNextConfigValue(File file) {
    //configuration file is in the format:  description: value 
    int i = 0;
    String lineBuffer = "";
    Timer SDTimer;
    int SDTimeout = SDTimer.after(2000, enableExitFlag);

    while ((incoming = file.read()) != '\n' && file.available()) {
        SDTimer.update();
        if (ExitFlag) {
            Serial.println(F("Timed out looking for '\\n' and EOF"));
            lcd_clear();
            lcd_print(L1Ta, F("Corrupt config.txt"));
            lcd_print(L2Ta, F("file. Creating new"));
            lcd_print(L3Ta, F("configuration file."));
            SD.remove("config.txt");
            createConfigFile();
            SDTimer.stop(SDTimeout);
            delay(2000);
            lcd_clear();
            return lineBuffer;
        }

        lineBuffer += (char) incoming;
    }

    while (lineBuffer.charAt(i++) != ':') {
        SDTimer.update();
        if (ExitFlag) {
            Serial.println(F("timed out after looking for ':'"));
            lcd_clear();
            lcd_print(L1Ta, F("Corrupt config.txt"));
            lcd_print(L2Ta, F("file. Creating new"));
            lcd_print(L3Ta, F("configuration file."));
            SD.remove("config.txt");
            createConfigFile();
            SDTimer.stop(SDTimeout);
            delay(2000);
            lcd_clear();

            return lineBuffer;
        }
    }

    return lineBuffer.substring(i);
}

void createConfigFile() {
    //These must be confired like this in order for the SD library to work
    char fileName[] = "config.txt";
    SD.remove(fileName);
    File configFile = SD.open(fileName, FILE_WRITE);
    configFile.close();

    if (!SD.exists(fileName)) {
        Serial.println(F("config file was not made!"));
        lcd_clear();
        lcd_print(L1Ta, F("Faulty SD Card"));
        lcd_print(L2Ta, F("configuration won't"));
        lcd_print(L3Ta, F("be saved"));
        delay(5000);
        lcd_clear();

        return;
    }

    configFile = SD.open(fileName, FILE_WRITE);
    configFile.println(F("data expected color: 1"));
    configFile.println(F("data expected ph: 1"));
    configFile.println(F("data expected refrac: 1"));
    configFile.println(F("data expected weights: 1"));
    configFile.println(F("Station code: 0"));
    configFile.println(F("ph timeout(ms): 3000"));
    configFile.println(F("ph epsilon(.xxx): 005"));
    configFile.println(F("ph reading rate(ms): 2000"));
    configFile.println(F("load number: 1"));
    configFile.println(F("Function (0= main menu, 1= master, 2 = measuring): 0"));
    configFile.println(F("Color Listen Flag: 0"));
    configFile.println(F("pH Listen Flag: 0"));
    configFile.println(F("Refrac Listen Flag: 0"));
    configFile.println(F("Weight Listen Flag: 0"));
    configFile.println(F("AR20 Listen Flag: 0"));
    configFile.println(F("LED listen Flag: 0"));
    configFile.println(F("RFM190 listen Flag: 0"));
    configFile.println(F("VC-505 listen Flag: 0"));
    for (int i = 0; i < 10; i++) {
        configFile.println(F("Load Queue: 0"));
    }
    configFile.println(F("Date: DATE"));
    configFile.println(F("Time: TIME"));
    configFile.println(F("Hopper Wt: SAMPLE WT"));
    configFile.println(F("Worm: WORM"));
    configFile.println(F("MOLD: MOLD"));
    configFile.println(F("GREEN: GREEN"));
    configFile.println(F("MOT: MOT"));
    configFile.println(F("LU: LU"));
    configFile.println(F("OTHER DEFECT 1: PEELER"));
    configFile.println(F("OTHER DEFECT 2: ALL"));
    configFile.println(F("Date ON: 1"));
    configFile.println(F("Time ON: 1"));
    configFile.println(F("Hopper Wt ON: 1"));
    configFile.println(F("Worm ON: 1"));
    configFile.println(F("MOLD ON: 1"));
    configFile.println(F("GREEN ON: 1"));
    configFile.println(F("MOT ON: 1"));
    configFile.println(F("LU ON: 1"));
    configFile.println(F("OTHER DEFECT 1 ON: 1"));
    configFile.println(F("OTHER DEFECT 2 ON: 1"));
    configFile.close();

    saveSerialToSD(colorSerial, "cSerial.txt");
    saveSerialToSD(phSerial, "pSerial.txt");
    saveSerialToSD(refracSerial, "rSerial.txt");
    saveSerialToSD(weightSerial, "wSerial.txt");

    if (!SD.exists("readme.txt"))
        createMakeMe();


}

void createMakeMe() {

    SD.remove("readme.txt");
    File readmeFile = SD.open("readme.txt", FILE_WRITE);
    readmeFile.println(F("These files have the following naming format: MMDDL###.txt"));
    readmeFile.println(F("The MM stands for month (for example: 07 = july)"));
    readmeFile.println(F("The DD is the day"));
    readmeFile.println(F("The L### is the load number, (for example: load 74 = L074"));
    readmeFile.println(F("Example: Load #74 taken on july 4 would be under the following name: 0704L74.txt"));
    readmeFile.println();
    readmeFile.println(F("The format for the comma delimited file is as follows:"));
    readmeFile.println(F("Station code(0), LoadNumber(1), date(2), time(3), hopper weight(4), worm weight(5), mold weight(6), green weight(7), mot is dirt (Y/N)(8), mot is extr (Y/N)(9), mot weight(10), lu weight(11), comm score(12), solids score(13), ph score(14), other defects 1(15), other defects 2(16), other defects 3(17), other defects 4(18), other defects 5(19), other defects 6(20), other defects 7(21), other defects 8(22), other defects 9(23), Regrade(Y/N)(24),"));
    readmeFile.close();
}

//The arduinofunction changes depending on where you are in the code
//so we cannot rely on the variable to be accurate

void saveCurrentConfiguration(int input_function) {
    //These must be confired like this in order for the SD library to work
    char fileName[] = "config.txt";
    SD.remove(fileName); //remove the old configuration file

    File configFile = SD.open(fileName, FILE_WRITE); //makes a new config file
    configFile.close();

    if (!SD.exists(fileName)) { //if the config file could not be made
        Serial.println(F("config file was not made!"));
        lcd_clear();
        lcd_print(L1Ta, F("Faulty SD Card"));
        lcd_print(L2Ta, F("configuration won't"));
        lcd_print(L3Ta, F("be saved"));
        delay(5000);
        lcd_clear();

        return;
    }

    configFile = SD.open(fileName, FILE_WRITE);

    configFile.print(F("data expected color: "));
    configFile.println((int) expectedDevices[0]);


    configFile.print(F("data expected ph: "));
    configFile.println((int) expectedDevices[1]);


    configFile.print(F("data expected refrac: "));
    configFile.println((int) expectedDevices[2]);

    configFile.print(F("data expected weights: "));
    configFile.println((int) expectedDevices[3]);


    configFile.print(F("Station code: "));
    configFile.println(stationCode);

    configFile.print(F("ph timeout(ms): "));
    configFile.println(PH_STABLE_TIMEOUT);

    configFile.print(F("ph epsilon(.xxx): "));
    configFile.println(PH_EPSILON * 1000);


    configFile.print(F("ph reading rate(ms): "));
    configFile.println(PH_READING_RATE);

    configFile.print(F("load number: "));
    configFile.println(LoadNumber);

    configFile.print(F("Function (0= main menu, 1= master, 2 = measuring): "));
    configFile.println(input_function);

    configFile.print(F("Color Listen Flag: "));
    configFile.println((int) listenColor);


    configFile.print(F("pH Listen Flag: "));
    configFile.println((int) listenPh);

    configFile.print(F("Refrac Listen Flag: "));
    configFile.println((int) listenRefrac);

    configFile.print(F("Weight Listen Flag: "));
    configFile.println((int) listenWeight);

    configFile.print(F("AR20 Listen Flag: "));
    configFile.println((int) listenAR20);

    configFile.print(F("LED listen Flag: "));
    configFile.println((int) listenLED);

    configFile.print(F("RFM190 listen Flag: "));
    configFile.println((int) listenRFM190);

    configFile.print(F("VC-505 listen Flag: "));
    configFile.println((int) listenVC505);

    for (int i = 0; i < 10; i++) {//Load Queue
        configFile.print(F("Load Queue: "));
        configFile.println(loadQueue.get(i));
    }

    //Saving the names
    configFile.print(F("Date: "));
    configFile.println(weightFields[DATE]);

    configFile.print(F("Time: "));
    configFile.println(weightFields[TIME]);

    configFile.print(F("Hopper Wt: "));
    configFile.println(weightFields[HOPPERWT]);

    configFile.print(F("Worm: "));
    configFile.println(weightFields[WORM]);

    configFile.print(F("MOLD: "));
    configFile.println(weightFields[MOLD]);

    configFile.print(F("GREEN: "));
    configFile.println(weightFields[GREEN]);

    configFile.print(F("MOT: "));
    configFile.println(weightFields[MOT]);

    configFile.print(F("LU: "));
    configFile.println(weightFields[LU]);

    configFile.print(F("OTHER DEFECT 1: "));
    configFile.println(weightFields[SPARE_1]);

    configFile.print(F("OTHER DEFECT 2: "));
    configFile.println(weightFields[SPARE_2]);

    //Saving the weight fields
    configFile.print(F("Date ON: "));
    configFile.println((int) weightFieldIsOn[DATE]);

    configFile.print(F("Time ON: "));
    configFile.println((int) weightFieldIsOn[TIME]);

    configFile.print(F("Hopper Wt ON: "));
    configFile.println((int) weightFieldIsOn[HOPPERWT]);

    configFile.print(F("Worm ON: "));
    configFile.println((int) weightFieldIsOn[WORM]);

    configFile.print(F("MOLD ON: "));
    configFile.println((int) weightFieldIsOn[MOLD]);

    configFile.print(F("GREEN ON: "));
    configFile.println((int) weightFieldIsOn[GREEN]);

    configFile.print(F("MOT ON: "));
    configFile.println((int) weightFieldIsOn[MOT]);

    configFile.print(F("LU ON: "));
    configFile.println((int) weightFieldIsOn[LU]);

    configFile.print(F("OTHER DEFECT 1 ON: "));
    configFile.println((int) weightFieldIsOn[SPARE_1]);

    configFile.print(F("OTHER DEFECT 2 ON: "));
    configFile.println((int) weightFieldIsOn[SPARE_2]);


    configFile.close();

    Serial.println(F("saving colorSerial"));
    saveSerialToSD(colorSerial, "cSerial.txt");


    Serial.println(F("Saving phSerial"));
    saveSerialToSD(phSerial, "pSerial.txt");


    Serial.println(F("Saving refracSerial"));
    saveSerialToSD(refracSerial, "rSerial.txt");


    Serial.println(F("Saving weightSerial"));
    saveSerialToSD(weightSerial, "wSerial.txt");


}

void DeleteSDCard() {
    lcd_clear();
    lcd_print(L1Ta, F("Delete Files?: "));
    lcd_print(L2Ta, F("1 - Yes"));
    lcd_print(L3Ta, F("2 - No"));

    while (key != '1' && key != '2' && key != CLEAR) getKey();

    if (key == '1') {//They want to delete files
        lcd_clear();
        lcd_print(L1Ta, F("VERY SURE YOU WANT"));
        lcd_print(L2Ta, F("TO DELETE ALL FILES?"));
        lcd_print(L3Ta, F("1 - Yes"));
        lcd_print(L4Ta, F("2 - No"));

        key = 0;
        while (key != '1' && key != '2' && key != CLEAR) getKey();

        if (key == '1') {// they are very sure they want to delete all files
            lcd_clear();
            lcd_print(L1Ta, F("Deleting files.."));
            File root = SD.open("/");
            recursiveSDDelete(root, "/");
        }
        if (key == CLEAR || key == '2') {
            lcd_clear();
            lcd_print(L1Ta, F("No files deleted"));
            delay(2000);
            return;
        }

    }//if they want to delete files

    if (key == CLEAR || key == '2') {
        lcd_clear();
        lcd_print(L1Ta, F("No files deleted"));
        delay(2000);

        return;
    }
}

void recursiveSDDelete(File dir, String ParentName) {
    bool removeAfterGettingNext = false;
    char ParentName_C[25];
    char fileName_C[25];
    while (true) {

        File entry = dir.openNextFile();

        if (removeAfterGettingNext) {
            Serial.print(F("Deleting: "));
            Serial.println(fileName_C);
            SD.remove(fileName_C);
        }

        if (!entry) {
            // no more files left in root
            Serial.print(F("Deleting (DIR): "));
            Serial.println(ParentName);

            ParentName.toCharArray(ParentName_C, 25);
            SD.rmdir(ParentName_C);
            break;
        }


        Serial.print(entry.name());

        if (entry.isDirectory()) {//if it is a directory
            removeAfterGettingNext = false;
            ParentName += entry.name();
            ParentName += "/";
            Serial.println(F("/"));

            recursiveSDDelete(entry, ParentName);

            //This part of the program will run after all subfiles are removed

        } else {//if it is a file
            // files have sizes, directories do not
            String FilePath = ParentName;
            FilePath += entry.name();

            Serial.print(F("\t\t"));
            Serial.println(entry.size(), DEC);
            FilePath.toCharArray(fileName_C, 25);
            removeAfterGettingNext = true;
        }
        entry.close();
    }
}

#endif	/* SDFUNCTIONS_H */

