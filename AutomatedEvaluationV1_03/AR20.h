/* 
 * File:   pH.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 9:27 AM
 */

#ifndef AR20_H
#define	AR20_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "AR20Scanner.h"

/******************************************************/

void phTimerFunction() {
    doneWaiting = true;
}//phTimerFunction

/******************************************************/
bool getPhBuffer() { // gets ph after 
    phBuffer = "";
    Timer phTimer;
    phTimer.every(7000, phTimerFunction); //ph Timeout interval
    int unresponsiveCounter = 0;
    delay(20);

    if (button1IsPressed() || listenPh) { // go into the loop waiting for ph
        //Serial.println(F("Trying to get ph stuff"));
        byte phCounter = 0;
        while (1) {//getting ph
            phTimer.update();

            if (doneWaiting) {
                unresponsiveCounter++;
                //Serial.println(F("DoneWaiting was enabled"));
                doneWaiting = false;
                measurements_menu[PHLINE] = "pH Unresponsive";
                phUnresponsive = true;
                ExitFlag = true;
                return 0;
            }

            getAR20();

            if (incoming != NODATA) //debug

                if (incoming == 'D') {
                    while (incoming != ':') {
                        getAR20();
                        if (incoming != NODATA);
                    }
                    while (incoming != 'p') {
                        getAR20();
                        if (incoming != NODATA);
                    }
                    while (incoming != 'H') {
                        getAR20();
                        if (incoming != NODATA);
                    }
                    while (incoming != 't') {
                        getAR20();
                        if (incoming != NODATA);
                    }
                    while (incoming != ':') {
                        getAR20();
                        if (incoming != NODATA);
                    }
                    while (incoming < '0' || incoming > '9') {
                        getAR20();
                        if (incoming != NODATA);
                    }

                    while (1) {//keeps looping to get the rest of the pH

                        if (incoming != NODATA) {
                            if (phCounter == 4) {
                                phBuffer += (char) incoming;
                                return true;
                            }

                            if (phCounter == 1 && incoming != '.')
                                break;

                            phBuffer += (char) incoming;
                            phCounter++;
                        }
                        getAR20();
                    }//while looping looping for ph
                }//found 'D'

        }//getting ph
    }
    return true;

}//void getPhBuffer()

/******************************************************/

void MeasuringAR20() {

    phSerial.begin(2400); // AR 20 - 9600 baud rate

    for (int i = 0; i < 15; i++) {//clearing out the pH buffer
        phBuffer[i] = '\0';
    }

    Timer phTimer;
    int timeoutEvent = phTimer.after(PH_STABLE_TIMEOUT, enableExitFlag);

    int measurerEvent = phTimer.every(PH_READING_RATE, readPh);

    int phBlinkEvent = phTimer.every(200, redIndicatorBlink);

    digitalWrite(indicatorGreen, LOW);

    phReadingCount = 0;
    phReadings = new float[50];

    readPh();

    while (1) {
        phTimer.update();
        if (ExitFlag) {
            ExitFlag = false; //resets the flag
            delete phReadings;
            break;
        }
    }//while(1)

    phTimer.stop(timeoutEvent);
    phTimer.stop(measurerEvent);
    phTimer.stop(phBlinkEvent);

    digitalWrite(indicatorRed, LOW);
    digitalWrite(indicatorGreen, HIGH);
}//MeasuringAR20

//--------------------------------------------------------

void enableExitFlag() {
    ExitFlag = true;
}

//--------------------------------------------------------

void readPh() {

    AR20Scanner phScanner(&phSerial);
    phBuffer = phScanner.getPh();
    phReadings[phReadingCount] = atof(&phBuffer[0]);

    Serial.print("This is the ph: ");
    Serial.println(phBuffer);


    if (phScanner.unresponsive) {
        Serial.println("It is unresponsive");
        ExitFlag = true;
        phUnresponsive = true;
    }

    if (phReadingCount >= 1) {
        if (checkDifference(phReadings[phReadingCount],
                phReadings[phReadingCount - 1], PH_EPSILON)) {
            ExitFlag = true;
        }
    }

    phReadingCount++;
}//readPh())

//--------------------------------------------------------

bool checkDifference(float first_measure, float second_measure, float epsilon_F) {
    int first = 1000 * first_measure;
    int second = 1000 * second_measure;
    int epsilon = epsilon_F * 1000;

    int difference = first - second;
    difference = abs(difference);

    if (difference < epsilon)
        return true;
    else
        return false;

}//checkDifference()


#endif	/* AR20_H */

