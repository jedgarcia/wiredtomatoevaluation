/* 
 * File:   MasterReceiverFunctions.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 10:45 AM
 */

#ifndef MASTERRECEIVERFUNCTIONS_H
#define	MASTERRECEIVERFUNCTIONS_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "Hopper.h"
#include "RegradeFunctions.h"
#include "LoadFunctions.h"


//***************************************

void sendToGradestar(SoftwareSerial & gradestar, File& file) {
    file.seek(0);

    String dummyBuffer;

    gradestar.write(2);
    gradestar.println();
    gradestar.println(F("<xml>"));

    //The station is saved onto the sd files, but till not be sent to gradestar
    //1 - station
    //gradestar.print(F("<station>"));
    //gradestar.print(readNextLoadValue(file));
    readNextLoadValue(file);
    //gradestar.println(F("</station>"));

    //2 - load number
    gradestar.print(F("<loadnumber>"));
    gradestar.print(readNextLoadValue(file)); //minus one because this is called after the load number is incremented
    gradestar.println(F("</loadnumber>"));



    //3 - DATE
    gradestar.print(F("<date>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</date>"));
    //END-OF-DATE-----------------------------------------


    //4 - TIME
    gradestar.print(F("<time>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</time>"));
    //END-OF-TIME -----hehehe--------------------------------------


    //5 - Hopper Wt
    gradestar.print(F("<hopperwt>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</hopperwt>"));

    //6 - Worm
    gradestar.print(F("<wormwt>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</wormwt>"));

    //7 - MOLD
    gradestar.print(F("<moldwt>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</moldwt>"));

    //8 - Green
    gradestar.print(F("<greenwt>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</greenwt>"));

    //9 - MOT
    gradestar.print(F("<motwt dirt=\""));
    dummyBuffer = readNextLoadValue(file);
    dummyBuffer.toLowerCase();
    gradestar.print(dummyBuffer); //dirt -- 'y' or 'n'

    dummyBuffer = readNextLoadValue(file);
    dummyBuffer.toLowerCase();
    gradestar.print(F("\" extr=\""));
    gradestar.print(dummyBuffer); //extr - 'y' or 'n'


    gradestar.print(F("\">"));
    gradestar.print(readNextLoadValue(file));
    //<motwt dirt="y" extr="n">
    gradestar.println(F("</motwt>"));
    //END-OF-MOT---------------------------------------------


    //10 - Limited USE
    gradestar.print(F("<luwt>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</luwt>"));
    //END-OF-LIMITED-USE----------------------------------


    //11 - color
    gradestar.print(F("<comm>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</comm>"));


    //12 - refrac
    gradestar.print(F("<solids>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</solids>"));

    //13 - pH
    gradestar.print(F("<ph>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</ph>"));

    //14 - Spare 1
    gradestar.print(F("<otherdefect1>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</otherdefect1>"));

    //15 - Spare 2
    gradestar.print(F("<otherdefect2>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</otherdefect2>"));


    gradestar.print(F("<otherdefect3>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</otherdefect3>"));


    gradestar.print(F("<otherdefect4>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</otherdefect4>"));


    gradestar.print(F("<otherdefect5>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</otherdefect5>"));


    gradestar.print(F("<otherdefect6>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</otherdefect6>"));


    gradestar.print(F("<otherdefect7>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</otherdefect7>"));


    gradestar.print(F("<otherdefect8>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</otherdefect8>"));


    gradestar.print(F("<otherdefect9>"));
    gradestar.print(readNextLoadValue(file));
    gradestar.println(F("</otherdefect9>"));

    gradestar.print(F("<regrade>"));
    String regrade = readNextLoadValue(file);
    regrade.toLowerCase();
    gradestar.print(regrade);
    gradestar.println(F("</regrade>"));

    gradestar.println(F("</xml>"));
    gradestar.write(3);
}


//*************************************** 

void getLoadNumberFromSerial(HardwareSerial& SerialChosen, char* dummyLoad) {
    //gets Load Number from the slave and puts in its correct place
    int i = 0;
    while (1) {
        incoming = SerialChosen.read();
        if (incoming != NODATA) {
            if (incoming == '\n') {
                dummyLoad[i] = '\0';
                break;
            }
            dummyLoad[i++] = incoming;


        }
    }
}

void getLoadNumberFromSerial(SoftwareSerial& SerialChosen, char* dummyLoad) {

    //gets Load Number from the slave and puts in its correct place
    int i = 0;
    while (1) {
        incoming = SerialChosen.read();
        if (incoming != NODATA) {
            if (incoming == '\n') {
                dummyLoad[i] = '\0';
                break;
            }
            dummyLoad[i++] = incoming;


        }
    }
}

//***************************************

void getIncomingMeasures(HardwareSerial & SerialChosen) {
    //Received in alphabetical order
    char tempStringToCharArray[20] = {'\0'};

    //color=====================================================================

    Serial.println(F("Right before COLOR getting measurements"));
    if (ReceivingDevicesFlags[0] && expectedDevices[0]) {//get Color Measurements
        COLORLINE = measurements_menu_size++;
        Serial.println(F("WE ARE GETTING COLOR WHYYY"));
        colorBuffer = getMeasurement(SerialChosen);
        getRidOfSpaces(colorBuffer);
        SerialChosen.print(CONFIRMATION);

        if (COLORLINE > 4)
            menu_frame = COLORLINE - 4;

        measurements_menu[COLORLINE] = "Color: ";
        measurements_menu[COLORLINE] += colorBuffer;
        //displayMenuMaster(false, measurements_menu, COLORLINE, menu_frame);
    }//if we need to get the LED measurement
    if (ReceivingDevicesFlags[0] && !expectedDevices[0]) {//slave is going to send color but we dont expect it
        COLORLINE = measurements_menu_size++;
        delay(10);
        SerialChosen.flushBuffer(); //delete the remaining message in the buffer
        SerialChosen.print(CONFIRMATION);
        measurements_menu[COLORLINE] = "Color:  sent wrongly";
        Serial.println(F("color Sent wrongly"));
    }
    //    else if (!expectedDevices[0]) {//we dont expect this at all
    //        measurements_menu[COLORLINE] = "Color:  ------------";
    //    }

    Serial.print(F("Receiving devices flag[0]: "));
    Serial.println((int) ReceivingDevicesFlags[0]);

    Serial.print(F("expected devices [0]: "));
    Serial.println((int) expectedDevices[0]);




    //PH========================================================================

    Serial.println(F("Right before getting ph"));
    if (ReceivingDevicesFlags[1] && expectedDevices[1]) {//get pH
        PHLINE = measurements_menu_size++;
        phBuffer = getMeasurement(SerialChosen);
        getRidOfSpaces(phBuffer);
        SerialChosen.print(CONFIRMATION);

        measurements_menu[PHLINE] = "pH: ";
        measurements_menu[PHLINE] += phBuffer;

        if (PHLINE > 4)
            menu_frame = PHLINE - 4;
        //displayMenuMaster(false, measurements_menu, PHLINE, menu_frame);
    }
    if (ReceivingDevicesFlags[1] && !expectedDevices[1]) {//the slave told us it was going to send the ph, but we weren't expecting it
        PHLINE = measurements_menu_size++;
        //act like its okay, and send the slave back a confirmation
        delay(10);
        SerialChosen.flushBuffer(); //delete the remaining message in the buffer
        SerialChosen.print(CONFIRMATION);
        measurements_menu[PHLINE] = "pH:     sent wrongly";
    }
    //    else if (!expectedDevices[1]) {//we dont expect this at all
    //        measurements_menu[PHLINE] = "pH: ----------------";
    //    }

    Serial.print(F("Receiving devices flag[1]: "));
    Serial.println((int) ReceivingDevicesFlags[1]);

    Serial.print(F("expected devices [1]: "));
    Serial.println((int) expectedDevices[1]);


    //refraction================================================================

    Serial.println(F("Right before refraction"));
    if (ReceivingDevicesFlags[2] && expectedDevices[2]) { //get Refraction
        REFRACLINE = measurements_menu_size++;
        Serial.println(F("INSIDE REFRAC"));
        refracBuffer = getMeasurement(SerialChosen);
        getRidOfSpaces(refracBuffer);
        SerialChosen.print(CONFIRMATION);

        measurements_menu[REFRACLINE] = "Refrac: ";
        measurements_menu[REFRACLINE] += refracBuffer;


        if (REFRACLINE > 4)
            menu_frame = REFRACLINE - 4;


        //displayMenuMaster(false, measurements_menu, REFRACLINE, menu_frame);
    }
    if (ReceivingDevicesFlags[2] && !expectedDevices[2]) {
        REFRACLINE = measurements_menu_size++;
        SerialChosen.flushBuffer(); //delete the remaining message in the buffer
        SerialChosen.print(CONFIRMATION);
        measurements_menu[REFRACLINE] = "Refrac: sent wrongly";
    }
    //    else if (!expectedDevices[2]) {//we dont expect this at all
    //        measurements_menu[REFRACLINE] = "Refrac: ------------";
    //    }

    Serial.print(F("Receiving devices flag[2]: "));
    Serial.println((int) ReceivingDevicesFlags[2]);

    Serial.print(F("expected devices [2]: "));
    Serial.println((int) expectedDevices[2]);


    //weights===================================================================

    Serial.println(F("Right before getting weights"));
    if (ReceivingDevicesFlags[3] && expectedDevices[3]) {//get Weight

        setToFalse(masterWeightIsOn, maximumHopperEntries);

        for (int i = 0; i < maximumHopperEntries; i++) {
            Serial.print(weightFields[i]);
            Serial.print(" Flag: ");

            while (!SerialChosen.available()); //waits until the serial port gets a flag
            incoming = SerialChosen.read();
            masterWeightIsOn[i] = (bool) (incoming - '0');


            Serial.println((char) incoming);
            SerialChosen.print(CONFIRMATION);
        }
        Serial.println("after getting all weight flags");

        WEIGHTLINE = measurements_menu_size;
        measurements_menu_size += countmasterWeightsExpected();

        //These will be 10 measurements coming in separated by a  newline \n
        int weightCounter = 0;
        for (int i = 0; i < maximumHopperEntries; i++) {
            if (masterWeightIsOn[i]) {
                //getting the measurement
                Serial.print("About To get: ");
                Serial.println(weightFields[i]);

                weightBuffers[i] = getMeasurement(SerialChosen);
                if (weightFieldIsOn[i])
                    measurements_menu[weightCounter + WEIGHTLINE] = weightBuffers[i];
                else {
                    measurements_menu[weightCounter + WEIGHTLINE] = weightFields[i];
                    measurements_menu[weightCounter + WEIGHTLINE] += " -NOT EXP-";
                }
                //Sending confirmation - we got the measurement
                SerialChosen.print(CONFIRMATION);

                if (WEIGHTLINE + weightCounter > 4)
                    menu_frame = WEIGHTLINE + weightCounter - 3;
                weightCounter++;

            }//if this is an expected weight
        }//for collecting all of the weights
        Serial.print(F("The menu size right after getting the measurements: "));
        Serial.println(measurements_menu_size);
    }//if we are expecting weights

    //We are getting the weights sent to us wrongly
    if (ReceivingDevicesFlags[3] && !expectedDevices[3]) {

        setToFalse(masterWeightIsOn, maximumHopperEntries);

        for (int i = 0; i < maximumHopperEntries; i++) {
            while (!SerialChosen.available()); //waits until the serial port gets a flag
            masterWeightIsOn[i] = (bool) Serial.read();
            SerialChosen.print(CONFIRMATION);
        }

        WEIGHTLINE = measurements_menu_size;
        measurements_menu_size += countWeightsExpected();

        for (int i = 0; i < 10; i++) {
            if (masterWeightIsOn[i]) {
                getMeasurement(SerialChosen);
                SerialChosen.print(CONFIRMATION);

                measurements_menu[WEIGHTLINE + i] = "Weight: sent wrongly";
                if (WEIGHTLINE + i > 4)
                    menu_frame = WEIGHTLINE + i - 3;
            }
        }
    }

    Serial.print(F("Receiving devices flag[3]: "));
    Serial.println((int) ReceivingDevicesFlags[3]);

    Serial.print(F("expected devices [3]: "));
    Serial.println((int) expectedDevices[3]);


    //==========================================================================


}//getIncoming

//***************************************

void getRefracFromSerial(HardwareSerial & SerialChosen) {
    while (1) { //waits until a valid number is sent
        //Serial.println("WAITING FOR INCOMING NUMBER");

        incoming = SerialChosen.read();
        if (incoming != NODATA) {
            readFromBSBufferSerial((char) incoming, SerialChosen);
            break;
        }//while waiting for valid number
    }

    //Serial.println("We got the index of refraction");
}


//***************************************

String readFromBSBufferSerial(char first, HardwareSerial & SerialChosen) {

    //only the first 5 chars are important but there are 7 sent

    refracBuffer[0] = first;
    for (int i = 1; i < 7; i++) {
        while (1) {
            incoming = SerialChosen.read();
            if (i >= 5) {
                refracBuffer[i] = '\0';
                break;
            }
            if (incoming != NODATA && i < 5) {
                refracBuffer[i] = (char) incoming;
                break;
            }//if getting refracBuffer data
        }//while getting refracBuffer
    }//for getting refracBuffer	

    for (int i = 0; i < 20; i++)
        SerialChosen.read();

    return refracBuffer;
}

//***************************************

void getPhBufferFromSerial(HardwareSerial & SerialChosen) {

    byte phCounter = 0;
    for (int i = 0; i < 15; i++) {
        phBuffer[i] = 0;
    }

    incoming = SerialChosen.read();

    while (1) {//getting ph

        if (incoming != NODATA) {
            phBuffer[phCounter] = (char) incoming;
            ++phCounter;
        }

        //message is done when '\n' is sent
        if ((char) incoming == '\0')
            break;

        incoming = SerialChosen.read();
    }//loop to find ph
}

//***************************************

void getIncomingFlag(HardwareSerial &SerialChosen, int flagIndex) {
    incoming = SerialChosen.read();
    while (incoming == NODATA) {
        blinkTimer.update();
        incoming = SerialChosen.read(); //waiting until first flag is sent
    }
    if (incoming) ReceivingDevicesFlags[flagIndex] = true;
    else ReceivingDevicesFlags[flagIndex] = false;

    switch (flagIndex) {
        case 0:
            if (incoming)
                listenPh = true;
            break;
        case 1:
            if (incoming)
                listenColor = true;
            break;
        case 2:
            if (incoming)
                listenRefrac = true;
            break;
        case 3:
            if (incoming)
                listenWeight = true;
            break;
    }
}

//***************************************

byte countRecievedFlags() {
    byte counterOfFlags;

    for (int i; i < ReceivingDevicesSize; i++)
        if (ReceivingDevicesFlags[i])
            counterOfFlags++;
        else continue;

    return counterOfFlags;
}

//***************************************

void getData(HardwareSerial& SerialChosen, int loadNumberIndex) {

    //We are picking up assuming the slave has just sent us an 'S' - Sending
    SerialChosen.print(CONFIRMATION); //confirming we have gotten their SENDING

    //The other Arduino will  now tell us the values of their flags
    //in alphabetical order - must always listen for ALL flags (even if false))
    for (int i = 0; i < 4; i++) {
        getIncomingFlag(SerialChosen, i);
    }

    //Send slave a confirmation that we got the flags it was sending us
    SerialChosen.print(CONFIRMATION);

    Serial.println(F("Confirmation sent - we got all flags")); //debug

    getIncomingMeasures(SerialChosen);

    Serial.println(F("We got all the measurements"));

}//get data - receives measurements from the slave
//***************************************

void masterReceiver() {
    HardwareSerial SerialArray[4] = {Serial, Serial1, Serial2, Serial3};
    Serial1.begin(9600);
    delay(50);
    Serial2.begin(9600);
    delay(50);
    Serial3.begin(9600);
    delay(50);
    digitalWrite(buttonLED, HIGH);

    FlushAllSerial();
    lcd_clear();

    if (!expectedDevices[0] && !expectedDevices[1] && !expectedDevices[2] && !expectedDevices[3]) {//if were not listening for any devices
        lcd_print(lines[0], F("Not listening for"));
        lcd_print(lines[1], F("any devices"));
        delay(3000);
        return;
    }


    menu_frame = 0;
    measurements_menu_size = 1;

    measurements_menu[LOADNUMBERLINE] = " Recently Received ";
    displayMenuMaster(false, measurements_menu, measurements_menu_size, menu_frame);

    saveCurrentConfiguration(MASTER_MODE);

    while (1) {
        getKey();

        if (key == CLEAR || main_menu_flag) {
            clearListenFlags();
            clearMeasurementsReceived();
            saveCurrentConfiguration(MAIN_MENU_MODE);
            main_menu_flag = false;
            turnIndicator("green");
            break; // breaks the while loop, returning to the function selection
        }//if CLEAR

        //Checking to see if the slaves sent us anything
        for (int i = 0; i < 4; i++) {
            if (!SerialTryingToSend[i])
                incoming = (SerialArray[i]).read();
            if (incoming != NODATA) {
                (SerialArray[i]).print((char) incoming);
            }
            if (incoming == PING) {
                (SerialArray[i]).print(CONFIRMATION);
            }
            if (incoming == SENDING || SerialTryingToSend[i]) {
                SerialTryingToSend[i] = false;
                getData((SerialArray[i]), i);
                loadNumberExchange(SerialArray[i]);
                clearMeasurementsReceived();
                menu_frame = 0;
                displayMenuMaster(true, measurements_menu, measurements_menu_size,
                        menu_frame);
                measurements_menu_size = 1;
            }
        }
    }//while
}//masterReceiver

String getMeasurement(HardwareSerial & serialChosen) {
    String tempBuffer = "";

    delay(15);
    while ((incoming = serialChosen.read()) != '\n') {
        blinkTimer.update();
        //Serial.println(F("Getting measurement"));
        if (incoming != NODATA) {
            //Serial.println((char) incoming);
            tempBuffer += (char) incoming;
        }
    }
    return tempBuffer.substring(0, tempBuffer.length() - 1);
}

//***************************************
#endif	/* MASTERRECEIVERFUNCTIONS_H */

