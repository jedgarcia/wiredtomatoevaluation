/* 
 * File:   QueueFunctions.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on September 18, 2013, 5:30 PM
 */

#ifndef QUEUEFUNCTIONS_H
#define	QUEUEFUNCTIONS_H
#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "SimpleVector.h"
#include "SDFunctions.h"

void sendFinishedQueuedLoads(bool isRegrade) {

    int arraySize = loadQueue.getSize();
    int tempArray[arraySize];

    for (int i = 0; i < loadQueue.getSize(); i++) {//puts the data from the vector to the array
        tempArray[i] = loadQueue.get(i);
    }

    for (int i = 0; i < arraySize; i++) {
        //getting the temporary location
        String filepath = "TEMP/";
        filepath += tempArray[i];
        filepath += ".txt";

        if (!SD.exists(&filepath[0])) {//if the file doesnt exist, its stale delete it from the queue
            loadQueue.removeData(tempArray[i]);
            continue;//no use going to check the loads if it doesnt exist
        }

        if (loadIsDone(&filepath[0], isRegrade)) {
            sendFileAndRemoveFromQueue(&filepath[0], isRegrade);
        }
    }


}


#endif	/* QUEUEFUNCTIONS_H */

