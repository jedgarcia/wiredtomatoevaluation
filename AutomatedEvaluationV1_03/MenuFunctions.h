/* 
 * File:   MenuFunctions.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 10:39 AM
 */

#ifndef MENUFUNCTIONS_H
#define	MENUFUNCTIONS_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "Button.h"

//==============================================================================
//MENU FUNCTIONS

//***************************************

//prints out "Incorrect Value!" to lcd

void IncorrectValueEntered() {
    lcd_clear();
    lcd_print(L1Ta, F("INCORRECT VALUE"));
    delay(500);
    lcd_clear();
}

//***************************************

//This function prints out 4 lines of the menu on the LCD
//starting from the byte value that is passed in
//crucial part of the menu's scrollability

void printFourFromReference(byte frame, const String incoming_menu[]) {
    lcd_clear();

    //prints out four lines
    //starting with the menu[frame]
    for (int i = 0; i < 4; i++) {
        lcd_println(lines[i], incoming_menu[frame]);
        frame++;
    }//for printing out the four lines using frame
}

void printFourFromReference(byte frame, const __FlashStringHelper* incoming_menu[]) {

    lcd_clear();

    //prints out four lines
    //starting with the menu[frame]
    for (int i = 0; i < 4; i++) {
        lcd_println(lines[i], incoming_menu[frame]);
        frame++;
    }//for printing out the four lines using frame


}

void displayMenuMaster(bool scrollable, String incoming_menu[], int incoming_size,
        int& frame) {

    Serial.print(F("freeMemory()="));
    Serial.println(freeMemory());



    lcd_clear();
    HardwareSerial SerialArray[4] = {Serial, Serial1, Serial2, Serial3};
    Serial.println(F("Showing MENU"));
    key = NULL;

    if (scrollable)
        Serial.println(F("it is scrollable"));

    Serial.print(F("incoming size: "));
    Serial.println(incoming_size);

    if (incoming_size > 3) {
        printFourFromReference(frame, incoming_menu); //prints out the first four menu options
    } else {
        Serial.println(F("Small menu!"));
        for (int i = 0; i < incoming_size; i++)
            lcd_print(lines[i], incoming_menu[i]);
    }

    Serial.println(F("After displaying printfour"));

    while (scrollable) {
        Serial.print(F("Size of menu: "));
        Serial.println(incoming_size);
        Serial.println(F("ITS SCROLLABLE"));
        if (incoming_size > 3) {
            printFourFromReference(frame, incoming_menu); //prints out the first four menu options
        } else {
            Serial.println(F("Small menu!"));
            for (int i = 0; i < incoming_size; i++)
                lcd_print(lines[i], incoming_menu[i]);
        }
        key = NULL;



        while (key == NULL || key == DOWN || key == UP || key == HELP) {//as long as the user is trying to scroll or there is no transmission keep listening
            blinkTimer.update();

            getKey();
            if (key == DOWN && frame < (incoming_size - 4)) {
                frame++;
                break;
            } //user wants to scroll down

            if (key == UP && frame > 0) {
                frame--;
                break;
            }//user wants to scroll up

            if (key == CLEAR || main_menu_flag) {
                lcd_clear();
                main_menu_flag = true;
                return;
            }

            for (int i = 0; i < 4; i++) {

                incoming = (SerialArray[i]).read();
                if (incoming == PING) {
                    (SerialArray[i]).print(CONFIRMATION);
                } else if (incoming == SENDING) {
                    SerialTryingToSend[i] = true;
                    lcd_clear();
                    return;
                }
            }
        }// this while loop ends when the user enters a scroll and it changes the startingFrame accordingly
    }
}

void displayMenu(bool scrollable, String incoming_menu[], int incoming_size,
        int& frame) {

    key = NULL;

    if (incoming_size > 3) {//if it even needs to be a scrolling menu
        printFourFromReference(frame, incoming_menu); //prints out the first four menu options
    } else {//less than 4 items just print them on screen
        for (int i = 0; i < incoming_size; i++) {
            lcd_println(lines[i], incoming_menu[i]);
        }
    }

    while (scrollable) {

        Serial.print(F("Size of menu: "));
        Serial.println(incoming_size);
        if (incoming_size > 3) {//if it even needs to be a scrolling menu
            printFourFromReference(frame, incoming_menu); //prints out the first four menu options
        } else {//less than 4 items just print them on screen
            for (int i = 0; i < incoming_size; i++) {
                lcd_println(lines[i], incoming_menu[i]);
            }
        }
        key = NULL;

        while (key == NULL || key == DOWN || key == UP || key == HELP) {//as long as the user is trying to scroll or there is no transmission keep listening

            //if (AlreadySentMyMeasurements) {//constantly need to see if the master wants to change load number
            if (masterAndGradestarSerial.available()) {
                incoming = masterAndGradestarSerial.read();
                if (incoming == 'Y') {
                    digitalWrite(buttonLED, HIGH);
                    getLoadNumberFromSerial(masterAndGradestarSerial, loadNumberAscii);
                    LoadNumber = atoi(loadNumberAscii);
                    lcd_clear();
                    lcd_print(L1Ta, F("This load is done."));
                    delay(3000);
                    //measurements_menu_size = 1;
                    frame = 0;
                    AlreadySentMyMeasurements = false;

                    measurements_menu[0] = "Load#: ";
                    measurements_menu[0] += LoadNumber - 1;
                    measurements_menu[0] += " done";

                    if (incoming_size > 3) {//if it even needs to be a scrolling menu
                        printFourFromReference(frame, incoming_menu); //prints out the first four menu options
                    } else {//less than 4 items just print them on screen
                        for (int i = 0; i < incoming_size; i++) {
                            lcd_println(lines[i], incoming_menu[i]);
                        }
                    }

                    stopBlinkingEvent();
                    turnIndicator("green");
                }

            }
            //}


            getKey();
            if (incoming_size > 3) {
                if (key == DOWN && frame < (incoming_size - 4)) {
                    frame++;
                    break;
                } //user wants to scroll down

                if (key == UP && frame > 0) {
                    frame--;
                    break;
                }//user wants to scroll up
            }

            if (button1IsPressed()) {
                measureFlag = true;
                lcd_clear();
                return;
            }

            if (listenColor && listenLED) {
                if (colorSerial.available()) {
                    Serial.println(F("In menu about to clear - color"));
                    lcd_clear();
                    if ((incoming = colorSerial.read()) == 'L') {
                        delay(10);
                        if ((incoming = colorSerial.read()) == 'O') {
                            delay(10);
                            if ((incoming = colorSerial.read()) == 'A') {
                                delay(10);
                                if ((incoming = colorSerial.read()) == 'D') {

                                    int color = readFromLedColorMeter();

                                    Serial.print(F("color measurement is: "));
                                    Serial.println(color);

                                    measurements_menu[COLORLINE] = "LED: ";
                                    measurements_menu[COLORLINE] += color;

                                    menu_frame = 0;
                                    LEDHasBeenMeasured = true;
                                    measureFlag = true;
                                    return;
                                }
                            }
                        }
                    }

                }
            }

            //measuring the weight without pressing a button
            if (listenWeight && listenVC505) {
                if (weightSerial.available()) {
                    Serial.println(F("in menu about to clear screen -weight"));
                    lcd_clear();

                    Serial.println(F("Doing weight things"));

                    readFromHopper();
                    //readFromVC505();
                    if (main_menu_flag) {
                        main_menu_flag = false;
                        measureFlag = false;
                        return;
                    } else {//we actually want to record
                        //lcd_clear();
                        measureFlag = true;
                        VC505HasBeenMeasured = true;
                        //blinkDuringOngoing = blinkTimer.every(200, greenIndicatorBlink);
                        return;
                    }
                }
            }

            if (key == HELP) {
                testing_dont_send = true;
                lcd_clear();
                return;
            }
            if (key == CLEAR) {
                main_menu_flag = true;
                measureFlag = false;
                lcd_clear();
                return;
            }


        }// this while loop ends when the user enters a scroll and it changes the startingFrame accordingly
    }
}

void MenuButton(String incoming_menu[], int incoming_size) {
    bool secondButton = false;
    byte startingFrame = 0;
    key = NULL;

    //this is going to keep outputting the options untill the user enters in a value
    while (key == NULL || key == DOWN || key == UP) {//as long as the user is trying to scroll or there is no transmission keep listening


        printFourFromReference(startingFrame, incoming_menu); //prints out the first four menu options


        while (key == NULL || key == DOWN || key == UP || key == SECOND) {//as long as the user is trying to scroll or there is no transmission keep listening
            if (!secondButton) {
                getKey(); //reads a new value for incoming - could be either scroll up or down or the value for the menu         
            } else {
                secondButton = false;
                break;
            }

            if (key == DOWN && startingFrame < (incoming_size - 4)) {
                startingFrame++;
                break;
            } //user wants to scroll down

            if (key == UP && startingFrame > 0) {
                startingFrame--;
                break;
            }//user wants to scroll up

            if (button1IsPressed()) {
                key = BUTTONNUMBER;
                return;
            }

        }// this while loop ends when the user enters a scroll and it changes the startingFrame accordingly
    }//while listening for user response to menu  

}//askForArduinoFunction

void Menu(String incoming_menu[], int incoming_size) {
    bool secondButton = false;
    byte startingFrame = 0;
    key = NULL;

    //this is going to keep outputting the options untill the user enters in a value
    while (key == NULL || key == DOWN || key == UP) {//as long as the user is trying to scroll or there is no transmission keep listening


        printFourFromReference(startingFrame, incoming_menu); //prints out the first four menu options


        while (key == NULL || key == DOWN || key == UP || key == SECOND) {//as long as the user is trying to scroll or there is no transmission keep listening
            if (!secondButton) {
                getKey(); //reads a new value for incoming - could be either scroll up or down or the value for the menu         
            } else {
                secondButton = false;
                break;
            }

            if (key == DOWN && startingFrame < (incoming_size - 4)) {
                startingFrame++;
                break;
            } //user wants to scroll down

            if (key == UP && startingFrame > 0) {
                startingFrame--;
                break;
            }//user wants to scroll up

        }// this while loop ends when the user enters a scroll and it changes the startingFrame accordingly
    }//while listening for user response to menu  

    arduinoFunction = key; // The last incoming to come out of the while loops is the correct value

}//askForArduinoFunction

void Menu(const __FlashStringHelper * incoming_menu[], int incoming_size) {
    bool secondButton = false;
    byte startingFrame = 0;
    key = NULL;

    //this is going to keep outputting the options untill the user enters in a value
    while (key == NULL || key == DOWN || key == UP) {//as long as the user is trying to scroll or there is no transmission keep listening
        printFourFromReference(startingFrame, incoming_menu); //prints out the first four menu options
        while (key == NULL || key == DOWN || key == UP || key == SECOND) {//as long as the user is trying to scroll or there is no transmission keep listening
            getKey(); //reads a new value for incoming - could be either scroll up or down or the value for the menu         
            if (key == DOWN && startingFrame < (incoming_size - 4)) {
                startingFrame++;
                break;
            } //user wants to scroll down

            if (key == UP && startingFrame > 0) {
                startingFrame--;
                break;
            }//user wants to scroll up

        }// this while loop ends when the user enters a scroll and it changes the startingFrame accordingly
    }//while listening for user response to menu  

    arduinoFunction = key; // The last incoming to come out of the while loops is the correct value

}//askForArduinoFunction

byte mainMenu(const __FlashStringHelper * incoming_menu[], int incoming_size) {
    byte startingFrame = 0;
    char response[3] = {'\0'};
    byte array_counter = 0;
    key = NULL;

    //this is going to keep outputting the options untill the user enters in a value
    while (key == NULL || key == DOWN || key == UP) {//as long as the user is trying to scroll or there is no transmission keep listening

        printFourFromReference(startingFrame, incoming_menu); //prints out the first four menu options


        while (key == NULL || key == DOWN || key == UP || key == SECOND) {//as long as the user is trying to scroll or there is no transmission keep listening
            getKey();

            if (key == DOWN && startingFrame < (incoming_size - 4)) {
                startingFrame++;
                break;
            } //user wants to scroll down

            if (key == UP && startingFrame > 0) {
                startingFrame--;
                break;
            }//user wants to scroll up

            if (key >= '0' && key <= '9') {
                response[array_counter++] = key;

                if (array_counter >= 2) {//were finished getting the response
                    response[array_counter] = '\0';
                    key = atoi(response);
                    return key;
                } else {
                    key = NULL; //done so that we loop again
                }
            }

        }// this while loop ends when the user enters a scroll and it changes the startingFrame accordingly
    }//while listening for user response to menu  
}//askForArduinoFunction

byte Menu2Digits(const String incoming_menu[], int incoming_size) {
    byte startingFrame = 0;

    char response[3] = {'\0'};
    byte array_counter = 0;

    key = NULL;

    //this is going to keep outputting the options untill the user enters in a value
    while (key == NULL || key == DOWN || key == UP) {//as long as the user is trying to scroll or there is no transmission keep listening

        if (incoming_size > 4)
            printFourFromReference(startingFrame, incoming_menu); //prints out the first four menu options
        else {
            for (int i = 0; i < incoming_size; i++) {
                lcd_println(lines[i], incoming_menu[i]);
            }
        }

        while (key == NULL || key == DOWN || key == UP || key == SECOND) {//as long as the user is trying to scroll or there is no transmission keep listening
            getKey();

            if (key == DOWN && startingFrame < (incoming_size - 4)) {
                startingFrame++;
                break;
            } //user wants to scroll down

            if (key == UP && startingFrame > 0) {
                startingFrame--;
                break;
            }//user wants to scroll up

            if (key >= '0' && key <= '9') {
                response[array_counter++] = key;

                if (array_counter >= 2) {//were finished getting the response
                    response[array_counter] = '\0';
                    key = atoi(response);

                    return key;

                } else {
                    key = NULL; //done so that we loop again
                }
            }




        }// this while loop ends when the user enters a scroll and it changes the startingFrame accordingly
    }//while listening for user response to menu  

}//askForArduinoFunction

//***************************************

void masterCheckMenu(bool scrollable, String incoming_menu[], int incoming_size,
        int& frame) {

    HardwareSerial SerialArray[4] = {Serial, Serial1, Serial2, Serial3};

    Serial.println(F("Showing MENU"));
    key = NULL;

    if (scrollable)
        Serial.println(F("it is scrollable"));

    Serial.print(F("incoming size: "));
    Serial.println(incoming_size);

    printFourFromReference(frame, incoming_menu); //prints out the first four menu options

    Serial.println(F("After displaying printfour"));

    while (scrollable) {
        Serial.print(F("Size of menu: "));
        Serial.println(incoming_size);
        Serial.println(F("ITS SCROLLABLE"));
        printFourFromReference(frame, incoming_menu);
        key = NULL;

        while (key == NULL || key == DOWN || key == UP || key == HELP) {//as long as the user is trying to scroll or there is no transmission keep listening


            getKey();
            if (key == DOWN && frame < (incoming_size - 4)) {
                frame++;
                break;
            } //user wants to scroll down

            if (key == UP && frame > 0) {
                frame--;
                break;
            }//user wants to scroll up

            if (key == CLEAR) {
                lcd_clear();
                lcd_print(L1Ta, F("Not Recorded"));
                delay(2000);
                key = 0;
                return;
            }

            if (button1IsPressed()) {//The user has decided to send the results to gradestar
                key = SENDING;
                return;
            }


            for (int i = 0; i < 4; i++) {

                incoming = (SerialArray[i]).read();
                if (incoming == PING) {
                    (SerialArray[i]).print(CONFIRMATION);
                    (SerialArray[i]).println(LoadNumber);
                } else if (incoming == SENDING) {
                    SerialTryingToSend[i] = true;
                    lcd_clear();
                    return;
                }
            }



        }// this while loop ends when the user enters a scroll and it changes the startingFrame accordingly
    }
}

char* numberPad(byte location, byte max_size, char* destination_array) {
    int i = 0;
    while (i < max_size) {
        getKey();
        while ((key < '0' || key > '9') && key != ENTER && key != CLEAR && key != '.') getKey(); //getting number

        if (key == ENTER) {//done
            destination_array[i] = '\0'; //terminating character
            break;
        } else if (key == CLEAR) {//delete one character

            if (i != 0) {
                destination_array[--i] = ' ';
                lcd_printChar(location + i, ' ');
            } else {
                ExitFlag = true;
                return "F"; //"F" is arbitrary, just needs to return some sort of char string
            }
        } else {//else the person is still entering in the number
            destination_array[i] = key;
            lcd_printChar(location + i, key);
            i++;
        }
    }

    return destination_array;
}//numberPad();
#endif	/* MENUFUNCTIONS_H */

