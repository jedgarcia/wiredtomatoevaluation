/* 
 * File:   MultipleDevices.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 10:36 AM
 */

#ifndef MULTIPLEDEVICES_H
#define	MULTIPLEDEVICES_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "debugFunctions.h"
#include "Button.h"

/******************************/

void waitForConfirmation() {
    incoming = masterAndGradestarSerial.read();
    while (incoming != CONFIRMATION) {
        //Serial.println(F("Waiting for confirmation"));
        incoming = masterAndGradestarSerial.read();
    }
}

void waitForConfirmationFromPing() {
    noMaster_t = masterPing.after(300, NoMaster_func);
    do {
        masterPing.update();
        Serial.println(F("Waiting for confirmation"));
        incoming = masterAndGradestarSerial.read();

        if (main_menu_flag) {
            Serial.println(F("The timer ran out"));
            masterPing.stop(noMaster_t);
            noMaster = true;
            main_menu_flag = false;
            return;
        }

    } while (incoming != CONFIRMATION);
    Serial.println(F("The master responded!"));

    //Serial.println(F("We got a confirmation from master"));
    masterPing.stop(noMaster_t);
    noMaster = false;
}

//SENDING-FLAGS==============================================================================================

void sendPhFlag() { //This sends over the AR20Flag to the master, so that if knows if it should
    //expect to get a ph measurement or not
    if (!phUnresponsive) {
        masterAndGradestarSerial.write(listenPh); // AR20 - flag
        delay(20);
    } else {
        masterAndGradestarSerial.write(!phUnresponsive);
        delay(20);
    }
}

/******************************/

void sendColorFlag() {
    if (!colorUnresponsive) {
        masterAndGradestarSerial.write(listenColor);
    } else {
        masterAndGradestarSerial.write(!colorUnresponsive);
    }
}

/******************************/

void sendRefracFlag() { //sends rfmflag to the master to let it know if it should expect an rfm190 measurement or not
    if (!refracUnresponsive) {
        masterAndGradestarSerial.write(listenRefrac); //refractometer - flag
    } else {
        masterAndGradestarSerial.write(!refracUnresponsive);
    }
}

/******************************/

void sendWeightFlag() {
    if (!weightUnresponsive) {
        masterAndGradestarSerial.write(listenWeight); //refractometer - flag
    } else {
        masterAndGradestarSerial.write(!weightUnresponsive);
    }
}
//END-OF-SENDING-FLAGS==========================================================================================

//SENDING-MEASUREMENTS==========================================================================================

void sendMeasurements() {

    if (listenColor && !colorUnresponsive) { //sending COLOR
        //1. - Color
        masterAndGradestarSerial.println(LedColor);
        waitForConfirmation();
    }

    if (listenPh && !phUnresponsive) {//sending pH
        //2. - pH
        masterAndGradestarSerial.println(phBuffer);
        waitForConfirmation();
    }

    if (listenRefrac && !refracUnresponsive) { //sending RFM190
        //3-RFM
        masterAndGradestarSerial.println(refracBuffer);
        waitForConfirmation();
    }

    if (listenWeight && !weightUnresponsive) { //sending VC505
        //4.VC505 - back up weight hopper
        Serial.println(F("About to send the weight flags"));
        //First have to send the flags of which of the weights to expect
        for (int i = 0; i < maximumHopperEntries; i++) {

            Serial.print(F("About to send the "));
            Serial.print(weightFields[i]);
            Serial.print(F(" Flag right now: "));
            Serial.println(weightFieldIsOn[i]);

            masterAndGradestarSerial.print(weightFieldIsOn[i]);
            waitForConfirmation();
        }
        Serial.print(F("Finished sending the weight flags"));

        for (int i = 0; i < maximumHopperEntries; i++) {

            if (weightFieldIsOn[i]) {//only send measurement if it is a weight that is enabled
                masterAndGradestarSerial.println(weightBuffers[i]);


                Serial.print(F("Waiting for confirmation: "));
                Serial.println(weightFields[i]);

                Serial.print(F("We sent: "));
                Serial.println(weightBuffers[i]);

                waitForConfirmation();
                Serial.print(F("Got confirmation: "));
                Serial.println(weightFields[i]);
            }
        }
    }
}//void sendMeasurements()

/******************************/

void sendPh() { //this sends over the phBuffer to the master
    if (listenPh && !phUnresponsive) {//The user is using the AR20 and the device is responsive
        masterAndGradestarSerial.print(phBuffer); //Serial.print() sends a '\0' terminated string
        masterAndGradestarSerial.print('\0'); //the receiver is looking for the '\0' to know when to stop receiving
        waitForConfirmation(); //Confirms the pHBuffer was received
    } else {// if the user is not using the device or its unresponsive
        if (listenPh)//if the user is using the device but it was unresponsive
            //pH sends too many characters which we do nothing with, so we flush the rest
            phSerial.flush();
    }
}

/******************************/

void sendColor() {
    //sending the LED to master
    if (listenColor && !colorUnresponsive) {
        masterAndGradestarSerial.println(LedColor);
        waitForConfirmation();
    }
}

/******************************/

void sendRefrac() { //this sends over the refracBuffer to the master
    if (listenRefrac && !refracUnresponsive) {
        //send refrac data
        masterAndGradestarSerial.print(refracBuffer);
        char NullTerminatorChar = '\0';
        masterAndGradestarSerial.write(NullTerminatorChar);
        waitForConfirmation(); //Confirms the pHBuffer was received
    }
}

/******************************/

void sendWeight() {
    //at this point all of the information should be in the weightBuffers
    //This does not take into account when one of the weights breaks, im not
    //sure how that is treated and if the output of the machine changes
    if (listenWeight && !weightUnresponsive) {
        for (int i = 0; i < 10; i++) { //theres only 8 different weights
            masterAndGradestarSerial.println(weightBuffers[i]); //These look like this: "MOT: 1.23"    
            delay(50);
        }
    }
}

//END-OF-SENDING-MEASUREMENTS========================================================================

//***************************************

void chooseSerialFromInput(HardwareSerial& ref) {

    //Don't get scared, but the software side to the arduino's serial
    //ports is extremely fussy, one mistake and your whole arduino
    //while just restart mid-execution, so leave this alone if you can

    //    HardwareSerial* dummy = deviceSerial;
    //    delete dummy;
    //    deviceSerial = new HardwareSerial(true);
    //    HardwareSerial& ref = *deviceSerial;


    switch (key) {

        case '1':
            ref = Serial1;
            break;
        case '2':
            ref = Serial2;
            break;
        case '3':
            ref = Serial3;
            break;
        case '4'://
            break;
        default:

            break;
    }//switch
}//chooseSerialFromInput

//***************************************

void setFlagsFromInput() {
    switch (key) {


        case 1: // '1' denotes having chosen no device 
            lcd_clear();

            //menuCounter is the counter in measuremultiple() for loop for
            //displaying the menu three times (up to 3 devices). If the user
            //does not want any more devices, this makes the for loop stop
            menuCounter = 3;



            delay(400);
            return;
            break;


        case 2://'2' denotes wanting to go back to the main menu
            main_menu_flag = true;
            return;
            break;


        case 3://'3' denotes wanting to listen to the Refrac
            listenRefrac = true;

            lcd_clear();
            lcd_print(L1Ta, F("RFM190 on port: ")); //ask for serial port

            key = 0;
            while (key < '1' || key > '3') getKey(); //get valid serial port 0-3

            lcd_printChar(L1Ta + 17, key); //print out the selected port on screen
            chooseSerialFromInput(refracSerial);
            delay(200); //just enough time for user to see his input
            listenRFM190 = true;
            break;

        case 4:
            listenPh = true;

            lcd_clear();
            lcd_print(L1Ta, F("AR20 on port: ")); //ask for serial port

            key = 0;
            while (key < '1' || key > '3') getKey(); //get valid serial port 0-3

            lcd_printChar(L1Ta + 17, key); //print out the selected port on screen
            chooseSerialFromInput(phSerial);
            delay(200); //just enough time for user to see his input
            listenAR20 = true;

            break;


        case 5: //placeholder color meeter
            listenColor = true;

            lcd_clear();
            lcd_print(L1Ta, F("ColM on port: ")); //ask for serial port

            key = 0;
            while (key < '1' || key > '3') getKey();

            lcd_printChar(L1Ta + 17, key); //print out the selected port on screen
            //chooseSerialFromInput(ColorimeterSerial);
            delay(500); //just enough time for user to see his input

            break;

        case 6: //VC505 hopper
            listenWeight = true;

            lcd_clear();
            lcd_print(L1Ta, F("VC-505 on port: ")); //ask for serial port

            key = 0;
            while (key < '1' || key > '3') getKey();
            lcd_printChar(L1Ta + 17, key); //print out the selected port on screen

            delay(500);
            chooseSerialFromInput(weightSerial);
            weightSerial.begin(9600, SERIAL_8N2);
            listenVC505 = true;
            break;

        case 7: //LED
            listenColor = true;

            lcd_clear();
            lcd_print(L1Ta, F("LED on port: ")); //asks user for which port

            key = 0;
            while (key < '1' || key > '3') getKey();
            lcd_printChar(L1Ta + 13, key); //prints out the selected port so user can see

            delay(500);
            chooseSerialFromInput(colorSerial); //uses key to correctly set up the colorimeter
            colorSerial.begin(19200); //the communication were intercepting is between the led - gradestar and gradestar is using 19200
            listenLED = true;
            break;

        case CLEAR:
            main_menu_flag = true;
            return;
            break;

        default://'2' denotes wanting to go back to the main menu
            lcd_println(L1Ta, F("Incorrect Input"));
            main_menu_flag = true;
            return;
            break;


    }
    lcd_clear();
    //delay(400);
} //setFlagsFromInput

//***************************************

void NoMaster_func() {
    noMaster = true;
    digitalWrite(buttonLED, LOW);
    main_menu_flag = true;

}

//------------------------------------------------------------------------------

void pingMaster() {
    masterAndGradestarSerial.print(PING);
    waitForConfirmationFromPing();
}

void MeasuringMultiple() {

    FlushAllSerial();

    //Pinging the master
    pingMaster();
    if (!noMaster)
        digitalWrite(buttonLED, HIGH);
    else
        digitalWrite(buttonLED, LOW);

    measurements_menu[LOADNUMBERLINE] = "Load #: ";
    measurements_menu[LOADNUMBERLINE] += LoadNumber; //puts the load number into the menu as a string


    bool firstMeasure = true;
    LEDHasBeenMeasured = false;
    VC505HasBeenMeasured = false;
    main_menu_flag = false;
    measurements_menu_size = 1; //starts at 1 b/c 0 is the load number line    
    menu_frame = 0;

    if (config_arduino_function != 2) {//if the user is manually entering the mode

        for (menuCounter = 0; menuCounter < 3; menuCounter++) { //can only measure 3 devices
            mainMenu(device_menu, DMENUSIZE); //display menu
            setFlagsFromInput(); // uses 'key' as its input - adds to measurement_menu_size
            if (main_menu_flag) {//checks for main menu escape
                main_menu_flag = false;
                clearListenFlags();
                AlreadySentMyMeasurements = false;
                noMaster = false;
                return;
            }//return to main menu
        }

        if (!AtLeastOneListenFlagIsEnabled()) {
            lcd_print(L1Ta, F("No devices selected..."));
            delay(1000);
            lcd_clear();
            return;
        }

        //It will save the configuration if a device is actually chosen
        saveCurrentConfiguration(MEASURING_MODE);
    } else {//we are going to measure from what we have saved in memory
        config_arduino_function = 0; //clearing so we dont loop
        if (listenColor && listenLED) {
            colorSerial.begin(19200);
            colorSerial.flushBuffer();
        }
        if (listenWeight && listenVC505) {
            weightSerial.begin(9600, SERIAL_8N2);
            delay(500);
            weightSerial.flushBuffer();
        }
    }



    lcd_print(L1Ta, measurements_menu[LOADNUMBERLINE]); //prints out the load number onto the screen
    //HERE IS WHERE THE LOOP STARTS--------------------------------------------------------------------------------------------
    //------------------------------HERE IS WHERE THE LOOP STARTS-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------HERE IS WHERE THE LOOP STARTS------------------------------------------------------------
    //-----------------------------------------------------------------------------------------HERE IS WHERE THE LOOP STARTS------------------------------
    //-----------------------------------------------------------------------------------------------------------------------HERE IS WHERE THE LOOP STARTS




    while (1) {//listening for user input (button press or HELP for test batch)
        getKey();
        if (key == CLEAR || main_menu_flag) { //Reset - '*'
            digitalWrite(buttonLED, LOW);
            main_menu_flag = false;
            measureFlag = false;
            clearListenFlags();
            AlreadySentMyMeasurements = false;
            noMaster = false;
            saveCurrentConfiguration(MAIN_MENU_MODE);
            return;
        }

        if (key == TESTING) {//Test Batch - '#'
            testing_dont_send = true; // user wants to do a test batch
        }

        //Since the LED needs the most physical interaction to read
        //we must always be scanning for any sign of the LED being measured
        //and assumer that is a trigger to measure the rest
        if (listenColor && listenLED) {
            if (colorSerial.available()) {
                Serial.println(F("The serial port was available"));
                if ((incoming = colorSerial.read()) == 'L') {
                    delay(10);
                    if ((incoming = colorSerial.read()) == 'O') {
                        delay(10);
                        if ((incoming = colorSerial.read()) == 'A') {
                            delay(10);
                            if ((incoming = colorSerial.read()) == 'D') {
                                measurements_menu[LOADNUMBERLINE] = "Load #: ";
                                measurements_menu[LOADNUMBERLINE] += LoadNumber; //puts the load number into the menu as a string


                                Serial.println(F("Doing led stuff in mult dev"));

                                if (firstMeasure)
                                    COLORLINE = measurements_menu_size++;

                                int color = readFromLedColorMeter();

                                Serial.print(F("color measurement is: "));
                                Serial.println(color);

                                Serial.print(F("color line in mult: "));
                                Serial.println(COLORLINE);

                                if (COLORLINE > 4)
                                    menu_frame = COLORLINE - 4;

                                measurements_menu[COLORLINE] = "LED: ";
                                measurements_menu[COLORLINE] += color;

                                displayMenu(false, measurements_menu, COLORLINE, menu_frame);
                                measureFlag = true;
                                LEDHasBeenMeasured = true;
                                Serial.println(F("After out of the LED"));
                            }
                        }
                    }
                }

            }
        }
        //measuring the weight without pressing a button
        if (listenWeight && listenVC505 && weightSerial.available()) {
            measurements_menu[LOADNUMBERLINE] = "Load #: ";
            measurements_menu[LOADNUMBERLINE] += LoadNumber; //puts the load number into the menu as a string

            Serial.println(F("Doing weight things"));

            if (firstMeasure) {
                WEIGHTLINE = measurements_menu_size;
                measurements_menu_size += countWeightsExpected(); //hopper needs at most 8 lines
            }

            //readFromVC505();
            readFromHopper();
            if (main_menu_flag) {
                main_menu_flag = false;
                measureFlag = false;
                return;
            } else {//we actually want to record
                //lcd_clear();
                measureFlag = true;
                VC505HasBeenMeasured = true;
                //displayMenu(false, measurements_menu, WEIGHTLINE, menu_frame);
            }
        }


        if (button1IsPressed() || testing_dont_send || measureFlag) {//reads from devices when button is pressed or user wants a test batch
            if (!LEDHasBeenMeasured && listenColor && listenLED) {
                lcd_clear();
                lcd_print(L1Ta, F("Please use LED"));
                lcd_print(L2Ta, F("tray to start"));
                lcd_print(L3Ta, F("measuring"));
                delay(1000);
                lcd_clear();
                lcd_print(L1Ta, measurements_menu[LOADNUMBERLINE]); //prints out the load number onto the screen

                testing_dont_send = false; // resets the flag
                phUnresponsive = false;
                refracUnresponsive = false;
                weightUnresponsive = false;
                colorUnresponsive = false;
                LEDHasBeenMeasured = false;
                measureFlag = false;
                VC505HasBeenMeasured = false;
                continue;
            }

            if (!VC505HasBeenMeasured && listenWeight && listenVC505) {
                lcd_clear();
                lcd_print(L1Ta, F("Please use VC505"));
                lcd_print(L2Ta, F("control panel to"));
                lcd_print(L3Ta, F("measure"));
                delay(1000);
                lcd_clear();
                lcd_print(L1Ta, measurements_menu[LOADNUMBERLINE]); //prints out the load number onto the screen

                testing_dont_send = false; // resets the flag
                phUnresponsive = false;
                refracUnresponsive = false;
                weightUnresponsive = false;
                colorUnresponsive = false;
                LEDHasBeenMeasured = false;
                measureFlag = false;
                VC505HasBeenMeasured = false;
                continue;
            }

            //The measure flag is set from another function to signal to trigger entrance into
            //this if statement so that it will trigger a reading of the devices
            measureFlag = false;

            //Listen Refrac-----------------------------------------------------
            if (listenRefrac) {//read from the refractometer     
                measurements_menu[LOADNUMBERLINE] = "Load #: ";
                measurements_menu[LOADNUMBERLINE] += LoadNumber; //puts the load number into the menu as a string

                refracSerial.flushBuffer();

                if (firstMeasure)
                    REFRACLINE = measurements_menu_size++;
                if (REFRACLINE > 4)
                    menu_frame = REFRACLINE - 3;

                measurements_menu[REFRACLINE] = "Reading refrac...";
                displayMenu(false, measurements_menu, REFRACLINE + 1, menu_frame);

                MeasuringRFM190();
                if (refracUnresponsive) {
                    refracSerial.flushBuffer();
                    measurements_menu[REFRACLINE] = "Refrac Unresponsive";
                } else {//if the refrac is responsive
                    if (refracInBounds()) {
                        measurements_menu[REFRACLINE] = "Refrac:";
                        measurements_menu[REFRACLINE] += refracBuffer;
                    } else {//refrac out of bounds
                        measurements_menu[REFRACLINE] = "Refrac out of bounds";
                        refracUnresponsive = true;
                    }
                }
                displayMenu(false, measurements_menu, REFRACLINE + 1, menu_frame);
            }//if (listenRefrac)

            //Listen pH---------------------------------------------------------
            if (listenPh) {//read from the pH meter
                measurements_menu[LOADNUMBERLINE] = "Load #: ";
                measurements_menu[LOADNUMBERLINE] += LoadNumber; //puts the load number into the menu as a string

                phSerial.flushBuffer();

                if (firstMeasure)
                    PHLINE = measurements_menu_size++;

                if (PHLINE > 4)
                    menu_frame = PHLINE - 3;

                measurements_menu[PHLINE] = "";
                measurements_menu[PHLINE] += "Reading pH...";
                displayMenu(false, measurements_menu, PHLINE + 1, menu_frame);

                MeasuringAR20();
                if (!phUnresponsive) {
                    if (phInBounds()) {//ph is in bounds
                        measurements_menu[PHLINE] = "pH: ";
                        measurements_menu[PHLINE] += phBuffer;
                    } else { //ph is out of bounds
                        measurements_menu[PHLINE] = "pH out of bounds";
                        phUnresponsive = true;
                    }
                } else {//pH is unresponsive
                    measurements_menu[PHLINE] = "ph Unresponsive...";
                    phSerial.flushBuffer();
                }
                displayMenu(false, measurements_menu, PHLINE + 1, menu_frame);
            }

            //listen minulta---------------------------------------------
            if (listenColor && !listenLED) {//read from Colorimeter - minulta, still in development
                measurements_menu[LOADNUMBERLINE] = "Load #: ";
                measurements_menu[LOADNUMBERLINE] += LoadNumber; //puts the load number into the menu as a string

                if (firstMeasure)
                    COLORLINE = measurements_menu_size++;

                if (COLORLINE > 4)
                    menu_frame = COLORLINE - 3;
                displayMenu(false, measurements_menu, COLORLINE + 1, menu_frame);
            }
            firstMeasure = false;


            //Sending to Master ------------------------------------------------
            if (!testing_dont_send && !phUnresponsive && !refracUnresponsive && !colorUnresponsive && !weightUnresponsive &&
                    !colorUnresponsive) {//If user wants the data to be recorded

                Serial.println(F("Going to send to master"));

                //The timers must be stopped first if they are to be reused
                pingMaster();
                if (!noMaster) {
                    digitalWrite(buttonLED, HIGH);
                    sendMultipleToMaster(); //Send measurements to master Arduino - Serial 1
                } else {//The master is not connected
                    digitalWrite(buttonLED, LOW);
                    Serial.println(F("Master was not connected"));
                }

                colorSerial.flushBuffer();
                phUnresponsive = false;
                refracUnresponsive = false;
                weightUnresponsive = false;
                colorUnresponsive = false;
                LEDHasBeenMeasured = false;
                VC505HasBeenMeasured = false;

                menu_frame = 0;
                Serial.println(F("Going into the measurements menu"));
                displayMenu(true, measurements_menu, measurements_menu_size, menu_frame);

            } else {//test batch
                testing_dont_send = false; // resets the flag

                phUnresponsive = false;
                refracUnresponsive = false;
                weightUnresponsive = false;
                colorUnresponsive = false;
                LEDHasBeenMeasured = false;
                VC505HasBeenMeasured = false;

                displayMenu(true, measurements_menu, measurements_menu_size, menu_frame);
            }//else

        }//if measuring needs to be done

    }//while listening for user input

}//MeasuringMultiple()

//***************************************

void getNextIncoming(HardwareSerial& SerialChosen) {//gets the next valid incoming from Serial
    incoming = SerialChosen.read();
    while (incoming == NODATA) incoming = SerialChosen.read();
}

void getNextIncoming(SoftwareSerial& SerialChosen) {//gets the next valid incoming from Serial
    incoming = SerialChosen.read();
    while (incoming == NODATA) incoming = SerialChosen.read();
}

//***************************************

//Assumes that the master Arduino is in Serial, we use the following:
//SENDING, CONFIRMATION, listenflags
//to tell other arduino how many measurements and which measurements
//The information is then sent a byte at a time from our buffer over Serial

void sendMultipleToMaster() {

    masterAndGradestarSerial.print(SENDING); //tells other arduino that we are sending a message

    //other arduino sends back confirmation that it received our request to send
    waitForConfirmation();

    //Master expects the Flags to be send in alphabetical order
    sendColorFlag();
    sendPhFlag();
    sendRefracFlag();
    sendWeightFlag();

    Serial.println(F("SENT FLAGS"));

    //Then we wait for a confirmation that they got all the flags
    waitForConfirmation();

    Serial.println(F("Got the flag confirmation"));

    //Now we send our measurements in alphabetical order
    sendMeasurements();
    Serial.println(F("MEASUREMENTS SENT"));


    //Load Number exchange procedure
    masterAndGradestarSerial.println(LoadNumber);
    waitForConfirmation();

    incrementLoadNumber();
    measurements_menu[0] = "Load#: ";
    measurements_menu[0] += LoadNumber - 1;
    measurements_menu[0] += " done";


    //now that we have the new load number, we write it to the sd card
    saveCurrentConfiguration(MEASURING_MODE);


    //Responsiveness is evaluated when the user tries to get measurements
    //so they must be reset now
    phUnresponsive = false;
    colorUnresponsive = false;
    refracUnresponsive = false;
    weightUnresponsive = false;


}//sendMultipleToMaster


//***************************************

//the commented out code is correct, it needs to be uncommented when actually given
//to ptab, its just commented out right now because it is easier to debug when you 
//can use any solution instead of only one in a certain range

bool refracInBounds() {
    char refracCharArray[15];
    refracBuffer.toCharArray(refracCharArray, 15);

    float refrac = atof(refracCharArray);

    return true; //debug

    if (1.99 < refrac && refrac < 20) {
        return true;
    } else
        return false;
}

bool phInBounds() {
    char phBufferCharArray[15];
    phBuffer.toCharArray(phBufferCharArray, 15);

    Serial.print(F("This is the ph in float format: "));
    Serial.println(phBufferCharArray);

    Serial.print(F("This is the buffer: "));
    Serial.println(phBuffer);

    float ph = atof(phBufferCharArray);
    if (3 < ph && ph < 5)
        return true;
    else
        return false;
}



#endif	/* MULTIPLEDEVICES_H */

