/* 
 * File:   Hopper.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on July 18, 2013, 10:31 AM
 */

#ifndef HOPPER_H
#define	HOPPER_H
#include "ForwardDeclarations.h"
#include "GlobalVariables.h"
#include "HopperScanner.h"

char displayWeightFieldInfo(const __FlashStringHelper * Title, int weightFieldIndex, bool manipulateString) {
    lcd_clear();
    lcd_print(L1Ta, F("   Edit "));
    lcd_print(L1Ta + 8, Title);

    if (manipulateString) {
        lcd_print(L2Ta, F("-"));
        lcd_print(L2Ta + 1, weightFields[weightFieldIndex]);

        lcd_print(L4Ta, F("Press ENTER to edit"));
    }

    if (weightFieldIsOn[weightFieldIndex]) {
        lcd_print(L3Ta, F("ON-Button to toggle"));
    } else {
        lcd_print(L3Ta, F("OFF-Button to toggle"));
    }

    do {
        getKey();
        if (button1IsPressed()) {
            weightFieldIsOn[weightFieldIndex] = !weightFieldIsOn[weightFieldIndex];
            if (weightFieldIsOn[weightFieldIndex]) {
                lcd_print(L3Ta, F("                    "));
                lcd_print(L3Ta, F("ON-Button to toggle"));
                delay(250);
            } else {
                lcd_print(L3Ta, F("                    "));
                lcd_print(L3Ta, F("OFF-Button to toggle"));
                delay(250);
            }
        }
    } while (key != ENTER && key != CLEAR);

    if (manipulateString) {
        if (key == ENTER) {//the user wants to edit
            lcd_clear();
            lcd_print(L1Ta, "  Edit Using Arrows");
            lcd_print(L4Ta, F("2ND-Back  ENTER-Next"));
            weightFields[weightFieldIndex] = customizeStringByScrolling(L2Ta, weightFields[weightFieldIndex]); //sets the Spare1Custom to what the user inputs
        }
    }
    return '0';
}

void HopperSettings() {
    byte hopperSettingsSize = 11;
    const __FlashStringHelper * hopperSettings[11];

    hopperSettings[0] = F("   Choose to edit");


    while (true) {
        if (weightFieldIsOn[HOPPERWT]) {
            hopperSettings[1] = F("0.*Hopper Wt");
        } else {
            hopperSettings[1] = F("0. Hopper Wt");
        }


        if (weightFieldIsOn[WORM]) {
            hopperSettings[2] = F("1.*WORM");
        } else {
            hopperSettings[2] = F("1. WORM");
        }


        if (weightFieldIsOn[MOLD]) {
            hopperSettings[3] = F("2.*MOLD");
        } else {
            hopperSettings[3] = F("2. MOLD");
        }


        if (weightFieldIsOn[GREEN]) {
            hopperSettings[4] = F("3.*GREEN");
        } else {
            hopperSettings[4] = F("3. GREEN");
        }


        if (weightFieldIsOn[MOT]) {
            hopperSettings[5] = F("4.*MOT");
        } else {
            hopperSettings[5] = F("4. MOT");
        }


        if (weightFieldIsOn[LU]) {
            hopperSettings[6] = F("5.*LU");
        } else {
            hopperSettings[6] = F("5. LU");
        }


        if (weightFieldIsOn[SPARE_1]) {
            hopperSettings[7] = F("6.*Other Defect 1");
        } else {
            hopperSettings[7] = F("6. Other Defect 1");
        }



        if (weightFieldIsOn[SPARE_2]) {
            hopperSettings[8] = F("7.*Other Defect 2");
        } else {
            hopperSettings[8] = F("7. Other Defect 2");
        }

        if (weightFieldIsOn[DATE]) {
            hopperSettings[9] = F("8.*Date");
        } else {
            hopperSettings[9] = F("8. Date");
        }

        if (weightFieldIsOn[TIME]) {
            hopperSettings[10] = F("9.*Time");
        } else {
            hopperSettings[10] = F("9. Time");
        }

        //display the menu here
        Menu(hopperSettings, hopperSettingsSize);

        switch (key) {
            case '0'://Hopper Wt 
                displayWeightFieldInfo(F("Hopper Wt"), HOPPERWT, true);
                break;
            case '1'://Worm
                displayWeightFieldInfo(F("WORM"), WORM, true);
                break;
            case '2'://MOLD
                displayWeightFieldInfo(F("MOLD"), MOLD, true);
                break;
            case '3'://Green
                displayWeightFieldInfo(F("GREEN"), GREEN, true);
                break;
            case '4'://Mot
                displayWeightFieldInfo(F("MOT"), MOT, true);
                break;
            case '5'://LU
                displayWeightFieldInfo(F("LU"), LU, true);
                break;
            case '6'://Other Defect 1
                displayWeightFieldInfo(F("Other Defect 1"), SPARE_1, true);
                break;
            case '7'://Other Defect 2
                displayWeightFieldInfo(F("Other Defect 2"), SPARE_2, true);
                break;
            case '8'://Other Defect 2
                displayWeightFieldInfo(F("Date"), DATE, false);
                break;
            case '9'://Other Defect 2
                displayWeightFieldInfo(F("Time"), TIME, false);
                break;

            case ENTER:
            case CLEAR:
                lcd_clear();
                lcd_print(lines[0], F("Saving changes..."));
                sendFinishedQueuedLoads(false);
                saveCurrentConfiguration(MAIN_MENU_MODE);
                return;
                break;
        }//switch(key))
    }//while(true)
}//hopperSettings

String customizeStringByScrolling(byte location, String customString) {

    String backupString = customString;

    //This is seeing where we should put the line that contains the asterisks
    //which signal which character you are editing, if you chose to do the customization
    //on the last line, then the asterisks will be places in the third.
    byte asteriskLine;
    if (location >= L1Ta && location <= 147) {
        asteriskLine = L2Ta;
    } else if (location >= L2Ta && location <= 211) {
        asteriskLine = L3Ta;
    } else if (location >= L3Ta && location <= 167) {
        asteriskLine = L4Ta;
    } else if (location >= L4Ta && location <= 231) {
        asteriskLine = L3Ta;
    }
    //Serial.println(asteriskLine);

    //Turn the string into all upper case
    customString.toUpperCase();

    //Fill in the empty white space up to 20 characters
    while (customString.length() < 20) {
        customString += " ";
    }

    lcd_print(location, customString);

    for (int i = 0; i < 20; i++) {//entering 20 characters

        //clears the line and then puts the asterisk in  its rightfull place
        lcd_print(asteriskLine, F("                  "));
        lcd_print(asteriskLine + i, F("*"));

        do {//Enter signals to go to next letter

            //listen if the user wants to scroll or presses enter or help or clear
            getKey();

            if (key == UP) {//want to decrease the ascii value
                Serial.println(F("Pressed up"));
                Serial.println(i);



                if (customString.charAt(i) > '0' && customString.charAt(i) <= '9') {//you can increment and decrease and be within numbers
                    customString[i] = customString[i] - 1;
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) > 'A' && customString.charAt(i) <= 'Z') {//you can increment and decrease and be within alphanumeric
                    customString[i] = customString[i] - 1;
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == 'A') {//going up when on A you roll into 9
                    customString[i] = '9';
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == '0') {
                    customString[i] = ' ';
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == ' ') {
                    customString[i] = 'Z';
                    lcd_print(location + i, customString.charAt(i));
                }
            }//They want to go up



            if (key == DOWN) {//want to increase the ascii value
                Serial.println(F("PRESSED DOWN"));
                Serial.println(i);
                if (customString.charAt(i) >= '0' && customString.charAt(i) < '9') {//you can increment and decrease and be within numbers
                    customString[i] = customString.charAt(i) + 1;
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) >= 'A' && customString.charAt(i) < 'Z') {//you can increment and decrease and be within alphanumeric
                    customString[i] = customString.charAt(i) + 1;
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == 'Z') {//going up when on A you roll into 9
                    customString[i] = ' ';
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == '9') {
                    customString[i] = 'A';
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == ' ') {
                    customString[i] = '1';
                    lcd_print(location + i, customString.charAt(i));
                }
            }//want to increase the ascii value


            if (key == HELP) { //Help signals making the current character a space
                customString[i] = ' ';
                lcd_print(location + i, " ");
            }//key == HELP


            if (key == SECOND) {//SECOND - want to go to a previous character
                if (i > 0)
                    i--;
                lcd_print(asteriskLine, F("                    "));
                lcd_print(asteriskLine + i, F("*"));
            }//key = SECOND


            if (button1IsPressed()) {//If they press the button, they are done manipulating
                //The function will exit after editing 20 characters, or when they press the button
                return customString;
            }

            if (key == CLEAR) {
                return backupString;
            }

        } while (key != ENTER);

    }
    customString.trim();
    return customString;
}

int askUserForDefectNumber() {
    //This is where you gotta ask what other defect slot
    lcd_clear();
    lcd_print(L1Ta, F("Other defects (1-9)"));
    lcd_print(L3Ta, F("Choose defect# :"));

    do {//wait until the user enters in a valid number
        getKey();
    } while (key <= '0' || key >= '9');

    lcd_print(L3Ta + 16, key);
    lcd_print(L4Ta, F("defect # selected!"));
    delay(1000);
    return (int) key;
}

void customHopperName() {
    lcd_clear();
    lcd_print(L1Ta, F(" Custom Hopper Names"));

    lcd_print(L2Ta, F("1. "));
    lcd_print(L2Ta + 3, spareCustomName[0]);

    lcd_print(L3Ta, F("2. "));
    lcd_print(L3Ta + 3, spareCustomName[1]);

    do {
        getKey();
    } while (key != '1' && key != '2' && key != CLEAR);


    if (key == '1') {
        lcd_clear();
        lcd_print(L1Ta, "Change with Arrows");
        spareCustomName[0] = customizeStringByScrolling(L2Ta, spareCustomName[0]); //sets the Spare1Custom to what the user inputs

        //Ask the user what defect field it should be in
        spareCustomDefectNumber[0] = askUserForDefectNumber();
        delay(1000);
        return;

    } else if (key == '2') {
        lcd_clear();
        lcd_print(L1Ta, "Change with Arrows");
        spareCustomName[1] = customizeStringByScrolling(L2Ta, spareCustomName[1]); //sets the Spare1Custom to what the user inputs

        //Ask the user what defect field it should be in
        spareCustomDefectNumber[1] = askUserForDefectNumber();
        delay(1000);
        return;
    } else if (key == CLEAR) {
        return;
    }
}

bool weightSyntaxRecognition(String input) {//This will put them into the right place
    //We are assuming that the string incoming has a category attached to to it
    //The formatincoming is like this: "Category: (data), e.g. : WORM: 1.24

    input.toUpperCase();

    if (input.startsWith(weightFields[DATE]) && weightFieldIsOn[DATE]) {//date
        weightBuffers[0] = input;
        return true;
    } else if (input.startsWith(weightFields[TIME]) && weightFieldIsOn[TIME]) {//time
        weightBuffers[1] = input;
        return true;
    } else if (input.startsWith(weightFields[HOPPERWT]) && weightFieldIsOn[HOPPERWT]) {//hopper wt
        weightBuffers[2] = input;
        return true;
    } else if (input.startsWith(weightFields[WORM]) && weightFieldIsOn[WORM]) {//WORM
        weightBuffers[3] = input;
        return true;
    } else if (input.startsWith(weightFields[MOLD]) && weightFieldIsOn[MOLD]) {//MOLD
        weightBuffers[4] = input;
        return true;
    } else if (input.startsWith(weightFields[GREEN]) && weightFieldIsOn[GREEN]) {//GREEN
        weightBuffers[5] = input;
        return true;
    } else if (input.startsWith(weightFields[MOT]) && weightFieldIsOn[MOT]) {//MOT
        weightBuffers[6] = input;
        return true;
    } else if (input.startsWith(weightFields[LU]) && weightFieldIsOn[LU]) {//LU
        weightBuffers[7] = input;
        return true;
    } else if (input.startsWith(weightFields[SPARE_1]) && weightFieldIsOn[SPARE_1]) {//SPARE 1
        weightBuffers[8] = input;
        return true;
    } else if (input.startsWith(weightFields[SPARE_2]) && weightFieldIsOn[SPARE_2]) {//SPARE 2
        weightBuffers[9] = input;
        return true;
    }
    return false;
}

String getCategoryFromString(String input) {
    String buffer = "";
    int i = 0;
    while (input.charAt(i) != ':') {
        buffer += input.charAt(i++);
    }
    return buffer;
}

bool hopperIsDonePrinting(String array) {

    int size_of_array = array.length();
    int starting_pos = size_of_array - 9; //this is enough room to look for Spec Serv

    if (starting_pos < 9)
        return false;

    for (int i = starting_pos; i < size_of_array; i++) {
        if (array[i] == '_') {
            return true;
        }
    }

    return false;


}

int firstIndexOfLine(String array, int pos) {
    int i = pos - 3;
    while (1) {//looks for the start of the line
        if (array[i] == ' ') {
            ++i;
            break;
        } else
            --i;
    }

    return i;
}

int lastIndexOfFloat(String array, int starting_pos) {
    int i = starting_pos - 3;
    while (1) {
        if (array[i] == ' ') {
            //--i;
            break;
        } else
            i++;
    }//while(1)
    return i;
}

void getMotType(String& category, HardwareSerial& serialChosen, String & hopperBuffer) {
    //this function is called after receiving "MOT" for a category
    //we will serial.read() until we get mot's secondary description
    //There is a chance that mot will not have a secondary description

    while (1) {
        if (serialChosen.available()) {
            incoming = serialChosen.read();
            hopperBuffer += (char) incoming;

            if (incoming >= '0' && incoming <= '9')//mot doesn't have a secondary description
                return;
            if (incoming >= 'A' && incoming <= 'z') {//mot has a secondary description
                category += " ";
                category += (char) incoming;

                while (1) {
                    if (serialChosen.available()) {
                        incoming = serialChosen.read();

                        if ((incoming >= '0' && incoming <= '9') || (incoming >= 'A' && incoming <= 'z') || incoming == ' ')
                            category += (char) incoming;

                        if (incoming == 13 || incoming == 10) {
                            category.toUpperCase();
                            return;
                        }//if it is time to stop reading
                    }//if serial is available
                }//while(1)
            }//if we are reading the secondary label
        }//if serial chosen is available
    }//while(1)
}//getMotType()

void getCategory(HardwareSerial& serialChosen, String& category, String & hopperBuffer) {

    //this function is called right after gettng the 'b' in 'lb'
    //so we advance the position by one to start the category reading at
    //a valid char and not a space

    int i = 0;

    while (1) {
        i++;
        while (serialChosen.available()) {
            incoming = serialChosen.read();
            hopperBuffer += (char) incoming;


            if ((incoming >= '0' && incoming <= '9') || (incoming >= 'A' && incoming <= 'z') || incoming == ' ') {
                category += (char) incoming;
                continue;
            }

            if (incoming == 13 || incoming == 10) {//if we reached the end of the line
                category.toUpperCase();

                //all categories at this point have a space in front of them " category"
                category = category.substring(1, category.length());

                if (category.equals("MOT")) {
                    getMotType(category, serialChosen, hopperBuffer); //looks for mot's both or ext or dirt
                }
                return;
            }

            //delay(10);
        }
    }
}

void getNumberFromHopperBuffer(String& buffer, String& destination, int pos_of_b) {
    //given the position of 'b' in "lb" which denotes a measurement in the hopper
    //we then return the floating number that goes along to that number;

    int firstIndex = firstIndexOfLine(buffer, pos_of_b); //index of first char of the float
    int lastIndex = lastIndexOfFloat(buffer, pos_of_b);

    destination = buffer.substring(firstIndex, lastIndex);
    return;
}

bool isDate(String input) {//it is a date if it has a forward slash '/'
    for (int i = 0; i < input.length(); i++) {
        if (input[i] == '/')
            return true;
    }
    return false;
}

bool isTime(String input) {//it is time if it has a colon ':'
    for (int i = 0; i < input.length(); i++) {
        if (input[i] == ':')
            return true;
    }
    return false;
}

bool isLoadNumber(String input) {//it is a load number if in the
    input.toUpperCase();
    String LoadSyntax = "LOAD";
    for (int i = 0; i < input.length(); i++) {
        if (input.substring(i).startsWith(LoadSyntax))
            return true;
    }
    return false;
}

void readFromHopper() {
    HopperScanner HopperScan(&weightSerial, weightFields, weightFieldIsOn);
    //Serial.println("After the initialization");
    delay(50);
    HopperScan.ReadHopperWeights();
    //Serial.println("After reading the weights");

    int counter = 0;
    for (int i = 0; i < 10; i++) {
        if (weightFieldIsOn[i]) {
            weightBuffers[i] = HopperScan.getField(i);
            measurements_menu[WEIGHTLINE + counter++] = HopperScan.getField(i);
            Serial.println(HopperScan.getField(i));
        }
    }

    weightSerial.flushBuffer();
    
    Serial.print(F("freeMemory()="));
    Serial.println(freeMemory());
}

void readFromVC505() {
    bool gotLoadNumber = false; //used to skip load numbers but still catch the date and time
    bool gotDate = false;
    bool gotTime = false;
    bool gotSpace = false;

    key = 0;
    char response[2];
    String hopperBuffer = "";
    weightBuffers_size = 0;
    bool alreadyPrinted = false;

    byte bucketDisp = 0;
    int numberOfWeightsToExpect = countWeightsExpected();

    while (1) {

        getKey();
        if (key == SECOND) {//dunno why this is here
            lcd_clear();
            lcd_print(L1Ta, F("Weighing again.."));
            delay(500);
            readFromVC505();
            return;
        }
        if (key == CLEAR) {
            main_menu_flag = true;
            measureFlag = false;
            return;
        }

        if (hopperIsDonePrinting(hopperBuffer)) {

            //display the last part of our received measurements
            menu_frame--;
            displayMenu(false, measurements_menu, WEIGHTLINE + bucketDisp, menu_frame);

            //clear buffer
            weightSerial.flushBuffer();

            //bucketdisp - 2 = solely the amount of weights (2 entries received are the date and time)
            if (bucketDisp < numberOfWeightsToExpect) {
                lcd_clear();
                lcd_print(L1Ta, F("Weight buckets"));
                lcd_print(L2Ta, F("did not record well"));
                lcd_print(L3Ta, F("Press Enter to try"));
                lcd_print(L4Ta, F("again..."));
                while (getKey() != ENTER);
                lcd_clear();
                readFromVC505();
            }

            return;
        }//if were done reading from the hopper


        if (!alreadyPrinted) {

            alreadyPrinted = true;

            if (WEIGHTLINE + bucketDisp > 3)
                menu_frame = WEIGHTLINE + bucketDisp - 3;
            else
                menu_frame = 0;

            if (bucketDisp < countWeightsExpected()) {
                measurements_menu[WEIGHTLINE + bucketDisp] = "Waiting on Hopper...";
                displayMenu(false, measurements_menu, WEIGHTLINE + bucketDisp + 1, menu_frame);
            }
        }



        if (weightSerial.available()) {
            incoming = weightSerial.read();
            hopperBuffer += (char) incoming;
            Serial.print((char) incoming);

            //This if statement is true when we are getting a non-date non-time entry
            if (hopperBuffer[hopperBuffer.length() - 1 ] == 'b' && hopperBuffer[hopperBuffer.length() - 2 ] == 'l') {

                String Measurement = "";
                getNumberFromHopperBuffer(hopperBuffer, Measurement, hopperBuffer.length() - 1);


                String Category = "";
                getCategory(weightSerial, Category, hopperBuffer);
                //Serial.println();
                //Serial.print("the category is: ");
                //Serial.println(Category);


                if (WEIGHTLINE + bucketDisp > 3)
                    menu_frame = WEIGHTLINE + bucketDisp - 2;
                else
                    menu_frame = 0;


                String temporaryBuffer = Category;
                temporaryBuffer += ": ";
                temporaryBuffer += Measurement;

                //at this point we have an entry in the format Category: (DATA)
                //weightSyntaxRecognition puts the string into the right weightBuffer[]
                if (weightSyntaxRecognition(temporaryBuffer)) {
                    measurements_menu[WEIGHTLINE + bucketDisp] = temporaryBuffer;
                    ++bucketDisp;
                    alreadyPrinted = false;
                } else {
                    alreadyPrinted = true;
                }


            } else if ((hopperBuffer[hopperBuffer.length() - 1 ] == 10) && (!gotLoadNumber || !gotTime || !gotDate || !gotSpace)) {
                //Serial.println(F("got into the looking for date loop"));

                //the only thing dependant on 
                if (!gotLoadNumber) {
                    if (isLoadNumber(hopperBuffer.substring(hopperBuffer.length() - 10, hopperBuffer.length() - 2))) {
                        Serial.println("We got the load number");
                        gotLoadNumber = true;
                        continue;
                    }
                }

                if (!gotSpace) {
                    gotSpace = true;
                    continue;
                }


                if (gotTime && gotDate) {
                    continue;
                }

                //Serial.println(F("Actually going to look for time and stuff"));

                String tempBuffer = "";

                if (isDate(hopperBuffer.substring(hopperBuffer.length() - 10, hopperBuffer.length() - 2))) {
                    tempBuffer = "DATE: ";
                    String tempDate = hopperBuffer.substring(hopperBuffer.length() - 10, hopperBuffer.length() - 2);
                    tempDate.trim();
                    tempBuffer += tempDate;
                    gotDate = true;
                } else if (isTime(hopperBuffer.substring(hopperBuffer.length() - 10, hopperBuffer.length() - 2))) {
                    tempBuffer = "TIME: ";
                    String tempTime = hopperBuffer.substring(hopperBuffer.length() - 10, hopperBuffer.length() - 2);
                    tempTime.trim();
                    tempBuffer += tempTime;
                    gotTime = true;
                } else {
                    continue;
                }

                if (WEIGHTLINE + bucketDisp > 3)
                    menu_frame = WEIGHTLINE + bucketDisp - 2;
                else
                    menu_frame = 0;

                if (weightSyntaxRecognition(tempBuffer)) {
                    measurements_menu[WEIGHTLINE + bucketDisp] = tempBuffer;
                    ++bucketDisp;
                    alreadyPrinted = false;
                } else {

                    alreadyPrinted = true;
                }
            }
        }//if weightSerial.available
    }//while(1)
}//readFromVC505()

String RemoveCategoryFromWeight(String weight) {

    int length = weight.length();
    String dummyString = "";

    bool hitSemiColon = false;

    for (int i = 0; i < length; i++) {
        if (weight.charAt(i) == ':' && !hitSemiColon) {
            hitSemiColon = true;
            continue;
        }
        if (hitSemiColon) {

            //            if ((weight.charAt(i) >= '0' && weight.charAt(i) <= '9') ||
            //                    weight.charAt(i) == '.' || weight.charAt(i) == ':' ||
            //                    weight.charAt(i) == '/')
            dummyString += weight.charAt(i);

        }
    }
    return dummyString;

}

bool MotIsDirt() {
    int length = weightBuffers[MOT].length();
    for (int i = 0; i < length; i++) {
        if (weightBuffers[MOT].charAt(i) == 'D' ||
                weightBuffers[MOT].charAt(i) == 'd') {

            return true;
        }
    }
    return false;
}

int countWeightsExpected() {
    int counter = 0;
    for (int i = 0; i < maximumHopperEntries; i++) {

        if (weightFieldIsOn[i])
            counter++;
    }
    return counter;
}

int countmasterWeightsExpected() {
    int counter = 0;
    for (int i = 0; i < maximumHopperEntries; i++) {

        if (masterWeightIsOn[i])
            counter++;
    }
    return counter;
}

bool MotISBoth() {
    int length = weightBuffers[MOT].length();
    for (int i = 0; i < length; i++) {
        if (weightBuffers[MOT].charAt(i) == 'B' ||
                weightBuffers[MOT].charAt(i) == 'B') {

            return true;
        }
    }
    return false;
}

bool MotISExtra() {
    int length = weightBuffers[MOT].length();
    for (int i = 0; i < length; i++) {
        if (weightBuffers[MOT].charAt(i) == 'E' ||
                weightBuffers[MOT].charAt(i) == 'E') {
            return true;
        }
    }
    return false;
}


#endif	/* HOPPER_H */

