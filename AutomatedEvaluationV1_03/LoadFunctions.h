/* 
 * File:   LoadFunctions.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Created on June 20, 2013, 10:52 AM
 */

#ifndef LOADFUNCTIONS_H
#define	LOADFUNCTIONS_H

#include "ForwardDeclarations.h"
#include "GlobalVariables.h"
#include "StringForSD.h"
#include "SDManipulation.h"


//==============================================================================
//Load Functions
//This function expects either a correctly formatted date - mm/dd/yy
//or a wrongly modified one which is: mm/dd/yyyy

String correctDateFormat(String date) {

    int firstSlashPosition = 0;
    while (date[firstSlashPosition++] != '/');

    int secondSlashPosition = firstSlashPosition;
    while (date[secondSlashPosition++] != '/');


    String Month = "";
    for (int i = 0; i < firstSlashPosition; i++)
        Month += date[i];

    String Day = "";
    for (int i = firstSlashPosition; i < secondSlashPosition; i++) {\
        Day += date[i];
    }

    String Year = "";
    for (int i = secondSlashPosition; i < date.length(); i++)
        Year += date[i];

    //Remove all whitespace
    Month.trim();
    Serial.println(Month);

    Day.trim();
    Serial.println(Day);
    Year.trim();
    Serial.println(Year);

    if (Year.toInt() < 2000) {
        int year_int = Year.toInt();
        Year = "";
        Year += year_int + 2000;
    }

    if (Month.toInt() < 10 && Month.length() < 2)
        Month = String("0") + Month;
    if (Day.toInt() < 10 && Day.length() < 2)
        Day = String("0") + Day;
    if (Year.toInt() < 10 && Year.length() < 2)
        Year = String("0") + Year;

    String correctDate = Month;
    correctDate += Day;
    correctDate += Year;

    return correctDate;
}

bool timeIsPm(String time) {
    for (int i = 0; i < time.length(); i++) {
        if (time[i] == 'P' || time[i] == 'p')
            return true;
    }
    return false;
}

String correctTimeFormat(String time) {
    time.trim();
    if (timeIsPm(time)) {
        String hour_s = "";
        int i = 0;
        while (time[i] != ':') {
            hour_s += time[i++];
        }
        int colonPosition = i;
        int hour = hour_s.toInt(); //to convert to military time
        if (hour != 12)
            hour += 12;


        hour_s = "";
        hour_s += hour;

        for (int i = colonPosition; i <= colonPosition + 2; i++) {
            hour_s += time[i];
        }

        if (hour_s.length() < 8)//if the time doesn't have a seconds part
            hour_s += ":00";

        Serial.println("this is the time afternoon");
        Serial.println(hour_s);

        return hour_s;

    } else {//it is morning time

        String hour_s = "";
        int i = 0;

        while (time[i] != ':') {
            hour_s += time[i++];
        }

        int colonPosition = i;
        int hour = hour_s.toInt(); //to convert to military time
        if (hour == 12)
            hour -= 12;

        hour_s = "";
        if (hour < 10)
            hour_s += "0";
        hour_s += hour;
        for (int i = colonPosition; i <= colonPosition + 2; i++) {
            hour_s += time[i];
        }

        if (hour_s.length() < 8)//if the time doesn't have a seconds part
            hour_s += ":00";

        Serial.println("this is the time afternoon");
        Serial.println(hour_s);

        return hour_s;
    }
}

void loadNumberExchange(HardwareSerial& SerialChosen) {
    //Gets the load number from the slave
    Serial.println(F("Getting Slave's load number"));
    char dummyLoadNumber[6];
    getLoadNumberFromSerial(SerialChosen, dummyLoadNumber);
    LoadNumber = atoi(dummyLoadNumber);
    SerialChosen.print(CONFIRMATION);

    SD.mkdir("TEMP/");




    //have to implement the regrade part of this
    //Have to change it so that instead of addToSD it basically only does it once

    //addToSD will take an array of StringForSD, which means we have to accumulate it into an array
    //the elegant thing to do is a linked list, but this has to be done kind of quickly
    //So im going to cheat and use part of an array

    int forSdCounter = 0;
    StringForSD sdInput[15];

    if (ReceivingDevicesFlags[1] && expectedDevices[1]) {//ph
        Serial.println(F("Adding the ph buffer"));
        sdInput[forSdCounter++].add(phBuffer.substring(0, 4), SDLocation[3]);
    }
    if (ReceivingDevicesFlags[0] && expectedDevices[0]) {//color
        Serial.println(F("Adding color buffer"));
        sdInput[forSdCounter++].add(colorBuffer, SDLocation[1]);
    }//
    if (ReceivingDevicesFlags[2] && expectedDevices[2]) {//refrac
        Serial.println(F("Adding the refrac buffer"));
        sdInput[forSdCounter++].add(refracBuffer, SDLocation[2]);
    }
    if (ReceivingDevicesFlags[3] && expectedDevices[3]) {//Weights

        if (masterWeightIsOn[DATE] && weightFieldIsOn[DATE]) {//Date
            Serial.println(F("Doing DATE things"));
            Serial.print(F("This is the DATE without category: "));
            Serial.println(RemoveCategoryFromWeight(weightBuffers[DATE]));
            Serial.print(F("This is after correctDateFormat: "));
            Serial.println(correctDateFormat(RemoveCategoryFromWeight(weightBuffers[DATE])));

            sdInput[forSdCounter++].add(correctDateFormat(RemoveCategoryFromWeight(weightBuffers[DATE])), 2);
        }
        if (masterWeightIsOn[TIME] && weightFieldIsOn[TIME]) {//Time
            //            Serial.println(F("Doing time things"));
            //            Serial.print(F("This is the time without category: "));
            //            Serial.println(RemoveCategoryFromWeight(weightBuffers[TIME]));
            //            Serial.print(F("This is after correctTimeFormat: "));
            //            Serial.println(correctTimeFormat(RemoveCategoryFromWeight(weightBuffers[TIME])));

            sdInput[forSdCounter++].add(correctTimeFormat(RemoveCategoryFromWeight(weightBuffers[TIME])), 3);
        }
        if (masterWeightIsOn[HOPPERWT] && weightFieldIsOn[HOPPERWT]) {//Hopper Wt
            sdInput[forSdCounter++].add(RemoveCategoryFromWeight(weightBuffers[HOPPERWT]), 4);
        }
        if (masterWeightIsOn[WORM] && weightFieldIsOn[WORM]) {//Worm
            sdInput[forSdCounter++].add(RemoveCategoryFromWeight(weightBuffers[WORM]), 5);
        }
        if (masterWeightIsOn[MOLD] && weightFieldIsOn[MOLD]) {//Mold
            sdInput[forSdCounter++].add(RemoveCategoryFromWeight(weightBuffers[MOLD]), 6);
        }
        if (masterWeightIsOn[GREEN] && weightFieldIsOn[GREEN]) {//Green
            sdInput[forSdCounter++].add(RemoveCategoryFromWeight(weightBuffers[GREEN]), 7);
        }

        if (masterWeightIsOn[MOT] && weightFieldIsOn[MOT]) {//Mot
            String yes = "y";
            String no = "n";

            if (MotISBoth()) {
                sdInput[forSdCounter++].add(yes, 8);
                sdInput[forSdCounter++].add(yes, 9);
            } else if (MotIsDirt()) {
                sdInput[forSdCounter++].add(yes, 8);
                sdInput[forSdCounter++].add(no, 9);
            } else if (MotISExtra()) {
                sdInput[forSdCounter++].add(no, 8);
                sdInput[forSdCounter++].add(yes, 9);
            } else {//it is undefined mot
                sdInput[forSdCounter++].add(no, 8);
                sdInput[forSdCounter++].add(no, 9);
            }
            sdInput[forSdCounter++].add(RemoveCategoryFromWeight(weightBuffers[MOT]), 10);
        }//MOT


        if (masterWeightIsOn[LU] && weightFieldIsOn[LU]) {//LU
            sdInput[forSdCounter++].add(RemoveCategoryFromWeight(weightBuffers[LU]), 11);
        }
        if (masterWeightIsOn[SPARE_1] && weightFieldIsOn[SPARE_1]) {//Other Defect 1
            sdInput[forSdCounter++].add(RemoveCategoryFromWeight(weightBuffers[SPARE_1]), 15);
        }
        if (masterWeightIsOn[SPARE_2] && weightFieldIsOn[SPARE_2]) {//Other defect 2
            sdInput[forSdCounter++].add(RemoveCategoryFromWeight(weightBuffers[SPARE_2]), 16);
        }
    }//weights
    
    char filePath_c[25];
    if (forSdCounter == 0) {//there is nothing to write
        return;
    } else {//There is stuff we need to write to the SD card

        //add the load number to the queue
        loadQueue.push(LoadNumber);
        String filePath_s = "temp/";
        filePath_s += LoadNumber;
        filePath_s += ".txt";

        filePath_s.toCharArray(filePath_c, 25);

        //If the file doesnt exist make a new one that is default
        File tempLoad;
        if (!SD.exists(filePath_c)) {//if the file doesnt exist, create one with -1 everywhere
            Serial.println(F("File does not exist"));
            tempLoad = SD.open(filePath_c, FILE_WRITE);
            createDefaultLoad(tempLoad);
            tempLoad.close();
        }
    }

    addToSD(filePath_c, sdInput, false, forSdCounter);

    //Now we have written everything to the file, we can check if it is complete
    if (loadIsDone(filePath_c, false)) {
        Serial.println("the load is complete");
        sendFileAndRemoveFromQueue(filePath_c, false);
    }

}//loadNumberExchange

void sendFileAndRemoveFromQueue(char* filePath_c, bool isRegrade) {
    //send the file to gradestar and restore the file in its right place
    File tempLoad = SD.open(filePath_c, FILE_WRITE);
    sendToGradestar(masterAndGradestarSerial, tempLoad);

    //Move file to the right location
    String correctLocation = getAppropriateFilename(filePath_c);
    Serial.print("The correct location is: ");
    Serial.println(correctLocation);


    String foldersThatNeedToBeMade = getAppropriateFolderPath(filePath_c);
    SD.mkdir(&foldersThatNeedToBeMade[0]);
    Serial.print("The folders: ");
    Serial.println(foldersThatNeedToBeMade);


    //open the original file
    char filePath_correct[25];

    //copy the old file to the new location
    correctLocation.toCharArray(filePath_correct, 25);
    copyWholeFileFromTo(tempLoad, filePath_correct);

    //remove the old position
    SD.remove(filePath_c);

    //Close the file
    tempLoad.close();

    //delete the temp load from the queue
    loadQueue.removeData(LoadNumber);
}

void SetLoadNumber() {

    lcd_clear();
    lcd_print(L1Ta, F("Current Load#: "));
    lcd_print(L1Ta + 16, itoa(LoadNumber, loadNumberitoa, 10));

    lcd_print(L3Ta, F("New LD#: "));
    key = 0;

    int i = 0;

    while (i < 4) {
        getKey();
        while ((key < '0' || key > '9') && key != ENTER && key != CLEAR) getKey(); //getting number

        if (key == ENTER) {//done
            loadNumberAscii[i] = '\0'; //terminating character

            break;
        } else if (key == CLEAR) {//delete one character

            if (i != 0) {
                loadNumberAscii[--i] = 32;
                lcd_printChar(L3Ta + 10 + i, ' ');
            }
        } else {//else the person is still entering in the number
            loadNumberAscii[i] = key;
            lcd_printChar(L3Ta + 10 + i, key);
            i++;
        }



    }//while getting the ascii load number



    int dummyLoad = atoi(loadNumberAscii);
    if (dummyLoad >= 9999) {//checks to see that the load number is in bounds
        lcd_print(L4Ta, F("# too high"));
        delay(1000);
    } else if (dummyLoad <= 0) {
        lcd_print(L4Ta, F("# too low"));
        delay(1000);
    } else {
        lcd_print(L4Ta, F("Load Set"));
        LoadNumber = dummyLoad;
        delay(1000);

    }

    saveCurrentConfiguration(MAIN_MENU_MODE);

}//SetLoadNumber

//***************************************

void decrementLoadNumber() {
    if (LoadNumber > 0)//loads cannot be negative
        --LoadNumber;

}

//***************************************

void incrementLoadNumber() {
    if (LoadNumber < 999) //has to be less than max of unsigned int
        ++LoadNumber;
}

//***************************************





#endif	/* LOADFUNCTIONS_H */

